cmake_minimum_required(VERSION 3.10)
project(Tensor-Decomposition VERSION 0.1)

set(CMAKE_CXX_STANDARD 17 )
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mkl -O3 -fp-stack-check -g -traceback -qopenmp")

include_directories(../include/)

#add_executable(test_col_to_row main_col_to_row)
#add_executable(test_core_diag main_core_diag.cpp)
#add_executable(test_dump_hamiltonian main_dump_hamiltonian.cpp)
#add_executable(test_mkl_wrapper main_mkl_wrapper.cpp)
add_executable(test_project_harmonic main_project_harmonic.cpp)
#add_executable(test_decompose main_decompose.cpp)
