#include <iostream>
#include "Grid.hpp"
#include <array>
#include "TuckerMethod.hpp"
#include <cmath>
#include <ctime>
#include "helper.hpp"
#include <omp.h>
#include <algorithm>
#include <fstream>
using namespace TensorDecompose;

int main(int argc, char *argv[]) {
//    if (argc != 8){
//        std::cout << "Input error!!\n(Usage) ./exe 'x_dim' 'y_dim' 'z_dim' 'x_rank' 'y_rank' 'z_rank' \"File_name\"" << std::endl;
//        exit(-1);
//    }

    std::string file_name = argv[7];

    double start1, end1, start2, end2, start_time_chk, end_time_chk;
    size_t x_dim = atoi(argv[1]);
    size_t y_dim = atoi(argv[2]);
    size_t z_dim = atoi(argv[3]);
    #pragma omp parallel
    {
        #pragma omp master
        std::cout << "omp_num_threads : " << omp_get_num_threads() << std::endl;
        #pragma omp master
        std::cout << "(x_dim, y_dim, z_dim) : (" << x_dim << ", " << y_dim << ", " << z_dim << ")" << std::endl;
    }
    std::cout << "File_name = " << file_name << std::endl;
    const size_t dimension = 6;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim, x_dim, y_dim, z_dim};

    start1 = omp_get_wtime();

    auto tensor = SparseTensor<double, dimension>(shape);
    tensor.read_file_Epetra(file_name);

    double local_shift = 100;   // default value
    if (argc == 9){
        local_shift = atof(argv[8]);
    }
    std::cout << "local_shift = " << local_shift << std::endl;
    tensor.local_shift(local_shift);
//    tensor.local_shift(200);
    tensor.complete();
//    kinetic_FD<double, dimension>(tensor, grid, 2);
//    harmonic_potential<double, dimension>(tensor, grid);
//    tensor.complete();
    end1 = omp_get_wtime();
    std::cout << "initialization time : " << end1 - start1 << " s" << std::endl;

    start1 = omp_get_wtime();
    
 ////////////////////////////////////////////////////////////////
//////////////////////// Decomposition /////////////////////////
    size_t x_rank = atoi(argv[4]);
    size_t y_rank = atoi(argv[5]);
    size_t z_rank = atoi(argv[6]);
    size_t rank_mult = x_rank * y_rank * z_rank;
    std::array<size_t, dimension> ranks = {x_rank, y_rank, z_rank, x_rank, y_rank, z_rank};

    std::cout << "========== dim of core tensor: (" << x_rank << ", " << y_rank << ", " << z_rank
    << ", " << x_rank << ", " << y_rank << ", " << z_rank << ") ==========" << std::endl;

// Tucker decomposition //
    start2 = omp_get_wtime();
    auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
    end2 = omp_get_wtime();
    std::cout << "HOSVD Time : " << end2 - start2 <<"s" <<std::endl; 
// Check eigenvalues //
    std::vector<double> eigen_values(x_rank*y_rank*z_rank);

    start_time_chk = omp_get_wtime();   // Check Diagonalize Time.
    //const size_t eig_num = rank_mult;
//    const size_t eig_num = 24;  // diamondSi
    const size_t eig_num = 30;  // Cu_fcc
    tucker.diagonalize(eigen_values, rank_mult);
    end_time_chk = omp_get_wtime();     // Check Diagonalize Time.
    std::cout << std::endl;
    std::cout << "Diagonalize time (core tensor) : " << end_time_chk - start_time_chk << " s" << std::endl;
    
    // Iterative Diagonalization //
    start_time_chk = omp_get_wtime();
    std::vector<double> eval_iter;
    std::vector<double> evec_iter;
    diagonalize_iter(tensor, eval_iter, evec_iter, eig_num);
    end_time_chk = omp_get_wtime();
    std::cout << "Diagonalize time (iter) : " << end_time_chk - start_time_chk << " s" << std::endl << std::endl;

    bool PRINT_EIGENVALUES = true;//false;//true;
    if(PRINT_EIGENVALUES){
        std::cout << "decomposed_eigenvalues = " << std::endl;
        //for (size_t i = 0; i < std::min(int(rank*rank*rank), 10); i++){
        for (size_t i = 0; i < std::min(int(rank_mult), int(eig_num)); i++){
            std::cout << std::fixed;
            std::cout.precision(7);
            std::cout << eigen_values[i] <<"\t" ;
        } std::cout << std::endl;

        std::cout << "iterative_digenvalues = " << std::endl;
        //for (size_t i = 0; i < rank_mult; i++){
        for (size_t i = 0; i < std::min(int(rank_mult), int(eig_num)); i++){
            std::cout << std::fixed;
            std::cout.precision(7);
            std::cout << eval_iter[i] <<"\t" ;
        } std::cout << std::endl;

    }

        bool PRINT_EIGEN_DIFF_ITER = true;
        if(PRINT_EIGEN_DIFF_ITER){
            std::cout << "eigenvalues diff (with iterative) = " << std::endl;
            for (size_t i = 0; i < std::min(int(rank_mult), int(eig_num)); i++){
                std::cout << std::fixed;
                std::cout.precision(7);
                std::cout << eigen_values[i] - eval_iter[i] <<"\t" ;
            }
            std::cout << std::endl;
        }
            bool PRINT_Us = true;//false;//true;
            if (PRINT_Us){
                std::cout << "Print Us[0] :" << std::endl;
                std::ofstream outFile("Us.txt");
                if (outFile.is_open()){
                    outFile << tucker.Us[0] << std::endl;
                }
//                for (size_t dim = 0; dim < dimension; dim++){
//                    std::cout << "Us[" << dim << "] = " << tucker.Us[dim] << std::endl;
//                }
                outFile.close();

                std::cout << "Print Us[1] : " << std::endl;
                std::ofstream outFileY("Us_y.txt");
                if (outFileY.is_open()){
                    outFileY << tucker.Us[1] << std::endl;
                }
                outFileY.close();

                std::cout << "Print Us[2] : " << std::endl;
                std::ofstream outFileZ("Us_z.txt");
                if (outFileZ.is_open()){
                    outFileZ << tucker.Us[2] << std::endl;
                }
                outFileZ.close();

            }
        
    end1 = omp_get_wtime();
    std::cout << std::endl;
    std::cout << "total time : " << end1 - start1 << " s" << std::endl;
    return 0;
}
