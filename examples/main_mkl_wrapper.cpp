#include <mkl_wrapper.hpp>
#include <iostream>
using namespace TensorDecompose;
#define N 3
int main(int argc, char *argv[]) {
    double start1, end1, start2, end2, start_time_chk, end_time_chk;

//    double* A = new double[N*N];
//    for (size_t i=0; i<N*N; i++){
//        A[i] = (double)i;
//    }
//
//    double* x = new double[N];
//    double* y = new double[N];
//    for (size_t i=0; i<N; i++){
//        x[i] = (double) i;
//        y[i] = (double) N-i;
//    }
//    
//    heevd<double>( LAPACK_COL_MAJOR, 'V', 'L', N, A, N, x );
//
//
//    std::cout << "new" <<std::endl;
//    for (size_t i=0; i<N; i++){
//        std::cout << x[i] <<std::endl;
//    }
//
//    for (size_t i=0; i<N*N; i++){
//        A[i] = (double)i;
//    }
//    LAPACKE_dsyevd( LAPACK_COL_MAJOR, 'V', 'L', N, A, N, x );
//
//    std::cout << "old" <<std::endl;
//    for (size_t i=0; i<N; i++){
//        std::cout << x[i] <<std::endl;
//    }
//

    std::complex<double>* A = new std::complex<double>[N*N];
    for (size_t i=0; i<N*N; i++){
        A[i] = std::complex<double> ( i, i );
    }

    double* x = new double[N];
    double* y = new double[N];
    for (size_t i=0; i<N; i++){
        x[i] = (double) i;
        y[i] = (double) N-i;
    }

    heevd<std::complex<double> >( LAPACK_COL_MAJOR, 'V', 'L', N, A, N, x );


    std::cout << "new" <<std::endl;
    for (size_t i=0; i<N; i++){
        std::cout << x[i] <<std::endl;
    }

    for (size_t i=0; i<N*N; i++){
        A[i] = std::complex<double> ( i, i );
    }

    LAPACKE_zheevd( LAPACK_COL_MAJOR, 'V', 'L', N, A, N, x );

    std::cout << "old" <<std::endl;
    for (size_t i=0; i<N; i++){
        std::cout << x[i] <<std::endl;
    }
    delete[] A;
    delete[] x;
    delete[] y;
    return 0;
}
