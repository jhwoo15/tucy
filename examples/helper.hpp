#pragma once
#include <iostream>
#include "Grid.hpp"
#include <array>
#include "Tensor.hpp"
#include "Tucker.hpp"
//#include "TuckerMethod.hpp"
#include "SparseTensor.hpp"
#include "DenseTensor.hpp"
#include <cmath>
#include <ctime>

using namespace TensorDecompose;

template <class datatype, size_t dimension>
void kinetic_FD_matrix(Grid grid, Matrix<datatype>& T_xx, Matrix<datatype>& T_yy, Matrix<datatype>& T_zz, size_t stencil = 2){
    double scaling_x = grid.scaling[0];
    double scaling_y = grid.scaling[1];
    double scaling_z = grid.scaling[2];
    size_t dim_x = grid.dim_x;
    size_t dim_y = grid.dim_y;
    size_t dim_z = grid.dim_z;

    double coef0 = 0.0;
    double coef1 = 0.0;
    double coef2 = 0.0;
    double coef3 = 0.0;

    T_xx = Matrix<datatype>(dim_x, dim_x);
    T_yy = Matrix<datatype>(dim_y, dim_y);
    T_zz = Matrix<datatype>(dim_z, dim_z);
    if (stencil == 1){
        coef0 = -2;
        coef1 = 1;
    }
    if (stencil == 2){
        coef0 = -5.0/2.0;
        coef1 = 4.0/3.0;
        coef2 = -1.0/12.0;
    }
    if (stencil == 3){
        coef0 = -49.0/18.0;
        coef1 = 3.0/2.0;
        coef2 = -3.0/20.0;
        coef3 = 1.0/90.0;
    }
    // T_xx //
    for (size_t i = 0; i < T_xx.row; ++i){
        // Diagonal element //
        T_xx(i, i) += -coef0 / scaling_x / scaling_x / 2;

        // Nondiagonal element //
        if (stencil >= 1){
            if (i >= 1){
                T_xx(i, i-1) += -coef1 / scaling_x / scaling_x / 2;
            }
            if (i+1 < dim_x){
                T_xx(i, i+1) += -coef1 / scaling_x / scaling_x / 2;
            }
        }
        if (stencil >= 2){
            if (i >= 2){
                T_xx(i, i-2) += -coef2 / scaling_x / scaling_x / 2;
            }
            if (i+2 < dim_x){
                T_xx(i, i+2) += -coef2 / scaling_x / scaling_x / 2;
            }
        }
    }
    // T_yy //
    for (size_t i = 0; i < T_yy.row; ++i){
        // Diagonal element //
        T_yy(i, i) += -coef0 / scaling_y / scaling_y / 2;

        // Nondiagonal element //
        if (stencil >= 1){
            if (i >= 1){
                T_yy(i, i-1) += -coef1 / scaling_y / scaling_y / 2;
            }
            if (i+1 < dim_y){
                T_yy(i, i+1) += -coef1 / scaling_y / scaling_y / 2;
            }
        }
        if (stencil >= 2){
            if (i >= 2){
                T_yy(i, i-2) += -coef2 / scaling_y / scaling_y / 2;
            }
            if (i+2 < dim_y){
                T_yy(i, i+2) += -coef2 / scaling_y / scaling_y / 2;
            }
        }
    }
    // T_zz //
    for (size_t i = 0; i < T_zz.row; ++i){
        // Diagonal element //
        T_zz(i, i) += -coef0 / scaling_z / scaling_z / 2;

        // Nondiagonal element //
        if (stencil >= 1){
            if (i >= 1){
                T_zz(i, i-1) += -coef1 / scaling_z / scaling_z / 2;
            }
            if (i+1 < dim_z){
                T_zz(i, i+1) += -coef1 / scaling_z / scaling_z / 2;
            }
        }
        if (stencil >= 2){
            if (i >= 2){
                T_zz(i, i-2) += -coef2 / scaling_z / scaling_z / 2;
            }
            if (i+2 < dim_z){
                T_zz(i, i+2) += -coef2 / scaling_z / scaling_z / 2;
            }
        }
    }
}

template <class datatype, size_t dimension>
void kinetic_FD(TensorDecompose::Tensor<datatype,dimension>& tensor, Grid grid, size_t stencil = 2){

// For 6-way tensor
    std::array<size_t, dimension> index;
    double scaling_x = grid.scaling[0];
    double scaling_y = grid.scaling[1];
    double scaling_z = grid.scaling[2];
    size_t dim_x = grid.dim_x;
    size_t dim_y = grid.dim_y;
    size_t dim_z = grid.dim_z;

    double coef0 = 0.0;
    double coef1 = 0.0;
    double coef2 = 0.0;
    double coef3 = 0.0;

    if (stencil == 1){
        coef0 = -2;
        coef1 = 1;
    }
    if (stencil == 2){
        coef0 = -5.0/2.0;
        coef1 = 4.0/3.0;
        coef2 = -1.0/12.0;
    }
    if (stencil == 3){
        coef0 = -49.0/18.0;
        coef1 = 3.0/2.0;
        coef2 = -3.0/20.0;
        coef3 = 1.0/90.0;
    }

    for(size_t i = 0; i < tensor.shape[0]; ++i){
        for(size_t j = 0; j < tensor.shape[1]; ++j){
            for(size_t k = 0; k < tensor.shape[2]; ++k){
                index = {i, j, k, i, j, k};

                tensor.insert_value(index, -coef0 / scaling_x / scaling_x / 2);
                tensor.insert_value(index, -coef0 / scaling_y / scaling_y / 2);
                tensor.insert_value(index, -coef0 / scaling_z / scaling_z / 2);
              
                if(stencil >= 1){
                    if(i >= 1){
                        index = {i, j, k, i-1, j, k};
                        tensor.insert_value(index, -coef1 / scaling_x / scaling_x / 2 );
                    }
                    if(i+1 < dim_x){
                        index = {i, j, k, i+1, j, k};
                        tensor.insert_value(index, -coef1 / scaling_x / scaling_x / 2);
                    }
                    if(j >= 1){
                        index = {i, j, k, i, j-1, k};
                        tensor.insert_value(index, -coef1 / scaling_y / scaling_y / 2);
                    }
                    if(j+1 < dim_y){
                        index = {i, j, k, i, j+1, k};
                        tensor.insert_value(index, -coef1 / scaling_y / scaling_y / 2);
                    }
                    if(k >= 1){
                        index = {i, j, k, i, j, k-1};
                        tensor.insert_value(index, -coef1 / scaling_z / scaling_z / 2);
                    }
                    if(k+1 < dim_z){
                        index = {i, j, k, i, j, k+1};
                        tensor.insert_value(index, -coef1 / scaling_z / scaling_z / 2);
                    }
                }
                if (stencil >= 2){
                    if(i >= 2){
                        index = {i, j, k, i-2, j, k};
                        tensor.insert_value(index, -coef2 / scaling_x / scaling_x / 2);
                    }
                    if(i+2 < dim_x){
                        index = {i, j, k, i+2, j, k};
                        tensor.insert_value(index, -coef2 / scaling_x / scaling_x / 2);
                    }
                    if(j >= 2){
                        index = {i, j, k, i, j-2, k};
                        tensor.insert_value(index, -coef2 / scaling_y / scaling_y / 2);
                    }
                    if(j+2 < dim_y){
                        index = {i, j, k, i, j+2, k};
                        tensor.insert_value(index, -coef2 / scaling_y / scaling_y / 2);
                    }
                    if(k >= 2){
                        index = {i, j, k, i, j, k-2};
                        tensor.insert_value(index, -coef2 / scaling_z / scaling_z / 2);
                    }
                    if(k+2 < dim_z){
                        index = {i, j, k, i, j, k+2};
                        tensor.insert_value(index, -coef2 / scaling_z / scaling_z / 2);
                    }
                }
                if (stencil >= 3){
                    if(i >= 3){
                        index = {i, j, k, i-3, j, k};
                        tensor.insert_value(index, -coef3 / scaling_x / scaling_x / 2);
                    }
                    if(i+3 < dim_x){
                        index = {i, j, k, i+3, j, k};
                        tensor.insert_value(index, -coef3 / scaling_x / scaling_x / 2);
                    }
                    if(j >= 3){
                        index = {i, j, k, i, j-3, k};
                        tensor.insert_value(index, -coef3 / scaling_y / scaling_y / 2);
                    }
                    if(j+3 < dim_y){
                        index = {i, j, k, i, j+3, k};
                        tensor.insert_value(index, -coef3 / scaling_y / scaling_y / 2);
                    }
                    if(k >= 3){
                        index = {i, j, k, i, j, k-3};
                        tensor.insert_value(index, -coef3 / scaling_z / scaling_z / 2);
                    }
                    if(k+3 < dim_z){
                        index = {i, j, k, i, j, k+3};
                        tensor.insert_value(index,  -coef3 / scaling_z / scaling_z / 2);
                    }
                }
            }
        }
    }
    return;    
}
//template <class datatype, size_t dimension>
//void kinetic_FD(TensorDecompose::DenseTensor<datatype,dimension>& tensor, Grid grid, size_t stencil = 1){
//
//// For 6-way tensor
//    std::array<size_t, dimension> index;
//    double scaling_x = grid.scaling[0];
//    double scaling_y = grid.scaling[1];
//    double scaling_z = grid.scaling[2];
//    size_t dim_x = grid.dim_x;
//    size_t dim_y = grid.dim_y;
//    size_t dim_z = grid.dim_z;
//
//    for(size_t i = 0; i < tensor.shape[0]; ++i){
//        for(size_t j = 0; j < tensor.shape[1]; ++j){
//            for(size_t k = 0; k < tensor.shape[2]; ++k){
//                index = {i, j, k, i, j, k};
//                tensor(index) += 2 / scaling_x / 2;
//                tensor(index) += 2 / scaling_y / 2;
//                tensor(index) += 2 / scaling_z / 2;
//                if(stencil == 1){
//                    if(i >= 1){
//                        index = {i, j, k, i-1, j, k};
//                        tensor(index) -= 1 / scaling_x / scaling_x / 2;
//                    }
//                    if(i+1 < dim_x){
//                        index = {i, j, k, i+1, j, k};
//                        tensor(index) -= 1 / scaling_x / scaling_x / 2;
//                    }
//                    if(j >= 1){
//                        index = {i, j, k, i, j-1, k};
//                        tensor(index) -= 1 / scaling_y / scaling_y / 2;
//                    }
//                    if(j+1 < dim_y){
//                        index = {i, j, k, i, j+1, k};
//                        tensor(index) -= 1 / scaling_y / scaling_y / 2;
//                    }
//                    if(k >= 1){
//                        index = {i, j, k, i, j, k-1};
//                        tensor(index) -= 1 / scaling_z / scaling_z / 2;
//                    }
//                    if(k+1 < dim_z){
//                        index = {i, j, k, i, j, k+1};
//                        tensor(index) -= 1 / scaling_z / scaling_z / 2;
//                    }
//                }
//            }
//        }
//    }
//    return;    
//}
//template <class datatype, size_t dimension>
//void harmonic_potential(TensorDecompose::DenseTensor<datatype,dimension>& tensor, Grid grid){
//// For 6-way tensor
//    std::array<size_t, dimension> index;
//    size_t dim_x = grid.dim_x;
//    size_t dim_y = grid.dim_y;
//    size_t dim_z = grid.dim_z;
//
//    for(size_t i_x = 0; i_x < dim_x; ++i_x){
//        for(size_t i_y = 0; i_y < dim_y; ++i_y){
//            for(size_t i_z = 0; i_z < dim_z; ++i_z){
//                index = {i_x, i_y, i_z, i_x, i_y, i_z};
//                tensor(index) -= 1 / pow( (1 + pow(grid.scaled_grid[0][i_x], 2) + pow(grid.scaled_grid[1][i_y], 2) + pow(grid.scaled_grid[2][i_z], 2)), 0.5);
//            }
//        }
//    }    
//    return;
//}


//template <class datatype, size_t dimension>
//void kinetic_FD(TensorDecompose::SparseTensor<datatype,dimension>& tensor, Grid grid, size_t stencil = 1){
//
//// For 6-way tensor
//    std::array<size_t, dimension> index;
//    double scaling_x = grid.scaling[0];
//    double scaling_y = grid.scaling[1];
//    double scaling_z = grid.scaling[2];
//    size_t dim_x = grid.dim_x;
//    size_t dim_y = grid.dim_y;
//    size_t dim_z = grid.dim_z;
//
//    for(size_t i = 0; i < tensor.shape[0]; ++i){
//        for(size_t j = 0; j < tensor.shape[1]; ++j){
//            for(size_t k = 0; k < tensor.shape[2]; ++k){
//                index = {i, j, k, i, j, k};
//                tensor.insert_value(index, 2 / scaling_x / 2);
//                tensor.insert_value(index, 2 / scaling_y / 2);
//                tensor.insert_value(index, 2 / scaling_z / 2);
//                if(stencil == 1){
//                    if(i >= 1){
//                        index = {i, j, k, i-1, j, k};
//                        tensor.insert_value(index,-1 / scaling_x / scaling_x / 2);
//                    }
//                    if(i+1 < dim_x){
//                        index = {i, j, k, i+1, j, k};
//                        tensor.insert_value(index,-1 / scaling_x / scaling_x / 2);
//                    }
//                    if(j >= 1){
//                        index = {i, j, k, i, j-1, k};
//                        tensor.insert_value(index, -1 / scaling_y / scaling_y / 2);
//                    }
//                    if(j+1 < dim_y){
//                        index = {i, j, k, i, j+1, k};
//                        tensor.insert_value(index, -1 / scaling_y / scaling_y / 2);
//                    }
//                    if(k >= 1){
//                        index = {i, j, k, i, j, k-1};
//                        tensor.insert_value(index, -1 / scaling_z / scaling_z / 2);
//                    }
//                    if(k+1 < dim_z){
//                        index = {i, j, k, i, j, k+1};
//                        tensor.insert_value(index, -1 / scaling_z / scaling_z / 2);
//                    }
//                }
//            }
//        }
//    }
//    return;    
//}
template <class datatype, size_t dimension>
void harmonic_potential(TensorDecompose::Tensor<datatype,dimension>& tensor, Grid grid){
// For 6-way tensor
    std::array<size_t, dimension> index;
    size_t dim_x = grid.dim_x;
    size_t dim_y = grid.dim_y;
    size_t dim_z = grid.dim_z;

    const double scaling_x = grid.scaling[0];
    const double scaling_y = grid.scaling[1];
    const double scaling_z = grid.scaling[2];

    for(size_t i_x = 0; i_x < dim_x; ++i_x){
        for(size_t i_y = 0; i_y < dim_y; ++i_y){
            for(size_t i_z = 0; i_z < dim_z; ++i_z){
                index = {i_x, i_y, i_z, i_x, i_y, i_z};
                //tensor.insert_value(index, 0.5*( pow(grid.scaled_grid[0][i_x], 2) + pow(grid.scaled_grid[1][i_y], 2) + pow(grid.scaled_grid[2][i_z], 2) ) );
                tensor.insert_value(index, -300 -1 / pow( (0.1 + pow(grid.scaled_grid[0][i_x], 2) + pow(grid.scaled_grid[1][i_y], 2) + pow(grid.scaled_grid[2][i_z], 2)), 0.5));
            }
        }
    }    
    return;
}

template <class datatype, size_t dimension>
void harmonic_potential_grid(Grid grid, std::vector<datatype>& potential) {
    // For 3-way tensor
    assert(dimension == 3);

    std::array<size_t, dimension> index;
    const size_t dim_x = grid.dim_x;
    const size_t dim_y = grid.dim_y;
    const size_t dim_z = grid.dim_z;
    const size_t total_dim = dim_x * dim_y * dim_z;

    const double scaling_x = grid.scaling[0];
    const double scaling_y = grid.scaling[1];
    const double scaling_z = grid.scaling[2];

    TensorDecompose::DenseTensor<datatype, dimension> tensor({dim_x, dim_y, dim_z});
    for(size_t i_x = 0; i_x < dim_x; ++i_x){
        for(size_t i_y = 0; i_y < dim_y; ++i_y){
            for(size_t i_z = 0; i_z < dim_z; ++i_z){
                //index = {i_x, i_y, i_z, i_x, i_y, i_z};
                index = {i_x, i_y, i_z};
                tensor.insert_value(index, -300 -1 / pow( (0.1 + pow(grid.scaled_grid[0][i_x], 2) + pow(grid.scaled_grid[1][i_y], 2) + pow(grid.scaled_grid[2][i_z], 2)), 0.5));
            }
        }
    }

    assert(potential.size() == total_dim);
    for (size_t i = 0; i < total_dim; i++){
        potential[i] = tensor[i];
    }
    return;
}

template <class datatype, size_t dimension>
SparseTensor<datatype, dimension> dense_to_sparse(DenseTensor<datatype, dimension>& dense_tensor){

    SparseTensor<datatype, dimension> sparse_tensor(dense_tensor.shape);

    for (size_t i = 0; i < dense_tensor.shape_mult[dimension]; i++){
        if (dense_tensor[i] != 0 ){
            sparse_tensor.insert_value( get_tensor_index<dimension> (i, dense_tensor.shape), dense_tensor[i] );
        }
    }
    return sparse_tensor;
}
