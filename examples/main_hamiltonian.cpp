#include <iostream>
#include "Grid.hpp"
#include <array>
#include "Tucker.hpp"
#include "TuckerMethod.hpp"
#include <cmath>
#include <ctime>
#include "helper.hpp"
#include <omp.h>
using namespace TensorDecompose;


int main() {
    double start1, end1, start2, end2;
    size_t dim = 10;
    size_t x_dim, y_dim, z_dim;
    x_dim = dim; y_dim = dim; z_dim = dim;
//    x_dim = dim; y_dim = dim+1; z_dim = dim+2;
    std::array<double, 3> scaling = {1, 1, 1};
    Grid grid(x_dim, y_dim, z_dim, scaling);
    const size_t dimension = 6;
    const size_t total_dim = x_dim * y_dim * z_dim * x_dim * y_dim * z_dim;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim, x_dim, y_dim, z_dim};
    
    auto ref_tensor = DenseTensor<double, dimension>(shape);
    kinetic_FD<double,dimension>(ref_tensor, grid);    
    harmonic_potential<double,dimension>(ref_tensor, grid); 

    start1 = omp_get_wtime();
    //auto tensor = DenseTensor<double, dimension>(shape);
    auto tensor = SparseTensor<double, dimension>(shape);
    kinetic_FD<double,dimension>(tensor, grid);    
    harmonic_potential<double,dimension>(tensor, grid); 
    tensor.complete();
    end1 = omp_get_wtime();
    std::cout << "initialization time : " << end1 - start1 << " s" << std::endl;

    start1 = omp_get_wtime();

    for (size_t rank = 1; rank < shape[0]+1; rank++) {
//    for (size_t rank = 10; rank < shape[0]+1; rank++) {
        std::array<size_t, dimension> ranks = {rank, rank, rank, rank, rank, rank};

        std::cout << "========== dim of core tensor: (" << rank << ", " << rank << ", " << rank
        << ", " << rank << ", " << rank << ", " << rank << ") ==========" << std::endl;

// Tucker decomposition //
        
        start2 = omp_get_wtime();
        //tucker.hosvd(ranks);
        auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
        end2 = omp_get_wtime();
        std::cout << "HOSVD Time : " << end2 - start2 <<"s" <<std::endl; 
// Tucker to tensor //
        std::cout << "Tucker to Tensor" << std::endl;
        std::vector<double> restored_tensor;
        tucker.tucker_to_tensor(restored_tensor);
  
// Calculate Error Norm //
        std::vector<double> error = ref_tensor.data;
        cblas_daxpy( error.size(), -1.0, restored_tensor.data(), 1, error.data(),1);
        auto norm = cblas_dnrm2(error.size(),error.data(), 1 );
        std::cout << "Error norm = " <<norm << std::endl;

        bool PRINT_U = false;
        if (PRINT_U){
            std::cout << "Print U" << std::endl;
            for (int i = 0; i < dimension; i++){
                std::cout << "U[" << i << "] =" << tucker.Us[i] << std::endl;
            }

            for (int i = 0; i < pow(rank, 6); i++){
                std::cout << tucker.core[i] << " ";
            } std::cout << std::endl;
        }

    } std::cout << std::endl;
    
    end1 = omp_get_wtime();
    std::cout << "total time : " << end1 - start1 << " s" << std::endl;
    return 0;
}
