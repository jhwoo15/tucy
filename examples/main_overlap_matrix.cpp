#include <iostream>
#include "Grid.hpp"
#include <array>
#include "Tucker.hpp"
#include "TuckerMethod.hpp"
#include <cmath>
#include "helper.hpp"
#include <omp.h>
#include <algorithm>
using namespace TensorDecompose;

int main() {
    //// Initialization ////
//    size_t dim = 12;
    size_t dim = 8;
    size_t x_dim, y_dim, z_dim;
    x_dim = dim; y_dim = dim; z_dim = dim;
//    x_dim = dim; y_dim = dim+1; z_dim = dim+2;
    std::array<double, 3> scaling = {0.22, 0.22, 0.22};
    Grid grid(x_dim, y_dim, z_dim, scaling);
    const size_t dimension = 6;
    const size_t total_dim = x_dim * y_dim * z_dim * x_dim * y_dim * z_dim;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim, x_dim, y_dim, z_dim};

    // Tensor to be decomposed
    auto tensor = SparseTensor<double, dimension>(shape);
    kinetic_FD<double,dimension>(tensor, grid, 2);
    harmonic_potential<double,dimension>(tensor, grid); 
    tensor.complete();

    // Reference Tensor
    auto ref_tensor = DenseTensor<double, dimension>(shape);
    kinetic_FD<double,dimension>(ref_tensor, grid, 2);
    harmonic_potential<double,dimension>(ref_tensor, grid);
    const size_t num_of_grid = x_dim*y_dim*z_dim;
    std::vector<double> exact_eigen_value(num_of_grid);
    //std::vector<double> exact_eigen_vector  = ref_tensor.data;
    Matrix<double> exact_eigen_vector(num_of_grid, num_of_grid,ref_tensor.data);
    //LAPACKE_dsyevd(LAPACK_COL_MAJOR, 'V', 'L',  num_of_grid, exact_eigen_vector.data(), num_of_grid, exact_eigen_value.data());  // Eigen values in ascending order.
    LAPACKE_dsyevd(LAPACK_COL_MAJOR, 'V', 'L',  num_of_grid, exact_eigen_vector.pointer.data(), num_of_grid, exact_eigen_value.data());  // Eigen values in ascending order.

/////////////////////////////////////////////////////////////////
//// Decomposition ////
    for (size_t rank = 1; rank < shape[0]+1; rank++) {
        std::array<size_t, dimension> ranks = {rank, rank, rank, rank, rank, rank};

        std::cout << "========== dim of core tensor: (" << rank << ", " << rank << ", " << rank
        << ", " << rank << ", " << rank << ", " << rank << ") ==========" << std::endl;

        const size_t num_of_decomposed_grid = rank * rank * rank;
// Tucker decomposition //
        auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
        //std::vector<double> eigen_value(num_of_decomposed_grid);
        std::vector<double> eigen_value(0);
        //Matrix<double> eigen_vector(num_of_decomposed_grid, num_of_decomposed_grid);
        Matrix<double> eigen_vector(num_of_grid, num_of_decomposed_grid);

        // Diagonalization //
        //const size_t num_eig = std::min(int(rank*rank*rank), 10);
        tucker.diagonalize(eigen_value, eigen_vector.pointer, num_of_decomposed_grid);

        /*
        Matrix<double> restored_eigen_vector(num_of_grid, num_of_decomposed_grid);

        std::vector <DenseTensor<double, 3>> restored_eigen_vector_tmp(num_of_decomposed_grid);
        for (size_t i = 0; i < num_of_decomposed_grid; i++){
            DenseTensor<double, 3> eigen_vector_tensor({rank, rank, rank}, eigen_vector.get_column_vector(i));
            restored_eigen_vector_tmp[i] = DenseTensor<double, 3>({x_dim, y_dim, z_dim});
            tucker.to_original_space( eigen_vector_tensor, restored_eigen_vector_tmp[i] );
        }
        // restored_eigen_vector_tmp -> restored_eigen_vector //
        for (size_t i = 0; i < num_of_grid; i++){
            for (size_t j = 0; j < num_of_decomposed_grid; j++){
                restored_eigen_vector(i, j) = restored_eigen_vector_tmp[j][i];
            }
        }
        */

                bool PRINT_EIGEN_VALUE = false; //true;
                if (PRINT_EIGEN_VALUE){
                    std::cout << "eigen_value = ";
                    for (size_t i = 0; i < eigen_value.size(); i++){
                        std::cout << eigen_value[i] << " ";
                    } std::cout << std::endl;
                    std::cout << "exact_eigen_value = ";
                    for (size_t i = 0; i < exact_eigen_value.size(); i++){
                        std::cout << exact_eigen_value[i] << " ";
                    } std::cout << std::endl;
                }
                bool PRINT_EIGEN_VECTOR = false;
                if (PRINT_EIGEN_VECTOR){
                    std::cout << "restored_eigen_vector = " << eigen_vector << std::endl;
                    std::cout << "exact_eigen_vector = " << exact_eigen_vector << std::endl;
                }
                bool PRINT_restored_eigen_vector_error = true;//false;//true;
                if(PRINT_restored_eigen_vector_error){
                    double error = 0.0;
                    for (size_t j = 0; j < num_of_decomposed_grid; j++){    // row
                        double error_per_one_column = 0.0;
                        for (size_t i = 0; i < num_of_grid; i++){           // column
                            //size_t combined_index = i + num_of_grid*j;
                            //size_t combined_index = i + num_of_grid*0;
                            //error += (restored_eigen_vector[0][i] - exact_eigen_vector[i]);
                            //error += (restored_eigen_vector[0][i] + exact_eigen_vector[i]);
                            //error += std::abs( ( std::abs(restored_eigen_vector[0][i]) ) - ( std::abs(exact_eigen_vector(i, 0)) ) );
                            //error_per_one_column += std::abs( ( std::abs(restored_eigen_vector[j][i]) ) - ( std::abs(exact_eigen_vector(i, j)) ) );
                            error_per_one_column += std::abs( ( std::abs(eigen_vector(i, j)) ) - ( std::abs(exact_eigen_vector(i, j)) ) );
                        }
                        error += error_per_one_column;
                    }
                    //std::cout << "error = " << error << std::endl;
                    std::cout << "error (mean for the grid) = " << error / (num_of_grid * num_of_decomposed_grid) << std::endl;
                }
         
                bool PRINT_overlap = true;
                if (PRINT_overlap){
                    Matrix<double> VTxV;
                    VTxV.multiply(eigen_vector, eigen_vector, "T", "NoT");
                    std::cout << std::fixed;
                    std::cout.precision(3);
                    ///std::cout << "VTxV = " << VTxV << std::endl;

                    Matrix<double> VTxV_cross;
                    VTxV_cross.multiply(eigen_vector, exact_eigen_vector, "T", "NoT");
                    /*
                     * restored_eigen_vector : (num_of_grid x num_of_decomposed_grid)
                     * exact_eigen_vector : (num_of_grid x num_of_grid)
                     * VTxV_cross : (num_of_decomposed_grid x num_of_grid)
                     */
                    ///std::cout << "VTxV_cross = " << VTxV_cross << std::endl;

                    // calculate row sum //
                    //std::vector<double> row_square_sum(VTxV_cross.get_col());
                    std::vector<double> row_square_sum(num_of_decomposed_grid);
                    //for (size_t i = 0; i < VTxV_cross.get_col(); i++){
                    for (size_t i = 0; i < num_of_decomposed_grid; i++){
                        double row_square_sum_val = 0.0;
                        std::vector<double> column_vector = VTxV_cross.get_column_vector(i);
                        for (size_t j = 0; j < VTxV_cross.get_row(); j++){
                            row_square_sum_val += column_vector[j] * column_vector[j];
                        }
                        row_square_sum[i] = row_square_sum_val;
                    }
                    std::cout << "row_square_sum = ";
                    for (size_t i = 0; i < row_square_sum.size(); i++){
                        std::cout << row_square_sum[i] << " ";
                    } std::cout << std::endl;
                }
    } std::cout << std::endl;
    
    return 0;
}
