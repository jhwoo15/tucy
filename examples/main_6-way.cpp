#include <iostream>
#include <vector>
#include "Tucker.hpp"
#include <cmath>
#include <ctime>

size_t main() {
    std::cout << "Tucker Decomposition START!!" << std::endl << std::endl;
    clock_t start, end;
    clock_t start1 = clock();

    bool PRINT_ORIGINAL_TENSOR = false;
    bool DECOMPOSITION_TEST = true;     bool PRINT_DECOMPOSITION = false;
    bool PRINT_RESTORED_TENSOR = false;
    //bool TUCKER_TO_TENSOR_TEST = true;

   
    // Input Setting //
    const size_t dimension = 6;
    std::vector<size_t> shape = {10, 10, 10, 10, 10, 10};
    size_t total_dim = shape[0]*shape[1]*shape[2]*shape[3]*shape[4]*shape[5];
    double* tensor = new double[total_dim];
    for (size_t i = 0; i < shape[0]; i++) { for (size_t j = 0; j < shape[1]; j++) { for(size_t k = 0; k < shape[2]; k ++) {
        if (i == j && j == k) { tensor[i + shape[0]*j + shape[0]*shape[1]*k] = dimension-i; }}}}
    //for (size_t i = 0; i < total_dim; i++) { tensor[i] = std::rand()/(double(RAND_MAX)); }

if (DECOMPOSITION_TEST) {
    for (size_t rank = 1; rank < shape[0]+1; rank++) {
        std::cout << "[rank = " << rank << "]" << std::endl;
// Tucker decomposition //
        std::vector<size_t> ranks = {rank, rank, rank, rank, rank, rank};
        Tucker<double, dimension> tucker(shape, tensor);
        if (PRINT_ORIGINAL_TENSOR) {
            std::cout << "Original tensor = " << std::endl;
            for (size_t i = 0; i < tucker.shape_mult[dimension]; i++) {
                std::cout << tucker.tensor[i] << " ";
            } std::cout << std::endl << std::endl;
        }
        start = clock();
        tucker.hosvd(ranks);
        end = clock();
        std::cout << "HOSVD Time : "; 
        prsize_tf("%0.5fs\n", (double)(end - start)/double(CLOCKS_PER_SEC));
 
// Prsize_t core tensor & Us //
        if (PRINT_DECOMPOSITION) { 
            std::cout << "core tensor = " << std::endl;
            for (size_t i = 0; i < tucker.ranks_mult[dimension]; i++) {
                std::cout << tucker.core[i] << " ";
            } std::cout << std::endl;
            for (size_t dim = 0; dim < dimension; dim++) {
                std::cout << "Us[" << dim << "] = " << std::endl;
                (tucker.Us[dim]).prsize_t_matrix();
                //std::cout << tucker.Us[dim] << std::endl;
            } std::cout << std::endl;
        }

// Tucker to tensor //
        std::cout << "Tucker to Tensor" << std::endl;
        double* restored_tensor = new double[total_dim];
        tucker.tucker_to_tensor(restored_tensor);
        if (PRINT_RESTORED_TENSOR) {
            for (size_t i = 0; i < total_dim; i++) {
                std::cout << restored_tensor[i] << " ";
            } std::cout << std::endl << std::endl;;
        }
  
// Calculate Error Norm //
        double norm = 0.0;
        double total_sum = 0.0;
        for (size_t i = 0; i < total_dim; i++) {
            // Frobenius norm
            norm += pow((tensor[i] - restored_tensor[i]), 2);
            total_sum += tucker.tensor[i];
        }
        //std::cout << "total_sum = " << total_sum << std::endl;
        std::cout << "Norm = " << sqrt(norm) << std::endl;
        
        delete[] restored_tensor;
    } std::cout << std::endl;
}
    delete[] tensor;
    clock_t end1 = clock();
    std::cout << "total time : " << double(end1 - start1)/double(CLOCKS_PER_SEC) << " s" << std::endl;
}
