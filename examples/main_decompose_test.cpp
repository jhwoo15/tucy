#include <iostream>
#include "Grid.hpp"
#include <array>
//#include "TuckerMethod.hpp"
#include <ctime>
#include "helper.hpp"
#include "Util.hpp"
using namespace TensorDecompose;


int main(int argc, char *argv[]) {
    if (argc != 7){
        std::cout << "Input error!!\n(Usage) ./exe 'x_dim' 'y_dim' 'z_dim' 'x_rank' 'y_rank' 'z_rank'" << std::endl;
        exit(-1);
    }

    double start, end, start1, end1;
    size_t x_dim = atoi(argv[1]);
    size_t y_dim = atoi(argv[2]);
    size_t z_dim = atoi(argv[3]);
    const size_t total_dim = x_dim*y_dim*z_dim*x_dim*y_dim*z_dim;

    std::array<double, 3> scaling = {1, 1, 1};
    Grid grid(x_dim, y_dim, z_dim, scaling);
    const size_t dimension = 6;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim, x_dim, y_dim, z_dim};
    const size_t num_grid = shape[0] * shape[1] * shape[2];

    start = omp_get_wtime();

    auto tensor = SparseTensor<double, dimension>(shape);
    //kinetic_FD<double, dimension>(tensor, grid);
    //harmonic_potential<double, dimension>(tensor, grid);
    std::array<size_t, 3> shape_3d = {x_dim, y_dim, z_dim};
    for (size_t i = 0; i < num_grid; i++){
        auto index = get_tensor_index(i, shape_3d);
        //auto index = get_tensor_index<3>(i, {x_dim, y_dim, z_dim});
        std::array<size_t, 6> index_6d = {index[0], index[1], index[2], index[0], index[1], index[2]};
        double value = double(i) / double(num_grid);
        tensor.insert_value(index_6d, value);
    }
    tensor.complete();
    
    end = omp_get_wtime();
    std::cout << "initialization time : " << end - start << " s" << std::endl;
    
 ////////////////////////////////////////////////////////////////
//////////////////////// Decomposition /////////////////////////
    size_t x_rank = atoi(argv[4]);
    size_t y_rank = atoi(argv[5]);
    size_t z_rank = atoi(argv[6]);
    size_t rank_mult = x_rank * y_rank * z_rank;
    std::array<size_t, dimension> ranks = {x_rank, y_rank, z_rank, x_rank, y_rank, z_rank};

    std::cout << "========== dim of core tensor: (" << x_rank << ", " << y_rank << ", " << z_rank
    << ", " << x_rank << ", " << y_rank << ", " << z_rank << ") ==========" << std::endl;

// Tucker decomposition //
    start = omp_get_wtime();
//    auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
    //auto tucker_ref = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
    std::cout << "tensor = " << tensor << std::endl;


    Tucker<double, dimension> tucker(shape, ranks);
    std::vector<size_t> row(tensor.data.size());
    std::vector<size_t> col(tensor.data.size());
    std::vector<double> val(tensor.data.size());
    for (size_t i = 0; i < tensor.data.size(); i++){
        std::array<size_t, 6> indx = tensor.data[i].first;
//        std::cout << "indx = (" << indx[0] << " " << indx[1] << " " << indx[2] << " "
//                               << indx[3] << " " << indx[4] << " " << indx[5] << ") ";
        row[i] = indx[0] + shape[0]*indx[1] + shape[0]*shape[1]*indx[2];
        col[i] = indx[3] + shape[3]*indx[4] + shape[3]*shape[4]*indx[5];
//        row[i] = indx[2] + shape[2]*indx[1] + shape[2]*shape[1]*indx[0];
//        col[i] = indx[5] + shape[5]*indx[4] + shape[5]*shape[4]*indx[3];
        val[i] = tensor.data[i].second;
//        std::cout << "row = " << row[i] << ", col = " << col[i] << ", val = " << val[i] << std::endl;
    }
    tucker.decompose( row, col, val, true );

    end = omp_get_wtime();
    std::cout << "HOSVD Time : " << end - start <<"s" <<std::endl; 

// Project kinetic and local potential //
    auto tucker_project = Tucker<double, dimension>(shape, ranks);
    for (size_t dim = 0; dim < dimension; dim++){
        tucker_project.Us[dim] = tucker.Us[dim];
    }
    const size_t stencil = 2;
    Matrix<double> T_xx, T_yy, T_zz;
    kinetic_FD_matrix<double, dimension>(grid, T_xx, T_yy, T_zz, stencil);
    start = omp_get_wtime();
    tucker_project.project_kinetic_matrix(shape[0], shape[1], shape[2],
                                          T_xx.pointer, T_yy.pointer, T_zz.pointer);
    end = omp_get_wtime();
    std::cout << "Time (project_kinetic) : " << end - start << std::endl;
    const size_t project_axis_level = 3;
    std::vector<double> local_potential(x_dim * y_dim * z_dim);
//    harmonic_potential_grid<double, 3>(grid, local_potential);
    for (size_t i = 0; i < num_grid; i++){
        local_potential[i] = double(i) / double(num_grid);
        std::cout << local_potential[i] << " ";
    }
    start = omp_get_wtime();
    //tucker_project.project_local_potential(local_potential, project_axis_level);
    end = omp_get_wtime();
    std::cout << "Time (project_local_potential) : " << end - start << std::endl;

    //
    bool PRINT_CORE_DIFF = true;
    if (PRINT_CORE_DIFF){
        double core_error_square = 0.0;
        #pragma omp parallel for reduction(+:core_error_square)
        for (size_t i = 0; i < rank_mult; i++){
            core_error_square += pow((tucker.core[i] - tucker_project.core[i]), 2);
        }
        std::cout << "RMSE = " << sqrt(core_error_square) << std::endl;
        for (size_t i = 0; i < rank_mult; i++){
            if ( abs(tucker.core[i] - tucker_project.core[i]) > 1E-9 ){
                std::cout << "tucker.core[" << i << "] = " << tucker.core[i] << std::endl;
                std::cout << "tucker_project.core[" << i << "] = " << tucker_project.core[i] << std::endl;
            }
        }
    }

    end1 = omp_get_wtime();
    std::cout << std::endl;
    std::cout << "total time : " << end1 - start1 << " s" << std::endl;
    return 0;
}
