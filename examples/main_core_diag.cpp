#include <iostream>
#include "Grid.hpp"
#include <array>
#include "TuckerMethod.hpp"
#include <cmath>
#include <ctime>
#include "helper.hpp"
#include <omp.h>
#include <algorithm>
using namespace TensorDecompose;

int main(int argc, char *argv[]) {
    if (argc != 7){
        std::cout << "Input error!!\n(Usage) ./exe 'x_dim' 'y_dim' 'z_dim' 'x_rank' 'y_rank' 'z_rank'" << std::endl;
        exit(-1);
    }

    double start1, end1, start2, end2, start_time_chk, end_time_chk;
    //size_t dim = 12;
    //size_t dim = atoi(argv[1]);
    size_t x_dim = atoi(argv[1]);
    size_t y_dim = atoi(argv[2]);
    size_t z_dim = atoi(argv[3]);
    #pragma omp parallel
    {
        #pragma omp master
        std::cout << "omp_num_threads : " << omp_get_num_threads() << std::endl;
        #pragma omp master
        std::cout << "(x_dim, y_dim, z_dim) : (" << x_dim << ", " << y_dim << ", " << z_dim << ")" << std::endl;
    }
    //size_t x_dim, y_dim, z_dim;
    //x_dim = dim; y_dim = dim; z_dim = dim;
    std::array<double, 3> scaling = {0.22, 0.22, 0.22};
    Grid grid(x_dim, y_dim, z_dim, scaling);
    const size_t dimension = 6;
    const size_t total_dim = x_dim * y_dim * z_dim * x_dim * y_dim * z_dim;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim, x_dim, y_dim, z_dim};
    
    start1 = omp_get_wtime();
    
    auto tensor = SparseTensor<double, dimension>(shape);
    kinetic_FD<double, dimension>(tensor, grid, 2);
    harmonic_potential<double, dimension>(tensor, grid);
    tensor.complete();
    end1 = omp_get_wtime();
    std::cout << "initialization time : " << end1 - start1 << " s" << std::endl;

//        // Exact solution //
//        auto ref_tensor = DenseTensor<double, dimension>(shape);
//        kinetic_FD<double, dimension>(ref_tensor, grid, 2);
//        harmonic_potential<double, dimension>(ref_tensor, grid);
//        
//        const size_t num_of_grid = x_dim*y_dim*z_dim;
//        std::vector<double> exact_eigen_values(num_of_grid);
//        std::vector<double> exact_eigen_vectors  = ref_tensor.data;
//        start_time_chk = omp_get_wtime();   // Check Diagonalize Time.
//        LAPACKE_dsyevd(LAPACK_COL_MAJOR, 'V', 'L',  num_of_grid, exact_eigen_vectors.data(), num_of_grid, exact_eigen_values.data());  // Eigen values in ascending order.
//        end_time_chk = omp_get_wtime();     // Check Diagonalize Time.
//        std::cout << "Diagonalize time (Full rank Hamiltonian) : " << end_time_chk - start_time_chk << " s" << std::endl;

    start1 = omp_get_wtime();
    
/////////////////////////////////////////////////////////////////
//    for (size_t rank = 1; rank < shape[0] + 1; rank++) {
//    for (size_t rank = atoi(argv[2]); rank < atoi(argv[3])+1; rank++){
        //std::array<size_t, dimension> ranks = {rank, rank, rank, rank, rank, rank};
        size_t x_rank = atoi(argv[4]);
        size_t y_rank = atoi(argv[5]);
        size_t z_rank = atoi(argv[6]);
        size_t rank_mult = x_rank * y_rank * z_rank;
        std::array<size_t, dimension> ranks = {x_rank, y_rank, z_rank, x_rank, y_rank, z_rank};

        std::cout << "========== dim of core tensor: (" << x_rank << ", " << y_rank << ", " << z_rank
        << ", " << x_rank << ", " << y_rank << ", " << z_rank << ") ==========" << std::endl;

// Tucker decomposition //
        start2 = omp_get_wtime();
        auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
        end2 = omp_get_wtime();
        std::cout << "HOSVD Time : " << end2 - start2 <<"s" <<std::endl; 
// Check eigenvalues //
        std::vector<double> eigen_values(x_rank*y_rank*z_rank);

        start_time_chk = omp_get_wtime();   // Check Diagonalize Time.
        const size_t eig_num = rank_mult;
        tucker.diagonalize(eigen_values, eig_num);
        end_time_chk = omp_get_wtime();     // Check Diagonalize Time.
        std::cout << "Diagonalize time (core tensor) : " << end_time_chk - start_time_chk << " s" << std::endl;
        
        // Iterative Diagonalization //
        start_time_chk = omp_get_wtime();
        std::vector<double> eval_iter;
        std::vector<double> evec_iter;
        diagonalize_iter<double,dimension>(tensor, eval_iter, evec_iter, eig_num);
        end_time_chk = omp_get_wtime();
        std::cout << "Diagonalize time (iter) : " << end_time_chk - start_time_chk << " s" << std::endl;

        bool PRINT_EIGENVALUES = false;//true;
        if(PRINT_EIGENVALUES){
            std::cout << "decomposed_eigenvalues = " << std::endl;
            //for (size_t i = 0; i < std::min(int(rank*rank*rank), 10); i++){
            for (size_t i = 0; i < rank_mult; i++){
                std::cout << std::fixed;
                std::cout.precision(7);
                std::cout << eigen_values[i] <<"\t" ;
            } std::cout << std::endl;

            std::cout << "iterative_digenvalues = " << std::endl;
            for (size_t i = 0; i < rank_mult; i++){
                std::cout << std::fixed;
                std::cout.precision(7);
                std::cout << eval_iter[i] <<"\t" ;
            } std::cout << std::endl;

        }

            bool PRINT_EIGEN_DIFF_ITER = true;
            if(PRINT_EIGEN_DIFF_ITER){
                std::cout << "eigenvalues diff (with iterative) = " << std::endl;
                for (size_t i = 0; i < std::min(int(rank_mult), 10); i++){
                    std::cout << std::fixed;
                    std::cout.precision(7);
                    std::cout << eigen_values[i] - eval_iter[i] <<"\t" ;
                }
                std::cout << std::endl << std::endl;;
            }
            
//            // Diff from exact solution.
//            bool PRINT_EIGEN_DIFF = true;
//            if(PRINT_EIGEN_DIFF){
//                std::cout << "eigenvalues diff = " << std::endl;
//                for (size_t i = 0; i < std::min(int(rank*rank*rank), 10); i++){
//                    std::cout << std::fixed;
//                    std::cout.precision(7);
//                    std::cout << eigen_values[i] - exact_eigen_values[i] <<"\t" ;
//                }
//                std::cout << std::endl;
//            }

//    } std::cout << std::endl;
            bool PRINT_Us = false;
            if (PRINT_Us){
                std::cout << "Print Us :" << std::endl;
                for (size_t dim = 0; dim < dimension; dim++){
                    std::cout << "Us[" << dim << "] = " << tucker.Us[dim] << std::endl;
                }
            }
    
    end1 = omp_get_wtime();
    std::cout << "total time : " << end1 - start1 << " s" << std::endl;
    return 0;
}
