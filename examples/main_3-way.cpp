#include <iostream>
#include "Grid.hpp"
#include <array>
#include "Tucker.hpp"
#include "TuckerMethod.hpp"
#include <cmath>
#include <omp.h>

using namespace TensorDecompose;

int main() {
    double start1, end1, start2, end2;
    size_t dim = 10;
    //size_t dim = 15;
    size_t x_dim, y_dim, z_dim;
    x_dim = dim; y_dim = dim; z_dim = dim;
    //x_dim = 2; y_dim = 3; z_dim = 4;
    const size_t dimension = 3;
    const size_t total_dim = x_dim * y_dim * z_dim;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim};
    //auto tensor = DenseTensor<double, dimension>(shape);
    std::vector<double> data(total_dim);
    for (int i = 0; i < total_dim; i++) { data[i] = i; }
    auto tensor = DenseTensor<double, dimension>(shape, data);
                bool PRINT_ORIGINAL_TENSOR = 0;
                if (PRINT_ORIGINAL_TENSOR) {
                    std::cout << "Original tensor = " << std::endl;
                    for (size_t i = 0; i < tensor.shape[0]; ++i){
                        for (size_t j = 0; j < tensor.shape[1]; ++j){
                            for (size_t k = 0; k < tensor.shape[2]; ++k){
                                std::cout << tensor.operator()({i,j,k}) << " ";
                            } std::cout << std::endl;
                        } std::cout << std::endl;
                    } std::cout << std::endl;
                }
    
    start1 = omp_get_wtime();
    for (size_t rank = 1; rank < shape[0]+1; rank++) {
        std::array<size_t, dimension> ranks = {rank, rank, rank};
        //std::array<size_t, dimension> ranks = {shape[0], shape[1], shape[2]};

        std::cout << "========== dim of core tensor: (" << rank << ", " << rank << ", " << rank
        << ", " << rank << ", " << rank << ", " << rank << ") ==========" << std::endl;

// Tucker decomposition //
        
        start2 = omp_get_wtime();
        auto tucker = TuckerMethod<double, dimension>::hosvd(tensor, ranks);
        end2 = omp_get_wtime();
        std::cout << "HOSVD Time : " << end2 - start2 <<"s" <<std::endl; 
 
// Tucker to tensor //
        //std::cout << "Tucker to Tensor" << std::endl;
        std::vector<double> restored_tensor;
        tucker.tucker_to_tensor(restored_tensor);
  
// Calculate Error Norm //
        std::vector<double> error = tensor.data;
        cblas_daxpy( error.size(), -1.0, restored_tensor.data(), 1, error.data(),1);
        auto norm = cblas_dnrm2(error.size(),error.data(), 1 );
        std::cout << "Error norm = " <<norm << std::endl;

        bool PRINT_U = 0;
        if (PRINT_U == 1){
            std::cout << "Print U" << std::endl;
            for (int i = 0; i < dimension; i++){
                std::cout << "U[" << i << "] =" << tucker.Us[i] << std::endl;
            }

            for (int i = 0; i < tucker.ranks_mult[dimension]; i++){
                std::cout << tucker.core[i] << " ";
            } std::cout << std::endl;
        }

    } std::cout << std::endl;
    
    end1 = omp_get_wtime();
    std::cout << "total time : " << end1 - start1 << " s" << std::endl;
    return 0;
}
