#pragma once
#include <iostream>
#include <vector>
#include <array>

class Grid{
    public:
        int dim_x, dim_y, dim_z;
        int total_dim;
        std::array<int, 3> points;
        std::array<double, 3> scaling;
        std::array<std::vector<double>, 3> grid;
        std::array<std::vector<double>, 3> scaled_grid;
        void compute_grid();
        void compute_scaled_grid();

        int periodicity;
        //int periodicity = 0;
    
    public:
        Grid(int dim_x, int dim_y, int dim_z, std::array<double, 3> scaling);

        int combine(int i_x, int i_y, int i_z, std::array<int, 3> points);

        void decompose(int index, std::array<int, 3> points, int& i_x, int& i_y, int& i_z);
        
        //void to_tensors(vector<int> vectors);
};

Grid::Grid(int dim_x, int dim_y, int dim_z, std::array<double, 3> scaling){
    this -> dim_x = dim_x;
    this -> dim_y = dim_y;
    this -> dim_z = dim_z;
    this -> total_dim = dim_x * dim_y * dim_z;
    this -> points = {dim_x, dim_y, dim_z};
    this -> scaling = scaling;
    
    this -> periodicity = 0;

    this->compute_grid();
    this->compute_scaled_grid();
};

int Grid::combine(int i_x,int i_y, int i_z, std::array<int,3> points){
    int return_val =i_x+points[0]*(i_y+points[1]*i_z);
    if (return_val>=total_dim){
        // XXX TODO Error terminate it.
        //throw std::invalid_argument("Grid_Basic::combine index out of range!");
        return -return_val;
    }
    //std::cout << i_x << " " << points[0] << " " << i_y << " " << points[1] << " " << i_z << " " << points[2] << std::endl;
    if(i_x < 0 or i_x >= points[0] or i_y < 0 or i_y >= points[1] or i_z < 0 or i_z >= points[2]){
        // XXX TODO Error terminate it.
        //throw std::invalid_argument("Grid_Basic::combine index out of range!");
        return -2;
    }
    return return_val;
}

void Grid::decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z){
    if(index < 0 or index >= total_dim){
        std::cout << "Grid_Basic::decompose index " << index << " out of range!" << std::endl;
        throw std::invalid_argument("Grid_Basic::decompose index out of range!");
    }
    i_x = index%points[0];
    int tmp = index/points[0];
    i_y = tmp%points[1];
    i_z = tmp/points[1];
}

void Grid::compute_grid(){
    if ( periodicity==0 ){
        for (int i=0;i<3;i++){
            this->grid[i].resize(points[i]);
        }
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                this->grid[i][j]=-0.5*(double((points[i]-1)))+j;
            }
        }
    }
    else{
        for (int i=0;i<3;i++){
            this->grid[i].resize(points[i]);
        }
        for (int i =0;i<3;i++){
            for (int j =0;j<points[i];j++){
                this->grid[i][j]=-0.5*(double((points[i])))+j;
            }
        }
    }
}

void Grid::compute_scaled_grid(){
    for (int i=0;i<3;i++){
        this->scaled_grid[i].resize(points[i]);
    }

    for(int i =0; i<3; i++){
        for(int j=0;j<points[i];j++){
            this->scaled_grid[i][j] = this->grid[i][j] * this->scaling[i];
            //scaled_grid[i][j] = grid[i][j] * scaling[i];
        }
    }
}


