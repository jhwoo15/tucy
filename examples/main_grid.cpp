#include <iostream>
#include "Grid.hpp"
#include "Grid.cpp"
#include <array>

int main() {
    std::cout << "== Start 'Grid' class Test ==" << std::endl;

    int x_dim, y_dim, z_dim;
    x_dim = 4; y_dim = 4; z_dim = 4;
    std::array<double, 3> scaling = {1., 1., 1.};
    //std::array<double, 3> scaling = {2, 2, 2};
    Grid grid(x_dim, y_dim, z_dim, scaling);
    

    std::cout << "scaled_grid = " << std::endl;
    for (int i = 0; i < 3; ++i){
        for (int j = 0; j < grid.points[i]; ++j){
            std::cout << grid.scaled_grid[i][j] << " ";
        } std::cout << std::endl;
    } std::cout << std::endl;


    std::cout << "== End 'Grid' class Test ==" << std::endl;
}
