#include <iostream>
#include "Grid.hpp"
#include <array>
#include "Tucker.hpp"
#include "TuckerMethod.hpp"
#include <cmath>
#include <omp.h>
#include "helper.hpp"

using namespace TensorDecompose;

int main() {
    double start_total, start;
    double end_total, end;

    size_t dim = 10;
    //size_t dim = 15;
    size_t x_dim, y_dim, z_dim;
//    x_dim = dim; y_dim = dim; z_dim = dim;
    x_dim = dim; y_dim = dim+1; z_dim = dim+2;

    const size_t dimension = 3;
    const size_t total_dim = x_dim * y_dim * z_dim;
    std::array<size_t, dimension> shape = {x_dim, y_dim, z_dim};

    auto dense_tensor = DenseTensor<double, dimension>(shape);
    dense_tensor.random();
                // Print tensor //
                bool PRINT_ORIGINAL_TENSOR = false;
                if (PRINT_ORIGINAL_TENSOR) {
                    std::cout << "Original tensor = " << std::endl;
                    for (size_t i = 0; i < dense_tensor.shape[0]; ++i){
                        for (size_t j = 0; j < dense_tensor.shape[1]; ++j){
                            for (size_t k = 0; k < dense_tensor.shape[2]; ++k){
                                std::cout << dense_tensor.operator()({i,j,k}) << " ";
                            } std::cout << std::endl;
                        } std::cout << std::endl;
                    } std::cout << std::endl;
                }

    auto sparse_tensor = dense_to_sparse(dense_tensor);
                // Print sparse_tensor values //
                bool PRINT_SPARSE_TENSOR = false;
                if (PRINT_SPARSE_TENSOR){
                    for (size_t i = 0; i < sparse_tensor.data.size(); i++){
                        std::cout << sparse_tensor.data[i].second << " ";
                        //std::vector<std::pair<std::array<size_t, dimension>, datatype>> data;
                    } std::cout << std::endl;
                }
    
    start_total = omp_get_wtime(); // Start Decomposition
    for (size_t rank = 1; rank < shape[0]+1; rank++) {
        std::array<size_t, dimension> ranks = {rank, rank, rank};

        std::cout << "========== dim of core tensor: (" << rank << ", " << rank << ", " << rank << ") ==========" << std::endl;

// Tucker decomposition //
        start = omp_get_wtime();
        auto tucker_from_dense = TuckerMethod<double, dimension>::hosvd(dense_tensor, ranks);
        end = omp_get_wtime();
        std::cout << "HOSVD Time (dense) : " << end - start << "s" << std::endl;
        start = omp_get_wtime();
        auto tucker_from_sparse = TuckerMethod<double, dimension>::hosvd(sparse_tensor, ranks);
        end = omp_get_wtime();
        std::cout << "HOSVD Time (sparse) : " << end - start << "s" << std::endl;
 
// Tucker to tensor //
        //std::cout << "Tucker to Tensor" << std::endl;
        std::vector<double> restored_tensor_from_dense;
        std::vector<double> restored_tensor_from_sparse;
        start = omp_get_wtime();
        tucker_from_dense.tucker_to_tensor(restored_tensor_from_dense);
        end = omp_get_wtime();
        std::cout << "Tucker to Tensor Time(dense) : " << end - start << "s" << std::endl;
        start = omp_get_wtime();
        tucker_from_sparse.tucker_to_tensor(restored_tensor_from_sparse);
        end = omp_get_wtime();
        std::cout << "Tucker to Tensor Time(sparse) : " << end - start << "s" << std::endl;
        
  
// Calculate Error Norm //
        std::vector<double> error_from_dense = dense_tensor.data;
        std::vector<double> error_from_sparse = dense_tensor.data;
        cblas_daxpy( error_from_dense.size(), -1.0, restored_tensor_from_dense.data(), 1, error_from_dense.data(),1);
        cblas_daxpy( error_from_sparse.size(), -1.0, restored_tensor_from_sparse.data(), 1, error_from_sparse.data(),1);
        auto norm_from_dense = cblas_dnrm2(error_from_dense.size(),error_from_dense.data(), 1 );
        auto norm_from_sparse = cblas_dnrm2(error_from_sparse.size(),error_from_sparse.data(), 1 );
        std::cout << "Error norm (dense) = " << norm_from_dense << std::endl;
        std::cout << "Error norm (sparse) = " << norm_from_sparse << std::endl;

        bool PRINT_U = false;
        if (PRINT_U){
            std::cout << "Print U (dense)" << std::endl;
            for (int i = 0; i < dimension; i++){
                std::cout << "U[" << i << "] =" << tucker_from_dense.Us[i] << std::endl;
            }
            std::cout << "Print U (sparse)" << std::endl;
            for (int i = 0; i < dimension; i++){
                std::cout << "U[" << i << "] =" << tucker_from_sparse.Us[i] << std::endl;
            }
        }
        bool PRINT_CORE = false;
        if (PRINT_CORE){
            std::cout << "Print core (dense)" << std::endl;
            for (int i = 0; i < tucker_from_dense.ranks_mult[dimension]; i++){
                std::cout << tucker_from_dense.core[i] << " ";
            } std::cout << std::endl;
            std::cout << "Print core (sparse)" << std::endl;
            for (int i = 0; i < tucker_from_sparse.ranks_mult[dimension]; i++){
                std::cout << tucker_from_sparse.core[i] << " ";
            } std::cout << std::endl;
        }

    } std::cout << std::endl;
    
    end_total = omp_get_wtime();
    std::cout << "total time : " << end_total - start_total << " s" << std::endl;
    return 0;
}
