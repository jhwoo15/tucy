#pragma once
#include <iostream>
#include <iomanip>
#include <string>
#include "mkl_wrapper.hpp"
#include <cassert>
#include <vector>
#include <type_traits>

namespace TensorDecompose{
template <class datatype>
class Matrix {
public:
    Matrix(){};
    Matrix(size_t row, size_t column);
    Matrix(size_t row, size_t column, std::vector<datatype> tensor);
    ~Matrix(){};

    // varibales
    size_t row = 0;
    size_t column = 0;
    std::vector<datatype> pointer;

    // Utils
    size_t get_row() const { return this->row; };
    size_t get_col() const { return this->column; };
    void print_shape() const;
    void print_matrix() const;
    Matrix<datatype> conj() const;

    datatype& operator[](size_t index);
    Matrix<datatype>& operator=(const Matrix<datatype>& matrix);
    Matrix<datatype> operator*(const Matrix<datatype>& matrix) const;
    Matrix<datatype> operator*(const datatype number) const;
    Matrix<datatype> operator+(const Matrix<datatype>& matrix) const;
    Matrix<datatype> operator-(const Matrix<datatype>& matrix) const;
    datatype& operator()(const size_t& i, const size_t& j);

    std::vector<datatype> get_column_vector(size_t column);
    std::vector<datatype> get_row_vector(size_t column);
    Matrix<datatype>& multiply(const Matrix<datatype>& matrix1, const Matrix<datatype>& matrix2, std::string trans1, std::string trans2);
};

template <class datatype>
std::ostream& operator<<(std::ostream& os, Matrix<datatype>& M);

template <class datatype>
Matrix<datatype> Map(std::vector<datatype>& tensor, size_t row, size_t column);

template <class datatype>
Matrix<datatype>::Matrix(size_t row, size_t column){
    this->row = row;
    this->column = column;
    pointer.resize(row * column, 0);
}

template <class datatype>
Matrix<datatype>::Matrix(size_t row, size_t column, std::vector<datatype> tensor){
    assert(tensor.size() == row * column);
    this->row = row;
    this->column = column;
    pointer = tensor;
}

template <class datatype>
void Matrix<datatype>::print_shape() const{
    std::cout << "(" << this->row << "-by-" << this->column << ")" << std::endl;
}

template <class datatype>
void Matrix<datatype>::print_matrix() const{
    for(size_t i = 0; i < this->row; ++i){
        for(size_t j = 0; j < this->column; ++j){
            std::cout << this->pointer[i + j*this->row] << " ";
        } std::cout << std::endl;
    } std::cout << std::endl;
}

template <class datatype>
Matrix<datatype>& Matrix<datatype>::multiply(const Matrix<datatype>& matrix1, const Matrix<datatype>& matrix2, std::string trans1, std::string trans2){
    /*
    (Usage example)
    Matrix<double> Mat3;
    Mat3.multiply(Mat1, Mat2, "NoT", "NoT");
    */
    if ( !((trans1 == "T" or trans1 == "NoT") && (trans2 == "T" or trans2 == "NoT")) ) {
        std::cout << "Matrix::multipy : Wrong input parameter." << std::endl;
        exit(-1);
    }
    size_t row1, column1, row2, column2;
    if (trans1 == "NoT") { row1 = matrix1.get_row(); column1 = matrix1.get_col(); }
    else { row1 =  matrix1.get_col(); column1 = matrix1.get_row(); }
    if (trans2 == "NoT") { row2 = matrix2.get_row(); column2 = matrix2.get_col(); }
    else { row2 = matrix2.get_col(); column2 = matrix2.get_row(); }
    if (column1 != row2) {
        std::cout << "Matrix::multipy : Matrix Shape Error! (";
        std::cout << "Can't multiply " << row1 << "-by-" << column1 << " matrix with " << row2 << "-by-" << column2 << " matrix.)" << std::endl;
        exit(-1);
    }

    this->row = row1; this->column = column2;
    this->pointer.resize(this->row * this->column,0);

    if(trans1 == "T" && trans2 == "T"){
        gemm<datatype>(CblasColMajor, CblasTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), column2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "T" && trans2 == "NoT"){
        gemm<datatype>(CblasColMajor, CblasTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), row2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "NoT" && trans2 == "T"){
        gemm<datatype>(CblasColMajor, CblasNoTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), column2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "NoT" && trans2 == "NoT"){
        gemm<datatype>(CblasColMajor, CblasNoTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), row2, 0.0, this->pointer.data(), this->get_row());
    }
    return *this;
}

template <>
Matrix<double> Matrix<double>::conj() const{
    Matrix<double> return_matrix(this->row, this->column);
    return_matrix.pointer = this->pointer;
    return return_matrix;
}

template <>
Matrix<std::complex<double>> Matrix<std::complex<double>>::conj() const{
    Matrix<std::complex<double>> return_matrix(this->row, this->column);
    const size_t size = this->pointer.size();
    #pragma omp parallel for
    for (size_t i = 0; i < size; i++){
        return_matrix[i] = std::conj(this->pointer[i]);
    }
    return return_matrix;
}

template <class datatype>
Matrix<datatype>& Matrix<datatype>::operator=(const Matrix<datatype>& matrix){
    this->pointer = matrix.pointer;
    this->row = matrix.row;
    this->column = matrix.column;

    return *this;
}

template <class datatype>
datatype& Matrix<datatype>::operator()(const size_t& i, const size_t& j){
    return this->pointer[i + this->row*j];
}

template <class datatype>
std::ostream& operator<<(std::ostream& os, Matrix<datatype>& M){
    os << std::endl;
    for(size_t i = 0; i < M.get_row(); ++i){
        for(size_t j = 0; j < M.get_col(); ++j){
            os << std::setw(10) << M(i,j) << " ";
            //os << std::setw(10) << round(M(i,j)*1E5)/1E5 << " ";
        } os << std::endl;
    }
    return os;
}

template <class datatype>
Matrix<datatype> Map(std::vector<datatype>& tensor, size_t row, size_t column){
    Matrix<datatype> mapped_matrix(row, column, tensor);
    return mapped_matrix;
}

template <class datatype>
std::vector<datatype> Matrix<datatype>::get_column_vector(size_t column){
    std::vector<datatype> return_vector(this->row);
    for (size_t i = 0; i < this->row; i++){
        return_vector[i] = this->operator()(i, column);
    }
    return return_vector;
}

template <class datatype>
std::vector<datatype> Matrix<datatype>::get_row_vector(size_t row){
    std::vector<datatype> return_vector(this->column);
    for (size_t i = 0; i < this->column; i++){
        return_vector[i] = this->operator()(row, i);
    }
    return return_vector;
}

template <class datatype>
Matrix<datatype> Matrix<datatype>::operator*(const Matrix<datatype> &matrix) const{
    // Matrix multiplication
    Matrix<datatype> return_matrix;
    return_matrix.multiply(*this, matrix, "NoT", "NoT");

    return return_matrix;
}

template <class datatype>
Matrix<datatype> Matrix<datatype>::operator*(const datatype number) const{
    Matrix<datatype> return_matrix(this->row, this->column);
    const size_t size = this->pointer.size();
    #pragma omp parallel for
    for (size_t i = 0; i < size; i++){
        return_matrix[i] = this->pointer[i] * number;
    }
    return return_matrix;
}

template <class datatype>
Matrix<datatype> Matrix<datatype>::operator+(const Matrix<datatype> &matrix) const{
    assert(this->row == matrix.row && this->column == matrix.column);
    Matrix<datatype> return_matrix(this->row, this->column);
    for (size_t i = 0; i < this->pointer.size(); i++){
        return_matrix.pointer[i] = (this->pointer[i] + matrix.pointer[i]);
    }
    return return_matrix;
}

template <class datatype>
Matrix<datatype> Matrix<datatype>::operator-(const Matrix<datatype> &matrix) const{
    assert(this->row == matrix.row && this->column == matrix.column);
    Matrix<datatype> return_matrix(this->row, this->column);
    for (size_t i = 0; i < this->pointer.size(); i++){
        return_matrix.pointer[i] = (this->pointer[i] - matrix.pointer[i]);
    }
    return return_matrix;
}

template <class datatype>
datatype& Matrix<datatype>::operator[](size_t index){
    return this->pointer[index];
}
}
