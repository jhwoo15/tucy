#pragma once
#include <array>
#include <string>
#include "Matrix.hpp"
#include <cassert>
#include "mkl_wrapper.hpp"

namespace TensorDecompose{

template <size_t dimension>
void cumprod(const std::array<size_t, dimension>& shape, std::array<size_t, dimension+1>& shape_mult, std::string indexing="F");
template <class datatype, size_t dimension>
void to_next_leading_axis(Matrix<datatype>& M, Matrix<datatype>& M_proj, std::array<size_t, dimension> shape1, std::array<size_t, dimension> shape2, size_t dim);

template <class datatype>
void sym_eigen_solver(const Matrix<datatype>& M, Matrix<datatype>& U, std::vector<double>& eigen_values);
template <class datatype>
void sym_eigen_solver(const Matrix<datatype>& M, Matrix<datatype>& U);

template <size_t dimension>
std::array<size_t, dimension> get_tensor_index(const size_t& input_index, const std::array<size_t, dimension>& shape, const std::string indexing="F");
template <size_t dimension>
std::array<size_t, dimension> get_tensor_index(const size_t& input_index, const std::array<size_t, dimension+1>& shape_mult, const std::string indexing="F");

template <class datatype>
Matrix<datatype> multiply(const Matrix<datatype>& matrix1, const Matrix<datatype>& matrix2, std::string trans1, std::string trans2);
//Matrix<double> multiply(const Matrix<double>& matrix1, const Matrix<double>& matrix2, std::string trans1, std::string trans2);

template <size_t dimension>
void cumprod(const std::array<size_t, dimension>& shape, std::array<size_t, dimension+1>& shape_mult, std::string indexing){
    /* Ex1)
     * shape = {2, 3, 4}, indexing="F"
     * shape_mult = {1, 2, 6, 24}
     * Ex2)
     * shape = {2, 3, 4}, indexing="C"
     * shape_mult = {1, 4, 12, 24}
     */
    shape_mult[0] = 1;
    if (indexing == "F")
        for (size_t i = 0; i < dimension; ++i) {
            shape_mult[i+1] = shape_mult[i] * shape[i];
        }
    else if(indexing == "C"){
        for (size_t i = 0; i < dimension; ++i) {
            shape_mult[i+1] = shape_mult[i] * shape[dimension-i-1];
        }
    }
}

template <class datatype, size_t dimension>
void to_next_leading_axis(Matrix<datatype>& M, Matrix<datatype>& M_proj, std::array<size_t, dimension> shape1, std::array<size_t, dimension> shape2, size_t dim){
    /*
     * If shape1 == shape and shape2 == ranks, Tensor to Tucker ( to_next_leading_axis(M, M_proj, shape, ranks, dim) )
     * If shape1 == ranks and shape2 == shape, Tucker to Tensor ( to_next_leading_axis(M, M_proj, ranks, shape, dim) )
     */

    // Make mult
    std::array<size_t, dimension+1> shape1_mult;
    std::array<size_t, dimension+1> shape2_mult;
    cumprod<dimension>(shape1, shape1_mult);
    cumprod<dimension>(shape2, shape2_mult);

    // projection to next leading axis
    M = Matrix<datatype>(shape1[dim], shape2_mult[dim]*shape1_mult[dimension]/shape1_mult[dim+1]);
    for (size_t j = 0; j < M_proj.get_col(); ++j) {
        size_t write_i = (j/shape2_mult[dim-1]) % shape1[dim];
        size_t base_write_j = j%shape2_mult[dim-1] + j/(shape2_mult[dim-1]*shape1[dim])*shape2_mult[dim];
        for (size_t i = 0; i < M_proj.get_row(); ++i) {
            M(write_i, base_write_j + i*shape2_mult[dim-1]) = M_proj(i, j);      // generating Memory Copy
        }
    }
}

template <class datatype>
void sym_eigen_solver(const Matrix<datatype>& M, Matrix<datatype>& U){
    // Change to 'dgesvd'.
    // sym_eigen_solver -> svd? get_left_singular_vector


    // Make symmetry matrix, then solve eigen problem.
    // M * M^T = U S U^T

    // 1. Make symmetric matrix
    // herk<datatype>(CblasColMajor, CblasLower, CblasNoTrans,
    //                M.get_row(), M.get_col(), 1., M.pointer.data(), M.get_row(), 0, U.pointer.data(), U.get_row());

    // 2. Eigen decomposition
    datatype* eigen_values = new datatype[M.get_row()];
    heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', M.get_row(), U.pointer.data(), M.get_row(), eigen_values);  // Eigen values in ascending order.
    
    delete[] eigen_values;
}

template<class datatype>
void sym_eigen_solver(const Matrix<datatype>& M, Matrix<datatype>& U, std::vector<double>& eigen_values){
    // Make symmetry matrix, then solve eigen problem.
    // M * M^T = U S U^T

//    // 1. Make symmetric matrix
//    cblas_dsyrk(CblasColMajor, CblasLower, CblasNoTrans,
//                M.get_row(), M.get_col(), 1., M.pointer.data(), M.get_row(), 0, U.pointer.data(), U.get_row());
    U.pointer = M.pointer;

    // 2. Eigen decomposition
    eigen_values.resize(M.get_row());
    heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', M.get_row(), U.pointer.data(), M.get_row(), eigen_values.data());  // Eigen values in ascending order.

}

template <size_t dimension>
std::array<size_t, dimension> get_tensor_index(const size_t& input_index, const std::array<size_t, dimension>& shape, const std::string indexing){
    /* If indexing="F",
     * (0, 0, 0), (1, 0, 0), (2, 0, 0), ..., (0, 1, 0), (1, 1, 0), ...
     * If indexing="C",
     * (0, 0, 0), (0, 0, 1), (0, 0, 2), ..., (0, 1, 0), (0, 1, 1), ...
     */
    std::array<size_t, dimension+1> shape_mult;
    cumprod<dimension>(shape, shape_mult, indexing);

    size_t combined_index = input_index;
    std::array<size_t, dimension> return_index;
    if (indexing == "F"){
        #pragma unroll
        for (size_t dim = 0; dim < dimension; ++dim){
            const size_t curr_dim = dimension - dim - 1;
            return_index[curr_dim] = combined_index / shape_mult[curr_dim];
            combined_index = (combined_index % shape_mult[curr_dim]);
        }
    }
    else if (indexing == "C"){
        #pragma unroll
        for (size_t dim = 0; dim < dimension; ++dim){
            return_index[dim] = combined_index / shape_mult[dimension-dim-1];
            combined_index = (combined_index % shape_mult[dimension-dim-1]);
        }
    }
    else{
        assert(0);
    }
    return return_index;
}


template <size_t dimension>
std::array<size_t, dimension> get_tensor_index(const size_t& input_index, const std::array<size_t, dimension+1>& shape_mult, const std::string indexing){
    /* If indexing="F",
     * (0, 0, 0), (1, 0, 0), (2, 0, 0), ..., (0, 1, 0), (1, 1, 0), ...
     * If indexing="C",
     * (0, 0, 0), (0, 0, 1), (0, 0, 2), ..., (0, 1, 0), (0, 1, 1), ...
     */

    size_t combined_index = input_index;
    std::array<size_t, dimension> return_index;
    if (indexing == "F"){
        #pragma unroll
        for (size_t dim = 0; dim < dimension; ++dim){
            const size_t curr_dim = dimension - dim - 1;
            return_index[curr_dim] = combined_index / shape_mult[curr_dim];
            combined_index = (combined_index % shape_mult[curr_dim]);
        }
    }
    else if (indexing == "C"){
        #pragma unroll
        for (size_t dim = 0; dim < dimension; ++dim){
            return_index[dim] = combined_index / shape_mult[dimension-dim-1];
            combined_index = (combined_index % shape_mult[dimension-dim-1]);
        }
    }
    else{
        assert(0);
    }
    return return_index;
}



template <class datatype>
Matrix<datatype> multiply(const Matrix<datatype>& matrix1, const Matrix<datatype>& matrix2, std::string trans1, std::string trans2){

    if ( !((trans1 == "T" or trans1 == "NoT") && (trans2 == "T" or trans2 == "NoT")) ) {
        std::cout << "Matrix::multipy : Wrong input parameter." << std::endl;
        exit(-1);
    }
    size_t row1, column1, row2, column2;
    if (trans1 == "NoT") { row1 = matrix1.get_row(); column1 = matrix1.get_col(); }
    else { row1 =  matrix1.get_col(); column1 = matrix1.get_row(); }
    if (trans2 == "NoT") { row2 = matrix2.get_row(); column2 = matrix2.get_col(); }
    else { row2 = matrix2.get_col(); column2 = matrix2.get_row(); }
    if (column1 != row2) {
        std::cout << "Matrix::multipy : Matrix Shape Error! (";
        std::cout << "Can't multiply " << row1 << "-by-" << column1 << " matrix with " << row2 << "-by-" << column2 << " matrix.)" << std::endl;
        exit(-1);
    }

    Matrix<datatype> return_matrix(row1, column2);

    if(trans1 == "T" && trans2 == "T") {
        gemm<datatype>(CblasColMajor, CblasTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), column2, 0.0, return_matrix.pointer.data(), return_matrix.get_row());
    } else if(trans1 == "T" && trans2 == "NoT") {
        gemm<datatype>(CblasColMajor, CblasTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), row2, 0.0, return_matrix.pointer.data(), return_matrix.get_row());
    } else if(trans1 == "NoT" && trans2 == "T") {
        gemm<datatype>(CblasColMajor, CblasNoTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), column2, 0.0, return_matrix.pointer.data(), return_matrix.get_row());
    } else if(trans1 == "NoT" && trans2 == "NoT") {
        gemm<datatype>(CblasColMajor, CblasNoTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), row2, 0.0, return_matrix.pointer.data(), return_matrix.get_row());
    }

    ///return *this;
    return return_matrix;
}
/*
template <class datatype>
Matrix<datatype>& Matrix<datatype>::multiply(const Matrix<datatype>& matrix1, const Matrix<datatype>& matrix2, std::string trans1, std::string trans2){
    
//    (Usage example)
//    Matrix<double> Mat3;
//    Mat3.multiply(Mat1, Mat2, "NoT", "NoT");
    
    if ( !((trans1 == "T" or trans1 == "NoT") && (trans2 == "T" or trans2 == "NoT")) ) {
        std::cout << "Matrix::multipy : Wrong input parameter." << std::endl;
        exit(-1);
    }
    size_t row1, column1, row2, column2;
    if (trans1 == "NoT") { row1 = matrix1.get_row(); column1 = matrix1.get_col(); }
    else { row1 =  matrix1.get_col(); column1 = matrix1.get_row(); }
    if (trans2 == "NoT") { row2 = matrix2.get_row(); column2 = matrix2.get_col(); }
    else { row2 = matrix2.get_col(); column2 = matrix2.get_row(); }
    if (column1 != row2) {
        std::cout << "Matrix::multipy : Matrix Shape Error! (";
        std::cout << "Can't multiply " << row1 << "-by-" << column1 << " matrix with " << row2 << "-by-" << column2 << " matrix.)" << std::endl;
        exit(-1);
    }

    this->row = row1; this->column = column2;
    this->pointer.resize(this->row * this->column,0);

    if(trans1 == "T" && trans2 == "T") {
        cblas_dgemm(CblasColMajor, CblasTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), column2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "T" && trans2 == "NoT") {
        cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), column1, matrix2.pointer.data(), row2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "NoT" && trans2 == "T") {
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), column2, 0.0, this->pointer.data(), this->get_row());
    } else if(trans1 == "NoT" && trans2 == "NoT") {
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, row1, column2, column1,
                    1.0, matrix1.pointer.data(), row1, matrix2.pointer.data(), row2, 0.0, this->pointer.data(), this->get_row());
    }

    return *this;
}
 */
}
