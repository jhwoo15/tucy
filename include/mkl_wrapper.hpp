#pragma once 
#include <complex>
#define MKL_Complex16 std::complex<double>
#define MKL_Complex8  std::complex<float>
#include "mkl.h"
#include <type_traits>

// templated version of mkl wrapper but only it works with only std::complex<double> and double types. 
// This header contains flowing function 
//  *SPARSE 
//     sparse_create_csr
//     sparse_create_csc
//     sparse_spmmd
//     sparse_export_csr
//  *ETC
//     omatadd
//  *LAPACKE
//     heevd(syevd)
//  *BLAS
//     axpy
//     gemm
//     herk (syrk)
//     ger  (gerc)
//  *BLAS extention
//     axpy_batch

template<class T>
struct get_core_type
{typedef T value_type;};
template<class T>
struct get_core_type<std::complex<T>> 
{typedef T value_type;};

namespace TensorDecompose{
/////////////////////////////////////////////////////////////////////// MKL SPARSE FUNCTION
template<class datatype>
sparse_status_t sparse_create_csr( sparse_matrix_t        *A,
                                   sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                   MKL_INT    rows,
                                   MKL_INT    cols,
                                   MKL_INT    *rows_start,
                                   MKL_INT    *rows_end,
                                   MKL_INT    *col_indx,
                                   datatype   *values )
{ static_assert(false,"This is not implemented yet");  };
template<>
sparse_status_t sparse_create_csr<double>( sparse_matrix_t        *A,
                                           sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                           MKL_INT    rows,
                                           MKL_INT    cols,
                                           MKL_INT    *rows_start,
                                           MKL_INT    *rows_end,
                                           MKL_INT    *col_indx,
                                           double   *values )
{ return mkl_sparse_d_create_csr( A, indexing, rows, cols, rows_start, rows_end, col_indx, values ); };

template<>
sparse_status_t sparse_create_csr<std::complex<double> >( sparse_matrix_t        *A,
                                                          sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                                          MKL_INT    rows,
                                                          MKL_INT    cols,
                                                          MKL_INT    *rows_start,
                                                          MKL_INT    *rows_end,
                                                          MKL_INT    *col_indx,
                                                          std::complex<double>   *values )
{ return mkl_sparse_z_create_csr( A, indexing, rows, cols, rows_start, rows_end, col_indx, values ); };

template<class datatype>
sparse_status_t sparse_create_csc( sparse_matrix_t        *A,
                                   sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                   MKL_INT    rows,
                                   MKL_INT    cols,
                                   MKL_INT    *rows_start,
                                   MKL_INT    *rows_end,
                                   MKL_INT    *col_indx,
                                   datatype   *values )
{ static_assert(false,"This is not implemented yet");  };

template<>
sparse_status_t sparse_create_csc<double>( sparse_matrix_t        *A,
                                             sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                             MKL_INT    rows,
                                             MKL_INT    cols,
                                             MKL_INT    *rows_start,
                                             MKL_INT    *rows_end,
                                             MKL_INT    *col_indx,
                                             double   *values )
{ return mkl_sparse_d_create_csc(A, indexing, rows, cols, rows_start, rows_end, col_indx, values);  };

template<>
sparse_status_t sparse_create_csc<std::complex<double>>( sparse_matrix_t        *A,
                                             sparse_index_base_t    indexing, /* indexing: C-style or Fortran-style */
                                             MKL_INT    rows,
                                             MKL_INT    cols,
                                             MKL_INT    *rows_start,
                                             MKL_INT    *rows_end,
                                             MKL_INT    *col_indx,
                                             std::complex<double>   *values )
{ return mkl_sparse_z_create_csc(A, indexing, rows, cols, rows_start, rows_end, col_indx, values);  };


template<class datatype>
sparse_status_t sparse_spmmd( sparse_operation_t      operation,
                                  const sparse_matrix_t   A,
                                  const sparse_matrix_t   B,
                                  sparse_layout_t  layout,       /* storage scheme for the output dense matrix: C-style or Fortran-style */
                                  datatype           *C,
                                  MKL_INT          ldc )
{ static_assert(false,"This is not implemented yet");  };


template<>
sparse_status_t sparse_spmmd<double>( sparse_operation_t      operation,
                                            const sparse_matrix_t   A,
                                            const sparse_matrix_t   B,
                                            sparse_layout_t  layout,       /* storage scheme for the output dense matrix: C-style or Fortran-style */
                                            double           *C,
                                            MKL_INT          ldc )
{ return mkl_sparse_d_spmmd(operation, A, B, layout, C, ldc);  };


template<>
sparse_status_t sparse_spmmd<std::complex<double>>( sparse_operation_t      operation,
                                            const sparse_matrix_t   A,
                                            const sparse_matrix_t   B,
                                            sparse_layout_t  layout,       /* storage scheme for the output dense matrix: C-style or Fortran-style */
                                            std::complex<double>           *C,
                                            MKL_INT          ldc )
{ return mkl_sparse_z_spmmd(operation, A, B, layout, C, ldc);  };

template<class datatype>
sparse_status_t  sparse_export_csr(const sparse_matrix_t source, sparse_index_base_t *indexing, MKL_INT *rows, MKL_INT *cols, MKL_INT **rows_start, MKL_INT **rows_end, MKL_INT **col_indx, datatype **values)
{return mkl_sparse_d_export_csr( source, indexing, rows, cols, rows_start, rows_end, col_indx, values);}

template<>
sparse_status_t  sparse_export_csr<double>(const sparse_matrix_t source, sparse_index_base_t *indexing, MKL_INT *rows, MKL_INT *cols, MKL_INT **rows_start, MKL_INT **rows_end, MKL_INT **col_indx, double **values)
{return mkl_sparse_d_export_csr( source, indexing, rows, cols, rows_start, rows_end, col_indx, values);}

template<>
sparse_status_t  sparse_export_csr<std::complex<double> >(const sparse_matrix_t source, sparse_index_base_t *indexing, MKL_INT *rows, MKL_INT *cols, MKL_INT **rows_start, MKL_INT **rows_end, MKL_INT **col_indx, std::complex<double> **values)
{return mkl_sparse_z_export_csr( source, indexing, rows, cols, rows_start, rows_end, col_indx, values);}

/////////////////////////////////////////////////////////////////////// MKL FUNCTION
template<class datatype>
void omatadd(
    char ordering, char transa, char transb,
    size_t rows, size_t cols,
    const datatype alpha,
    const datatype * A, size_t lda,
    const datatype beta,
    const datatype * B, size_t ldb,
    datatype * C, size_t ldc)
{ static_assert(false,"This is not implemented yet");  };


template<>
void omatadd<double>(
    char ordering, char transa, char transb,
    size_t rows, size_t cols,
    const double alpha,
    const double * A, size_t lda,
    const double beta,
    const double * B, size_t ldb,
    double * C, size_t ldc)
{ return mkl_domatadd(ordering,transa,transb,rows,cols,alpha,A,lda,beta,B,ldb,C,ldc);  };


template<>
void omatadd<std::complex<double> >(
    char ordering, char transa, char transb,
    size_t rows, size_t cols,
    const std::complex<double> alpha,
    const std::complex<double> * A, size_t lda,
    const std::complex<double> beta,
    const std::complex<double> * B, size_t ldb,
    std::complex<double> * C, size_t ldc)
{ return mkl_zomatadd(ordering,transa,transb,rows,cols,alpha,A,lda,beta,B,ldb,C,ldc);  };


//////////////////////////////////////////////////////////////////// LAPACKE FUNCTION

//template<class datatype, typename = void>
//lapack_int LAPACKE_heevd<datatype>( int matrix_layout, char jobz, char uplo, lapack_int n, datatype* a, lapack_int lda, datatype * w )
//{ return LAPACKE_dsyevd(matrix_layout, jobz, uplo, n, a, lda, w); }
//
//template<class datatype >
//lapack_int LAPACKE_heevd<datatype, std::void_t<typename datatype::value_type> >( int matrix_layout, char jobz, char uplo, lapack_int n, datatype* a, lapack_int lda, datatype::value_type * w )
//{ return LAPACKE_zheevd(matrix_layout, jobz, uplo, n, a, lda, w); }

//template<typename datatype1, typename datatype2= typename get_core_type<datatype1>::value_type >
//lapack_int heevd( int matrix_layout, char jobz, char uplo, lapack_int n, datatype1* a, lapack_int lda, datatype2* w )
//{  static_assert(false,"This is not implemented yet"); }
template<typename datatype>
lapack_int heevd( int matrix_layout, char jobz, char uplo, lapack_int n, datatype* a, lapack_int lda, double* w )
{  static_assert(false,"This is not implemented yet"); }

//template<>
//lapack_int heevd<float,float>( int matrix_layout, char jobz, char uplo, lapack_int n, float* a, lapack_int lda, float * w )
//{ return LAPACKE_ssyevd(matrix_layout, jobz, uplo, n, a, lda, w); }

template<>
lapack_int heevd<double>( int matrix_layout, char jobz, char uplo, lapack_int n, double* a, lapack_int lda, double * w )
{ return LAPACKE_dsyevd(matrix_layout, jobz, uplo, n, a, lda, w); }

//template<>
//lapack_int heevd<std::complex<float>,float >( int matrix_layout, char jobz, char uplo, lapack_int n, std::complex<float>* a, lapack_int lda, float* w )
//{ return LAPACKE_cheevd(matrix_layout, jobz, uplo, n, a, lda, w); }

template<>
lapack_int heevd<std::complex<double>>( int matrix_layout, char jobz, char uplo, lapack_int n, std::complex<double>* a, lapack_int lda, double* w )
{ return LAPACKE_zheevd(matrix_layout, jobz, uplo, n, a, lda, w); }

///////////////////////////////////////////////////////////////////  BLAS FUNCTION
template<class datatype >
void axpy(const MKL_INT n, const datatype a, const datatype *x, const MKL_INT incx, datatype *y, const MKL_INT incy)
{  static_assert(false,"This is not implemented yet"); }

template<>
void axpy<double>(const MKL_INT n, const double a, const double *x, const MKL_INT incx, double *y, const MKL_INT incy)
{ return cblas_daxpy ( n,  a, x, incx, y, incy); }

template<>
void axpy<std::complex<double> > (const MKL_INT n, const std::complex<double> a, const std::complex<double> *x, const MKL_INT incx, std::complex<double> *y, const MKL_INT incy)
{ return cblas_zaxpy ( n,  (void*) &a, (void*) x, incx, (void*) y, incy); }

template<class datatype >
void gemm(const CBLAS_LAYOUT Layout, const CBLAS_TRANSPOSE transa, const CBLAS_TRANSPOSE transb, const MKL_INT m, const MKL_INT n, const MKL_INT k, const datatype alpha, const datatype *a, const MKL_INT lda, const datatype *b, const MKL_INT ldb, const datatype beta, datatype *c, const MKL_INT ldc)
{  static_assert(false,"This is not implemented yet"); }

template<>
void gemm<double>(const CBLAS_LAYOUT Layout, const CBLAS_TRANSPOSE transa, const CBLAS_TRANSPOSE transb, const MKL_INT m, const MKL_INT n, const MKL_INT k, const double alpha, const double *a, const MKL_INT lda, const double *b, const MKL_INT ldb, const double beta, double *c, const MKL_INT ldc)
{ return cblas_dgemm(Layout, transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc); }

template<>
void gemm<std::complex<double> >(const CBLAS_LAYOUT Layout, const CBLAS_TRANSPOSE transa, const CBLAS_TRANSPOSE transb, const MKL_INT m, const MKL_INT n, const MKL_INT k, const std::complex<double> alpha, const std::complex<double> *a, const MKL_INT lda, const std::complex<double> *b, const MKL_INT ldb, const std::complex<double> beta, std::complex<double> *c, const MKL_INT ldc)
{ return cblas_zgemm(Layout, transa, transb, m, n, k, (const void*) &alpha, (const void*) a, lda, (const void*) b, ldb, (const void*) &beta, (void*) c, ldc); }

template<class datatype >
void herk(const CBLAS_LAYOUT Layout, const CBLAS_UPLO uplo, const CBLAS_TRANSPOSE trans, const MKL_INT n, const MKL_INT k, const double alpha, const datatype* a, const MKL_INT lda, const double beta, datatype* c, const MKL_INT ldc)
{  static_assert(false,"This is not implemented yet"); }

template<>
void herk<double  >(const CBLAS_LAYOUT Layout, const CBLAS_UPLO uplo, const CBLAS_TRANSPOSE trans, const MKL_INT n, const MKL_INT k, const double alpha, const double*   a, const MKL_INT lda, const double beta, double*   c, const MKL_INT ldc)
{ return cblas_dsyrk(Layout, uplo, trans, n, k, alpha, a, lda, beta, c, ldc);}

template<>
void herk<std::complex<double> >(const CBLAS_LAYOUT Layout, const CBLAS_UPLO uplo, const CBLAS_TRANSPOSE trans, const MKL_INT n, const MKL_INT k, const double alpha, const std::complex<double> *a, const MKL_INT lda, const double beta, std::complex<double> *c, const MKL_INT ldc)
{ return cblas_zherk(Layout, uplo, trans, n, k, alpha, (const void*) a, lda, beta, (void*) c, ldc);}

template<class datatype>
void ger(const CBLAS_LAYOUT Layout, const MKL_INT m, const MKL_INT n, const datatype alpha, const datatype* x, const MKL_INT incx, const datatype* y, const MKL_INT incy, datatype* a, const MKL_INT lda)
{  static_assert(false,"This is not implemented yet"); }
template<>
void ger<double> (const CBLAS_LAYOUT Layout, const MKL_INT m, const MKL_INT n, const double alpha, const double* x, const MKL_INT incx, const double* y, const MKL_INT incy, double* a, const MKL_INT lda)
{ return cblas_dger( Layout, m, n, alpha, x, incx, y, incy, a, lda); }

template<>
void ger<std::complex<double> >(const CBLAS_LAYOUT Layout, const MKL_INT m, const MKL_INT n, const std::complex<double> alpha, const std::complex<double>* x, const MKL_INT incx, const std::complex<double>* y, const MKL_INT incy, std::complex<double>* a, const MKL_INT lda)
{ return cblas_zgerc( Layout, m, n, (void*) &alpha, (const void*) x, incx, (const void*) y, incy, (void*) a, lda); }
//SparseTensor.hpp:    auto info = mkl_sparse_d_ev(&which, pm, A, descr, k0, &k, eigen_values.data(), eigen_vectors.data(), res);
//
template<class datatype>
void axpy_batch(const MKL_INT *n_array, const datatype *alpha_array, const datatype** x_array, const MKL_INT *incx_array, datatype** y_array, const MKL_INT *incy_array, const MKL_INT group_count, const MKL_INT *group_size_array)
{ static_assert(false,"This is not implemented yet"); }

//template<>
//void axpy_batch<double>(const MKL_INT *n_array, const double *alpha_array, const double **x_array, const MKL_INT *incx_array, double **y_array, const MKL_INT *incy_array, const MKL_INT group_count, const MKL_INT *group_size_array)
//{ return cblas_daxpy_batch(n_array, alpha_array, x_array, incx_array, y_array, incy_array, group_count, group_size_array); }
//
//template<>
//void axpy_batch<std::complex<double> >(const MKL_INT *n_array, const std::complex<double> *alpha_array, const std::complex<double> **x_array, const MKL_INT *incx_array, std::complex<double> **y_array, const MKL_INT *incy_array, const MKL_INT group_count, const MKL_INT *group_size_array)
//{ return cblas_zaxpy_batch(n_array, (const void*) alpha_array, (const void**) x_array, incx_array, (void**) y_array, incy_array, group_count, group_size_array); }

}
