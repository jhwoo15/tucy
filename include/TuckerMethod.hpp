#pragma once
#include <iostream>
#include <array>
#include <vector>
#include <cmath>
#include "Matrix.hpp"
#include "Util.hpp"
#include "Tucker.hpp"
#include "DenseTensor.hpp"
#include "SparseTensor.hpp"
#include "mkl_solvers_ee.h"
#include <algorithm>
#include <omp.h>

namespace TensorDecompose{
template <class datatype, size_t dimension>
class TuckerMethod {
public:
    static Tucker<datatype, dimension> hosvd(DenseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks);
    static Tucker<datatype, dimension> hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core=true);
    static Tucker<datatype, dimension> hosvd_hamiltonian(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, size_t project_axis_level);
protected:
    static void get_left_singular_vector(const Matrix<datatype>& M, Matrix<datatype>& U, const size_t rank);
    static int get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype>& Bx, Matrix<datatype>& U, const size_t rank); 

};

template <class datatype, size_t dimension>
Tucker<datatype, dimension> TuckerMethod<datatype, dimension>::hosvd(DenseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks) {
    // Store information about ranks, and set memory size of core tensor.
    auto tucker = Tucker<datatype, dimension>(tensor.shape, ranks);

    assert(tucker.ranks_mult[dimension] != 0);

    // First unfolding: special case (elements are already arranged as we want)
    auto M = Map(tensor.data, tensor.shape[0], tensor.shape_mult[dimension]/tensor.shape[0]);
    Matrix<datatype> M_proj;
    get_left_singular_vector(M, tucker.Us[0], tucker.ranks[0]);
    M_proj = multiply(tucker.Us[0], M, "T", "NoT");

    for (size_t dim = 1; dim < dimension; ++dim) {
        to_next_leading_axis(M, M_proj, tucker.shape, tucker.ranks, dim);
        get_left_singular_vector(M, tucker.Us[dim], tucker.ranks[dim]);
        M_proj = multiply(tucker.Us[dim], M, "T", "NoT");
    }

    // Fold back from matrix size_to N-dimensional tensor
    for (size_t i = 0; i < tucker.ranks[dimension-1]; i++) {
        for (size_t j = 0; j < tucker.ranks_mult[dimension-1]; j++) {
            tucker.core[i*tucker.ranks_mult[dimension-1] + j] = M_proj(i, j);
        }
    }
    return tucker;
}

template <class datatype, size_t dimension>
void TuckerMethod<datatype, dimension>::get_left_singular_vector(const Matrix<datatype>& M, Matrix<datatype>& U, const size_t rank) {
    size_t row = M.get_row();
    U = Matrix<datatype>(row, rank);
    Matrix<datatype> U_unsorted(row, row);

    sym_eigen_solver(M, U_unsorted);

    // Sort U in desending order.
    for(size_t i = 0; i < row; ++i){
        for(size_t j = 0; j < rank; ++j){
            U(i,j) = U_unsorted(i,row-j-1);
        }
    }
}

Tucker<double, 6> TuckerMethod<double, 6>::hosvd_hamiltonian(SparseTensor<double, 6>& tensor, std::array<size_t, 6> ranks, size_t project_axis_level=3){
    // If projector_axis_level is 3, x, y, z -axes U matrices are calculated.
    // If projector_axis_level is 2, x, y    -axes U matrices are calculated.
    // If projector_axis_level is 1, x       -axis U matrix   is  calcualted.
    // Using Symmetry
    const size_t dimension = 6;
    double start, end, start_time, end_time;
    bool TIME_CHECK = true;
    if (TIME_CHECK){
        std::cout << "== hosvd hamiltonian time check ==" << std::endl;
        start_time = omp_get_wtime();
    }

    if (TIME_CHECK) { start = omp_get_wtime(); }
    // Store information about ranks, and set memory size of core tensor.
    auto tucker = Tucker<double, dimension>(tensor.shape, ranks);
    assert(tucker.ranks_mult[dimension] != 0);

//    #pragma omp parallel for
    for (size_t dim = 0; dim < project_axis_level; dim++){
        std::vector<int> Bp;
        std::vector<int> Bj;
        std::vector<double> Bx;
        tensor.to_csr(dim, Bp, Bj, Bx);
        get_left_singular_vector(tensor.shape[dim], tensor.shape_mult[dimension]/tensor.shape[dim], Bp, Bj, Bx, tucker.Us[dim], tucker.ranks[dim]);
    }
    if (TIME_CHECK) {
        end = omp_get_wtime();
        std::cout << "|\tstep 1 (SVD): " << end - start << " s" << std::endl;
    }
    std::cout << "Not yet implemented." << std::endl;
    assert(false && "Not yet implemented.");
    return tucker;
}

template <class datatype, size_t dimension>
Tucker<datatype, dimension> TuckerMethod<datatype, dimension>::hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core){
    /*
    If 'calculate_core' == false, only U matrices are calculated.

    (Usage example)
    auto tucker = hosvd(tensor, ranks, true);
    */
    double start, end, start_time, end_time;
    bool TIME_CHECK = true;
    if (TIME_CHECK){
        std::cout << "== hosvd (SparseTensor) time check ==" << std::endl;
        start_time = omp_get_wtime();
    }

    if (TIME_CHECK) { start = omp_get_wtime(); }
    // Store information about ranks, and set memory size of core tensor.
    auto tucker = Tucker<datatype, dimension>(tensor.shape, ranks);
    assert(tucker.ranks_mult[dimension] != 0);

//    int n_core;
//    #pragma omp parallel
//    {
//        #pragma omp master
//        n_core = omp_get_num_threads();
//    }
//    mkl_set_num_threads( n_core / 6);
//    #pragma omp parallel for num_threads(6)
    #pragma omp parallel for
    for (size_t dim = 0; dim < dimension; ++dim) {
        std::vector<int> Bp;
        std::vector<int> Bj;
        std::vector<datatype> Bx;
        tensor.to_csr(dim, Bp, Bj, Bx);
        get_left_singular_vector(tensor.shape[dim], tensor.shape_mult[dimension]/tensor.shape[dim], Bp, Bj, Bx, tucker.Us[dim], tucker.ranks[dim]);
    }
    if (TIME_CHECK) {
        end = omp_get_wtime();
        std::cout << "|\tstep 1 (SVD): " << end - start << " s" << std::endl;
    }
    if (calculate_core == false){
        return tucker;
    }

    if (TIME_CHECK) { start = omp_get_wtime(); }
/*
    // calculate column major index from the combined index
    #pragma omp parallel for
    for (size_t i=0; i<tucker.ranks_mult[dimension] ; i++){ 
        const auto index = get_tensor_index<dimension>(i, tucker.ranks );
        for (auto iter = tensor.data.begin(); iter!=tensor.data.end(); iter++){
            datatype val = (*iter).second;
            #pragma unroll
            for (size_t j = 0; j < dimension; j++){
                val*=tucker.Us[j]((*iter).first[j] , index[j] );
            }
            tucker.core[i]+=val;
        }
    }
    */
/////
    const std::vector<std::pair<std::array<size_t, dimension>, datatype>>& data = tensor.data;
    const size_t size_k = data.size();
    const size_t size_i = tucker.ranks_mult[dimension];
    //std::cout << "size_i: " <<size_i <<std::endl;
    #pragma omp parallel for // collapse(2)
    for (size_t i=0; i<size_i; i++){
        const auto index = get_tensor_index(i, tucker.ranks );
        for (size_t k=0; k<size_k; k++){
            datatype val = data[k].second;
            #pragma unroll
            for (size_t j=0; j<dimension ; j++){
                val*=tucker.Us[j](data[k].first[j] , index[j] );
            }
//            #pragma omp atomic
             tucker.core[i]+=val;
         }
     }
/////
    if (TIME_CHECK) {
        end = omp_get_wtime();
        std::cout << "|\tstep 2 (fold): " << end - start << " s" << std::endl;
    }
    if (TIME_CHECK){
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    return tucker;
}

template <class datatype, size_t dimension>
int TuckerMethod<datatype, dimension>::get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype> & Bx, Matrix<datatype>& U, const size_t rank) {
    const int row_ = row;
    const int col_ = col;
    /* Input variables for svd() */
//    const size_t max_rowcol = std::max<const size_t>(row, col);
//    std::cout << "max_rowcol: " << max_rowcol <<std::endl;
    //char    whichE = 'L';           /* Input: Which singular values to calculate. ('L' - largest (algebraic) singular values, 'S' - smallest (algebraic) singular values) */
    //char    whichV = 'L';           /* Input: Which singular vectors to calculate. ('R' - right singular vectors, 'L' - left singular vectors */
    //MKL_INT pm[128];                /* Input: Array used to pass various parameters to Extended Eigensolver Extensions routines. */
    MKL_INT k;                      /* Output: Total number of sv calculated */
    /* Output: E - k entries of singular values, XL - k corresponding left-singular vectors, XR - k corresponding right-singular vectors */
    Matrix<datatype> M (row, row);
    Matrix<datatype> U_unsorted (row, row);
    //std::vector<double> E(rank);
    //std::vector<double> XL(rank*row);
    //std::vector<double> XR(rank*col);
    //double  E[rank], XL[rank*row], XR[rank*col]; /* Output: E - k entries of singular values, XL - k corresponding left-singular vectors, XR - k corresponding right-singular vectors */
    double  res[rank];                 /* Output: First k components contain the relative residual vector */

    /* Input variables for SGEMM */
    char        XGEMMC = 'T';  /* Character for GEMM routine, transposed case */
    char        XGEMMN = 'N';  /* Character for GEMM routine, non-transposed case */
    double      one = 1.0;     /* alpha parameter for GEMM */
    double      zero = 0.0;    /* beta  parameter for GEMM */
    /* Sparse BLAS IE variables */
    sparse_status_t status;
    sparse_matrix_t A = NULL; /* Handle containing sparse matrix in internal data structure */
    sparse_matrix_t B = NULL; /* Handle containing sparse matrix in internal data structure */
//    struct matrix_descr descr; /* Structure specifying sparse matrix properties */

    /* Local variables */
//    MKL_INT     i, j;
//    int info;
//    MKL_INT     compute_vectors = 1; /* Flag to compute singular vectors */
//    MKL_INT     tol = 5;             /* Tolerance */
    /* Create handle for matrix A stored in CSR format */
//    descr.type = SPARSE_MATRIX_TYPE_GENERAL;
//    status = mkl_sparse_d_create_csr ( &A, SPARSE_INDEX_BASE_ZERO, row_, col_, const_cast<int*>(Bp.data()), 
//                                                                               const_cast<int*>(Bp.data())+1, 
//                                                                               const_cast<int*>(Bj.data()), 
//                                                                               const_cast<double*>(Bx.data()) );
//
    // transpose                                                                                
    status = mkl_sparse_d_create_csc ( &A, SPARSE_INDEX_BASE_ZERO, col_, row_, const_cast<int*>(Bp.data()), 
                                                                               const_cast<int*>(Bp.data())+1, 
                                                                               const_cast<int*>(Bj.data()), 
                                                                               const_cast<double*>(Bx.data()) );
    
    assert(SPARSE_STATUS_SUCCESS==status);

    status = mkl_sparse_d_spmmd(SPARSE_OPERATION_TRANSPOSE,A,A,SPARSE_LAYOUT_ROW_MAJOR, M.pointer.data(),row_) ;
    assert(SPARSE_STATUS_SUCCESS==status);

    sym_eigen_solver(M, U_unsorted);

    // Sort U in desending order.
    for(size_t i = 0; i < row; ++i){
        for(size_t j = 0; j < rank; ++j){
            U(i,j) = U_unsorted(i,row-j-1);
        }
    }

//    /* Call mkl_sparse_ee_init to define default input values */
//    auto info0 = mkl_sparse_ee_init(pm);
//    pm[1] = tol;
//    pm[6] = compute_vectors;
//    pm[7] = 1; /* Use absolute stopping critertia. Iteration is stopped if norm(Ax-λx) < 10-pm[1] */
//
//    /* Call mkl_sparse_d_svd to obtain rank singular values and singular vectors */
//    info = mkl_sparse_d_svd(&whichE, &whichV, pm, A, descr, rank, &k, E.data(), XL.data(), XR.data(), res);
//    assert(info == SPARSE_STATUS_SUCCESS);
    auto info1 = mkl_sparse_destroy(A);
    auto info2 = mkl_sparse_destroy(B);
        

//
//
//    for (size_t i =0; i<k; i++){
//        std::cout << i <<"\t" <<E[k] <<"\t" << res[k] <<std::endl;     
//    }
//    std::cout << "get_left_singular_vector6"<<std::endl;
    return 0;
}

}
