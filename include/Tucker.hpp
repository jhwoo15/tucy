#pragma once
#include <iostream>
#include <cstring>
#include <array>
#include <vector>
#include <cmath>
#include "Matrix.hpp"
#include "Util.hpp"
#include "Tensor.hpp"
#include "DenseTensor.hpp"
#include "SparseTensor.hpp"
#include "Clock.hpp"
#include <cassert>
#include <omp.h>
#include "mkl_wrapper.hpp"

namespace TensorDecompose{
template <class datatype, size_t dimension>
class Tucker : public Tensor<datatype, dimension>{
    public:
        std::array<size_t, dimension> ranks;
        std::vector<datatype> core;
        std::array<Matrix<datatype>, dimension> Us;
        std::array<Matrix<datatype>, dimension> singular_vectors; // It contains all the singular vectors unlike Us.
        std::array<std::vector<double>, dimension> singular_values; // It contains all the singular values.
        std::array<size_t, dimension+1> shape_mult;
        std::array<size_t, dimension+1> ranks_mult;


        // Constructors
        Tucker(std::array<size_t, dimension> shape, std::array<size_t, dimension> ranks);


        void tucker_to_tensor(std::vector<datatype>& return_tensor);
        datatype& operator()(const std::array<size_t, dimension> index);
        void clear();
        //void decompose(const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<datatype>& val, bool calculate_core=false, bool pass_complete=false);
        //void decompose(const size_t* row, const size_t* col, const datatype* val,
        void decompose(const int* row, const int* col, const datatype* val,
                       const size_t row_size, const size_t col_size, const size_t val_size,
                       bool calculate_core=false, bool pass_complete=false);
        void hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core);
        int get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype> & Bx, Matrix<datatype>& U, std::vector<double>& singular_values, Matrix<datatype>& singular_vector, const size_t rank);
        void insert_value(std::array<size_t, dimension> index, datatype value){ std::cout << "Tucker::insert_value is not supported" <<std::endl; assert(false); };
        void diagonalize(std::vector<double>& eigen_values, size_t num_eig, bool use_fp32);
        std::pair<std::vector<double>, std::vector<datatype>> diagonalize(size_t num_eig);
        void diagonalize(std::vector<double>& eigen_values, std::vector<datatype>& eigen_vectors, size_t num_eig, bool use_fp32);
        std::vector<double> get_singular_values(const size_t dim) { return this->singular_values[dim]; };
        std::vector<datatype> get_singular_vector(const size_t dim) { return this->singular_vectors[dim].pointer; };
        std::vector<datatype> get_U(const size_t axis) { return this->Us[axis].pointer; };
        void add_to_core(const datatype* inp_vec);
        void set_Us(const datatype* U1, const datatype* U2, const datatype* U3,
                    const datatype* U4, const datatype* U5, const datatype* U6);
        void get_core(datatype* out);


        // Projector Methods //
        void to_projected_space(DenseTensor<datatype, dimension/2>& input_tensor, DenseTensor<datatype, dimension/2>& output_tensor);
        void to_projected_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor);
        void to_original_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor);
        void to_original_space(DenseTensor<datatype, dimension/2>& input_tensor, DenseTensor<datatype, dimension/2>& output_tensor);
        void to_original_space(DenseTensor<datatype, dimension/2>& input_tensor, SparseTensor<datatype, dimension/2>& output_tensor);
        void project_kinetic_matrix(Matrix<datatype>& T_xx, Matrix<datatype>& T_yy, Matrix<datatype>& T_zz);
        void project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, std::vector<datatype> t_xx, std::vector<datatype> t_yy, std::vector<datatype> t_zz);
        void project_hamiltonian(SparseTensor<datatype, dimension>& tensor);
        void project_local_potential(double* local_potential, size_t project_axis_level);
        void new_project_local_potential(double* local_potential, size_t project_axis_level);
        void project_KB_projector(std::vector<size_t> D_shape, std::vector<datatype> D_val, std::vector<size_t> KB_shape,
                                  std::vector<size_t> KB_row, std::vector<size_t> KB_col, std::vector<datatype> KB_val);
};


template <class datatype, size_t dimension>
Tucker<datatype, dimension>::Tucker(std::array<size_t, dimension> shape, std::array<size_t, dimension> ranks){
    this->shape = shape;
    this->ranks = ranks;
    // Ranks should be less than or equal to shape of tensor.
    for (size_t dim = 0; dim < dimension; dim++){
        assert(ranks[dim] <= shape[dim]);
    }
    cumprod<dimension>(this->shape, this->shape_mult);
    cumprod<dimension>(this->ranks, this->ranks_mult);
    assert(this->shape_mult[dimension]!=0);
    assert(this->ranks_mult[dimension]!=0);
    this->core.resize(this->ranks_mult[dimension], 0);
    for (size_t dim = 0; dim < dimension; dim++){
        this->Us[dim] = Matrix<datatype>(shape[dim], ranks[dim]);
        this->singular_vectors[dim] = Matrix<datatype>(shape[dim], shape[dim]);
        this->singular_values[dim].resize(shape[dim]);
    }
}


//template <class datatype, size_t dimension>
//void Tucker<datatype, dimension>::decompose(const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<datatype>& val, bool calculate_core, bool pass_complete){
//    /* Do HOSVD -> make Us and core tensor.
//     * Input)
//     * row  : row indices of COO matrix
//     * col  : col indices of COO matrix
//     * val  : values of COO matrix correspoding to row and col indices.
//     */
//    Clock::instance().start("Tucker<datatype, dimension>::decompose");
//    std::cout <<"Tucker<datatype,dimension>::decompose" <<std::endl;
//    auto start_time = omp_get_wtime();
//    SparseTensor<datatype, dimension> sparse_tensor(this->shape, row, col, val, "C", pass_complete);
//    auto end_time = omp_get_wtime();
//    std::cout << "* create sparse tensor in C++ : " << end_time-start_time <<std::endl;
//    start_time = omp_get_wtime();
//    hosvd(sparse_tensor, this->ranks, calculate_core);
//    end_time = omp_get_wtime();
//    Clock::instance().end("Tucker<datatype, dimension>::decompose");
//    std::cout << "* calcualte hosvd in C++ " << end_time-start_time <<std::endl;
//    //auto wall_time = Clock::instance().get_wall_time("Tucker<datatype, dimension>::decompose");
//    //std::cout << "* calcualte hosvd in C++ " << wall_time.first << "\t(" << wall_time.second <<")" <<std::endl;
//}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::add_to_core(const datatype* inp_vec){
    //assert(inp_vec.size() == this->core.size());
    axpy<datatype>(this->core.size(), 1., inp_vec, 1, this->core.data(), 1);
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::set_Us(const datatype* U1, const datatype* U2, const datatype* U3,
                                         const datatype* U4, const datatype* U5, const datatype* U6){
    memcpy(this->Us[0].pointer.data(), U1, Us[0].pointer.size() * sizeof(datatype));
    memcpy(this->Us[1].pointer.data(), U2, Us[1].pointer.size() * sizeof(datatype));
    memcpy(this->Us[2].pointer.data(), U3, Us[2].pointer.size() * sizeof(datatype));
    memcpy(this->Us[3].pointer.data(), U4, Us[3].pointer.size() * sizeof(datatype));
    memcpy(this->Us[4].pointer.data(), U5, Us[4].pointer.size() * sizeof(datatype));
    memcpy(this->Us[5].pointer.data(), U6, Us[5].pointer.size() * sizeof(datatype));
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::get_core(datatype* out){
    //axpy<datatype>(this->core.size(), 1., this->core.data(), 1, out, 1);
    #pragma omp parallel for
    for (size_t i = 0; i < core.size(); i++){
        out[i] = this->core[i];
    }
    return;
}


template <class datatype, size_t dimension>
//void Tucker<datatype, dimension>::decompose(const std::vector<size_t>& row_ptr, const std::vector<size_t>& col_ind, const std::vector<datatype>& val, bool calculate_core, bool pass_complete){
void Tucker<datatype, dimension>::decompose(const int* row_ptr, const int* col_ind, const datatype* val, const size_t row_size, const size_t col_size, const size_t val_size, bool calculate_core, bool pass_complete){
    /* Do HOSVD -> make Us and core tensor.
     * Input)
     * row  : row_ptr of CSR matrix
     * col  : col_ind of CSR matrix
     * val  : nonzero values of CSR matrix 
     */
    Clock::instance().start("Tucker<datatype, dimension>::decompose");
    std::cout <<"Tucker<datatype,dimension>::decompose" <<std::endl;
    auto start_time = omp_get_wtime();
    SparseTensor<datatype, dimension> sparse_tensor(this->shape);
    //sparse_tensor.from_csr(row_ptr, col_ind, val, "C", pass_complete);
    sparse_tensor.from_csr(row_ptr, col_ind, val, row_size, col_size, val_size, "C", pass_complete);
    auto end_time = omp_get_wtime();
    std::cout << "* create sparse tensor in C++ : " << end_time-start_time <<std::endl;
    start_time = omp_get_wtime();
    hosvd(sparse_tensor, this->ranks, calculate_core);
    end_time = omp_get_wtime();
    Clock::instance().end("Tucker<datatype, dimension>::decompose");
    std::cout << "* calculate hosvd in C++ " << end_time-start_time <<std::endl;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::tucker_to_tensor(std::vector<datatype>& return_tensor){
    Clock::instance().start("Tucker<datatype, dimension>::tucker_to_tensor(std::vector<datatype>& return_tensor)");
    return_tensor.resize(shape_mult[dimension]);
    Matrix<datatype> G = Map(core, ranks[0], ranks_mult[dimension]/ranks[0]);
    Matrix<datatype> G_proj;

    G_proj = multiply(Us[0], G, "NoT", "NoT");

    for (size_t dim = 1; dim < dimension; ++dim) {
        to_next_leading_axis(G, G_proj, this->ranks, this->shape, dim);
        G_proj = multiply(Us[dim], G, "NoT", "NoT");
    }

    for (size_t i = 0; i < this->shape[dimension-1]; i++) {
        for (size_t j = 0; j < shape_mult[dimension-1]; j++) {
            return_tensor[i*shape_mult[dimension-1] + j] = G_proj(i , j);
        }
    }
    Clock::instance().end("Tucker<datatype, dimension>::tucker_to_tensor(std::vector<datatype>& return_tensor)");
}


template <class datatype, size_t dimension>
datatype& Tucker<datatype, dimension>::operator()(const std::array<size_t, dimension> index){
    //return index[0] + index[1] * dims[0] + index[2] * dims[0] * dims[1];
    size_t combined_index = 0;
    #pragma unroll
    for (size_t i = 0; i < dimension; i++) {
        combined_index += index[i] * this->shape_mult[i];
    }
    return this->tensor[combined_index];
}


template <class datatype, size_t dimension>
std::pair<std::vector<double>, std::vector<datatype>> Tucker<datatype, dimension>::diagonalize(const size_t num_eig){
    Clock::instance().start("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, const size_t num_eig = 0, const bool use_fp32=false)");
    static_assert(dimension==6, "dimension for Tucker::diagonalize should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    assert(this->ranks[0] == this->ranks[3]);
    assert(this->ranks[1] == this->ranks[4]);
    assert(this->ranks[2] == this->ranks[5]);
    const size_t num_of_grid = ranks[3] * ranks[4] * ranks[5];
    if (num_eig == 0){
        num_eig = num_of_grid;  // default value of num_eig
    }
    std::vector<double> eigen_values(num_eig);
    std::vector<datatype> eigen_vectors;

    assert(this->core.size() == num_of_grid * num_of_grid && "maybe core tensor is not generated.");

    Matrix<datatype>    projected_eigen_vectors(num_of_grid, num_of_grid, this->core);
    std::vector<double> projected_eigen_values(core.size());
    const size_t row_size = this->ranks[0] * this->ranks[1] * this->ranks[2];

    heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', row_size, projected_eigen_vectors.pointer.data(), row_size, projected_eigen_values.data());

    #pragma omp parallel for 
    for (size_t i = 0; i < num_eig; i++){
        eigen_values[i] = projected_eigen_values[i];
    }

    double start, end;
    bool TIME_CHECK = true;
    if (TIME_CHECK){ start = omp_get_wtime(); }
    // eigen_vectors(projection space) -> eigen_vectors(original space) //
    const size_t num_of_original_grid = this->shape[3] * this->shape[4] * this->shape[5];
    eigen_vectors.resize(0);
    for (size_t i = 0; i < num_eig; ++i){
        auto i_th_vector = projected_eigen_vectors.get_column_vector(i);
        std::vector<datatype> original_i_th_vector;
        to_original_space(i_th_vector, original_i_th_vector);
        eigen_vectors.insert(eigen_vectors.end(), original_i_th_vector.begin(), original_i_th_vector.end());
    }
    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "to_original_space time (diagonalize)" << end - start << " s" << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t num_eig = 0, const bool use_fp32=false)");

    return std::pair<std::vector<double>, std::vector<datatype>>(eigen_values, eigen_vectors);
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::diagonalize(std::vector<double>& eigen_values, const size_t num_eig = 0, const bool use_fp32=false){
    Clock::instance().start("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, const size_t num_eig = 0, const bool use_fp32=false)");
    static_assert(dimension==6, "dimension for Tucker::diagonalize should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    assert(this->ranks[0] == this->ranks[3]);
    assert(this->ranks[1] == this->ranks[4]);
    assert(this->ranks[2] == this->ranks[5]);
    const size_t num_of_grid = ranks[3] * ranks[4] * ranks[5];
    if (num_eig == 0){
        num_eig = num_of_grid;  // default value of num_eig
    }
    //const size_t num_eig  = eigen_values.size();
    eigen_values.resize(num_eig);
    assert(num_eig <= num_of_grid && "num_eig is over maximum value");
    assert(this->core.size() == num_of_grid * num_of_grid && "maybe core tensor is not generated.");

    std::vector<datatype> projected_eigen_vectors = this->core;
    std::vector<datatype> projected_eigen_values(core.size());
    const size_t row_size = this->ranks[0] * this->ranks[1] * this->ranks[2];
    // diagonalization
    // diagonalization
    if(use_fp32){
        const size_t num_elements_values =projected_eigen_values.size();
        const size_t num_elements_vectors=projected_eigen_vectors.get_row()*projected_eigen_vectors.get_col();
        float* float_eigen_values   = new float[num_elements_values];
        float* float_eigen_vectors  = new float[num_elements_vectors];

        #pragma omp parallel for
        for (size_t i=0; i< num_elements_vectors; i++){
            float_eigen_vectors[i] = (float) projected_eigen_vectors[i];
        }

        heevd<float>(LAPACK_COL_MAJOR, 'V', 'L', row_size, float_eigen_vectors, row_size, float_eigen_values);
        
        #pragma omp parallel for
        for (size_t i=0; i< num_elements_vectors; i++){
            projected_eigen_vectors[i] = (double) float_eigen_vectors[i];
        }
        #pragma omp parallel for
        for (size_t i=0; i< num_elements_values; i++){
            projected_eigen_values[i] = (double) float_eigen_values[i];
        }
        delete [] float_eigen_values;
        delete [] float_eigen_vectors;
    }
    else{
        heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', row_size, projected_eigen_vectors.data(), row_size, projected_eigen_values.data());  // Eigen values in ascending order.
    }

    // select largest eigenvalues
    #pragma omp parallel for
    for (size_t i = 0; i < num_eig; i++){
        eigen_values[i] = projected_eigen_values[i];
    }

    Clock::instance().end("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, const size_t num_eig = 0, const bool use_fp32=false)");
    return;
}

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::diagonalize(std::vector<double>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t num_eig = 0, const bool use_fp32=false){
    Clock::instance().start("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t num_eig = 0, const bool use_fp32=false)");
    static_assert(dimension==6, "dimension for Tucker::diagonalize should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");

    assert(this->ranks[0] == this->ranks[3]);
    assert(this->ranks[1] == this->ranks[4]);
    assert(this->ranks[2] == this->ranks[5]);
    const size_t num_of_grid = ranks[3] * ranks[4] * ranks[5];
    if (num_eig == 0){
        num_eig = num_of_grid;  // default value of num_eig
    }
    eigen_values.resize(num_eig);
    //eigen_vectors.resize(num_of_grid * num_eig);    // (num_of_grid * num_of_grid)
    assert(this->core.size() == num_of_grid * num_of_grid && "maybe core tensor is not generated.");

    Matrix<datatype>      projected_eigen_vectors(num_of_grid, num_of_grid, this->core);
    //std::vector<datatype> projected_eigen_values(core.size());
    std::vector<double> projected_eigen_values(core.size());
    const size_t row_size = this->ranks[0] * this->ranks[1] * this->ranks[2];
    // diagonalization
///    if(use_fp32){
///        const size_t num_elements_values =projected_eigen_values.size();
///        const size_t num_elements_vectors=projected_eigen_vectors.get_row()*projected_eigen_vectors.get_col();
///        float* float_eigen_values   = new float[num_elements_values];
///        float* float_eigen_vectors  = new float[num_elements_vectors];
///
///        #pragma omp parallel for
///        for (size_t i=0; i< num_elements_vectors; i++){
///            float_eigen_vectors[i] = (float) projected_eigen_vectors[i];
///        }
///
///        heevd<float>(LAPACK_COL_MAJOR, 'V', 'L', row_size, float_eigen_vectors, row_size, float_eigen_values);
///        
///        #pragma omp parallel for
///        for (size_t i=0; i< num_elements_vectors; i++){
///            projected_eigen_vectors[i] = (double) float_eigen_vectors[i];
///        }
///        #pragma omp parallel for
///        for (size_t i=0; i< num_elements_values; i++){
///            projected_eigen_values[i] = (double) float_eigen_values[i];
///        }
///        delete [] float_eigen_values;
///        delete [] float_eigen_vectors;
///    }
///    else{
///        heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', row_size, projected_eigen_vectors.pointer.data(), row_size, projected_eigen_values.data());
///    }
    heevd<datatype>(LAPACK_COL_MAJOR, 'V', 'L', row_size, projected_eigen_vectors.pointer.data(), row_size, projected_eigen_values.data());
//    LAPACKE_dsyevd(LAPACK_COL_MAJOR, 'V', 'L', row_size, projected_eigen_vectors.pointer.data(), row_size, projected_eigen_values.data());  // Eigen values in ascending order.

    // select largest eigenvalues
    #pragma omp parallel for 
    for (size_t i = 0; i < num_eig; i++){
        eigen_values[i] = projected_eigen_values[i];
        //eigen_values[i] = projected_eigen_values[num_eig - i -1];
    }

/*    
    // get eigen_vectors from projected_eigen_vectors
    for (size_t i = 0; i < num_of_grid; ++i){
        for (size_t j = 0; j < num_eig; ++j){
            const size_t combined_index = i +  num_of_grid * j;
            //eigen_vectors[combined_index] = projected_eigen_vectors(i, num_of_grid-j-1);
            eigen_vectors[combined_index] = projected_eigen_vectors(i, j);
        }
    }
*/    

    double start, end;
    bool TIME_CHECK = true;
    if (TIME_CHECK){ start = omp_get_wtime(); }
    // eigen_vectors(projection space) -> eigen_vectors(original space) //
    const size_t num_of_original_grid = this->shape[3] * this->shape[4] * this->shape[5];
    //eigen_vectors.reserve(num_of_original_grid * num_eig);
    eigen_vectors.resize(0);
    for (size_t i = 0; i < num_eig; ++i){
        auto i_th_vector = projected_eigen_vectors.get_column_vector(i);
        std::vector<datatype> original_i_th_vector;
        to_original_space(i_th_vector, original_i_th_vector);
        eigen_vectors.insert(eigen_vectors.end(), original_i_th_vector.begin(), original_i_th_vector.end());
    }
    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "to_original_space time (diagonalize)" << end - start << " s" << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::diagonalize(std::vector<datatype>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t num_eig = 0, const bool use_fp32=false)");
    return;
}

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::to_projected_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor){
    Clock::instance().start("Tucker<datatype, dimension>::to_projected_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor)");
    static_assert(dimension==6, "dimension for Tucker::to_projected_space should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    const size_t tensor_dimension = 3;

    output_tensor.resize(this->ranks[tensor_dimension] * this->ranks[tensor_dimension+1] * this->ranks[tensor_dimension+1]);

    Tucker<datatype, tensor_dimension> tucker({this->shape[3], this->shape[4], this->shape[5]} , {this->ranks[3], this->ranks[4], this->ranks[5]});  // Some problems in Constructor. (uselessly allocate Us)

    Matrix<datatype> G = Map(input_tensor, tucker.shape[0], tucker.shape_mult[tensor_dimension]/tucker.shape[0]);
    Matrix<datatype> G_proj;

    G_proj = multiply(Us[tensor_dimension], G, "T", "NoT");

//    for (size_t dim = tensor_dimension+1; dim < dimension; ++dim) {
    for (size_t dim = 1; dim < tensor_dimension; ++dim) {
        to_next_leading_axis(G, G_proj, tucker.shape, tucker.ranks, dim);
        G_proj = multiply(Us[dim], G, "T", "NoT");
    }

    #pragma omp parallel for collapse(2)
    for (size_t i = 0; i < tucker.ranks[tensor_dimension-1]; i++) {
        for (size_t j = 0; j < tucker.ranks_mult[tensor_dimension-1]; j++) {
            output_tensor[i*tucker.ranks_mult[tensor_dimension-1] + j] = G_proj(i , j);
        }
    }
    Clock::instance().end("Tucker<datatype, dimension>::to_projected_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor)");
}

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::to_projected_space(DenseTensor<datatype, dimension/2>& input_tensor, DenseTensor<datatype, dimension/2>& output_tensor){
    Clock::instance().start("Tucker<datatype, dimension>::to_projected_space(DenseTensor<datatype, dimension/2>& input_tensor, DenseTensor<datatype, dimension/2>& output_tensor)");
    static_assert(dimension==6, "dimension for Tucker::to_projected_space should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    const size_t tensor_dimension = dimension/2;

    assert(output_tensor.shape[0] == this->ranks[tensor_dimension]);
    assert(output_tensor.shape[1] == this->ranks[tensor_dimension+1]);
    assert(output_tensor.shape[2] == this->ranks[tensor_dimension+2]);
    assert(input_tensor.shape[0] == this->shape[tensor_dimension]);
    assert(input_tensor.shape[1] == this->shape[tensor_dimension+1]);
    assert(input_tensor.shape[2] == this->shape[tensor_dimension+2]);
    //assert(input_tensor.shape == std::array<datatype, tensor_dimension>({ranks[3], ranks[4], ranks[5]})); // array->vector로 바꿨을때.

    Tucker<double, tensor_dimension> tucker({this->shape[3], this->shape[4], this->shape[5]} , {this->ranks[3], this->ranks[4], this->ranks[5]});  // Some problems in Constructor. (uselessly allocate Us)

    Matrix<double> G = Map(input_tensor.data, tucker.shape[0], tucker.shape_mult[tensor_dimension]/tucker.shape[0]);
    Matrix<double> G_proj;

    G_proj = multiply(Us[tensor_dimension], G, "T", "NoT");

//    for (size_t dim = tensor_dimension+1; dim < dimension; ++dim) {
    for (size_t dim = 1; dim < tensor_dimension; ++dim) {
        to_next_leading_axis(G, G_proj, tucker.shape, tucker.ranks, dim);
        G_proj = multiply(Us[dim], G, "T", "NoT");
    }

    #pragma omp parallel for collapse(2)
    for (size_t i = 0; i < tucker.ranks[tensor_dimension-1]; i++) {
        for (size_t j = 0; j < tucker.ranks_mult[tensor_dimension-1]; j++) {
            output_tensor[i*tucker.ranks_mult[tensor_dimension-1] + j] = G_proj(i , j);
        }
    }
    Clock::instance().end("Tucker<datatype, dimension>::to_projected_space(DenseTensor<datatype, dimension/2>& input_tensor, DenseTensor<datatype, dimension/2>& output_tensor)");
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::to_original_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor){
    Clock::instance().start("Tucker<datatype, dimension>::to_original_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor)");
    static_assert(dimension==6, "dimension for Tucker::to_original_space should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    const size_t tensor_dimension = dimension/2;
    output_tensor.resize(this->shape[tensor_dimension] * this->shape[tensor_dimension+1] * this->shape[tensor_dimension+2]);

    Tucker<datatype, tensor_dimension> tucker({this->shape[3], this->shape[4], this->shape[5]} , {this->ranks[3], this->ranks[4], this->ranks[5]});  // Some problems in Constructor. (uselessly allocate Us)

    Matrix<datatype> G = Map(input_tensor, tucker.ranks[0], tucker.ranks_mult[tensor_dimension]/tucker.ranks[0]);
    Matrix<datatype> G_proj;

    //G_proj = multiply(Us[tensor_dimension], G, "NoT", "NoT");
    G_proj = multiply(Us[3], G, "NoT", "NoT");

    for (size_t dim = 1; dim < tensor_dimension; ++dim) {
        to_next_leading_axis(G, G_proj, tucker.ranks, tucker.shape, dim);
        //G_proj = multiply(Us[dim], G, "NoT", "NoT");
        G_proj = multiply(Us[dim+3], G, "NoT", "NoT");
    }
    const size_t loop_size1 = tucker.shape[tensor_dimension-1];
    const size_t loop_size2 = tucker.shape_mult[tensor_dimension-1];
    #pragma omp parallel for collapse(2)
    for (size_t i = 0; i < loop_size1; i++) {
        for (size_t j = 0; j < loop_size2; j++) {
            output_tensor[i * tucker.shape_mult[tensor_dimension-1] + j] = G_proj(i, j);
        }
    }
    Clock::instance().end("Tucker<datatype, dimension>::to_original_space(std::vector<datatype>& input_tensor, std::vector<datatype>& output_tensor)");
}

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::to_original_space(DenseTensor<datatype, dimension/2>& input_tensor, SparseTensor<datatype, dimension/2>& output_tensor){
    Clock::instance().start("Tucker<datatype, dimension>::to_original_space(DenseTensor<datatype, dimension/2>& input_tensor, SparseTensor<datatype, dimension/2>& output_tensor)");
    static_assert(dimension==6, "dimension for Tucker::to_original_space should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    const size_t tensor_dimension = dimension / 2;

    assert(output_tensor.shape[0] == this->shape[tensor_dimension]);
    assert(output_tensor.shape[1] == this->shape[tensor_dimension+1]);
    assert(output_tensor.shape[2] == this->shape[tensor_dimension+2]);
    assert(input_tensor.shape[0] == this->ranks[tensor_dimension]);
    assert(input_tensor.shape[1] == this->ranks[tensor_dimension+1]);
    assert(input_tensor.shape[2] == this->ranks[tensor_dimension+2]);

    Tucker<datatype, tensor_dimension> tucker({this->shape[3], this->shape[4], this->shape[5]} , {this->ranks[3], this->ranks[4], this->ranks[5]});  // Some problems in Constructor. (uselessly allocate Us)

    Matrix<datatype> G = Map(input_tensor.data, tucker.ranks[0], tucker.ranks_mult[tensor_dimension]/tucker.ranks[0]);
    Matrix<datatype> G_proj;

    //G_proj = multiply(Us[tensor_dimension], G, "NoT", "NoT");
    G_proj = multiply(Us[3], G, "NoT", "NoT");

//    for (size_t dim = tensor_dimension+1; dim < dimension; ++dim) {
    for (size_t dim = 1; dim < tensor_dimension; ++dim) {
        to_next_leading_axis(G, G_proj, tucker.ranks, tucker.shape, dim);
        //G_proj = multiply(Us[dim], G, "NoT", "NoT");
        G_proj = multiply(Us[dim+3], G, "NoT", "NoT");
    }

    const double sparse_criteria = 1E-6;
    const size_t loop_size = tucker.shape[tensor_dimension-1];
    for (size_t i = 0; i < loop_size; i++) {
        for (size_t j = 0; j < loop_size; j++) {
            //output_tensor[i*tucker.shape_mult[tensor_dimension-1] + j] = G_proj(i , j);
            if (std::abs( G_proj(i, j) ) > sparse_criteria){
                size_t input_index = i*tucker.shape_mult[tensor_dimension-1] + j;
                std::array<size_t, 3> index = get_tensor_index(input_index, output_tensor.shape);
                output_tensor.insert_value(index, G_proj(i, j));
            }
        }
    }
    output_tensor.complete();
    Clock::instance().end("Tucker<datatype, dimension>::to_original_space(DenseTensor<datatype, dimension/2>& input_tensor, SparseTensor<datatype, dimension/2>& output_tensor)");
}


////template <class datatype, size_t dimension>
//void Tucker<double, 6>::project_local_potential(SparseTensor<double, 6>& local_potential){
//    // Input : 'local_potential' is the difference between the local potential of the recent step's and the previous step's.
//    const size_t dimension = 6;
//    const size_t local_potential_length = local_potential.data.size();
//    const size_t core_tensor_length = this->ranks_mult[3];
//    //double val ;
//    //#pragma omp parallel for collapse(2) // private(val)
//    //
//
//    #pragma omp parallel for
//    for (size_t i = 0; i < core_tensor_length; i++){
//        for (size_t j = i; j < core_tensor_length; j++){
//
//            const size_t core_index = i + core_tensor_length * j;
//            const auto index = get_tensor_index(core_index, this->ranks);
//            for (size_t k = 0; k < local_potential_length; k++){
//                double val = local_potential.data[k].second;
//                for (size_t dim = 0; dim < 3; dim++){
//                // Using the property that Us[0] = Us[3] & Us[1] = Us[4] & Us[2] = Us[5] //
//                    val *= this->Us[dim](local_potential.data[k].first[dim], index[dim]) * this->Us[dim](local_potential.data[k].first[dim], index[dim+3]);
//                }
//                this->core[core_index] += val;
//                if (i != j){
//                    const size_t core_index_transpose = core_tensor_length * i + j;
//                    this->core[core_index_transpose] += val ;
//                }
//            }
////            if(i!=j){
////                const size_t core_index_transpose = core_tensor_length * i + j;
////                this->core[core_index_transpose] += this->core[core_index];   // Not only local potential term in core.
////            }
//        }
//    }
//}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::project_kinetic_matrix(Matrix<datatype>& T_xx, Matrix<datatype>& T_yy, Matrix<datatype>& T_zz){
    Clock::instance().start("Tucker<datatype, dimension>::project_kinetic_matrix(Matrix<datatype>& T_xx, Matrix<datatype>& T_yy, Matrix<datatype>& T_zz)");
    static_assert(dimension==6, "dimension for Tucker::project_kinetic_matrix should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    /**********************************************************************************************************************************
    "projected kinetic matrix" = U^T T_xxyyzz U
                               = ( U_z (x) U_y (x) U_x )^T ( T_xx (+) T_yy (+) T_zz ) ( U_z (x) U_y (x) U_x )
                               = U^T ( I_zz (x) I_yy (x) T_xx + I_zz (x) T_yy (x) I_xx + T_zz (x) I_yy (x) I_xx ) U
                               = I_rz,rz (x) I_ry,ry (x) T'_rx,rx + I_rz,rz (x) T'_ry,ry (x) I_rx,rx + T'_rz,rz (x) I_ry,ry (x) I_rx,rx

    "original kinetic matrix" (N_x, N_y, N_z, N_x, N_y, N_z) -> "projected kinetic matrix" (r_x, r_y, r_z, r_x, r_y, r_z)

    Complexity of Matrix multiplication of each axis = O(r_x*N_x^2 + r_x^2*N_x + r_y*N_y^2 + r_y^2*N_y + r_z*N_z^2 + r_z^2*N_z)
                                                     ~ O(r*N^{2/3})

    Complexity of Kronecker product = O( 3 r_x^2*r_y^2*r_z^2 ) ~ O(3*r^6)
    **********************************************************************************************************************************/
    bool TIME_CHECK = true;
    //double start, end, start_time, end_time;
    double start_time, end_time;
    if(TIME_CHECK){
        start_time = omp_get_wtime();
        std::cout << "====== project_kinetic_matrix ======" << std::endl;
    }

    // 1. Matrix multiplication //
    // 1-1. Calculate (U_x)^T T_xx U_x //
    Matrix<datatype> T_xx_projected;
    T_xx_projected = multiply(Us[0], T_xx, "T", "NoT");
    T_xx_projected = multiply(T_xx_projected, Us[3], "NoT", "NoT");
    // 1-2. Calculate (U_y)^T T_yy U_y //
    Matrix<datatype> T_yy_projected;
    T_yy_projected = multiply(Us[1], T_yy, "T", "NoT");
    T_yy_projected = multiply(T_yy_projected, Us[4], "NoT", "NoT");
    // 1-3. Calculate (U_z)^T T_zz U_z //
    Matrix<datatype> T_zz_projected;
    T_zz_projected = multiply(Us[2], T_zz, "T", "NoT");
    T_zz_projected = multiply(T_zz_projected, Us[5], "NoT", "NoT");

    size_t rx = this->ranks[0];
    size_t ry = this->ranks[1];
    size_t rz = this->ranks[2];

    //Matrix<double> T_xyzxyz_projected(rx*ry*rz, rx*ry*rz);
    /********************************************************************
     Make T_xyzxyz_projected. (No kronecker product. Just reordering)
     Complexity : O( rx*ry*rx*(rx + ry + rz) ) ~ O(r^4)
     *******************************************************************/
    // Complexity
    // T_xx_projected -> T_xyzxyz_projected //
    size_t arg1, arg2, arg3, arg4;
    arg1 = rx * ry * rz;
    arg2 = rx * rx * ry * rz + rx;
    //#pragma omp parallel for num_threads(3) collapse(3)
    for (size_t k = 0; k < ry * rz; k++){
        for (size_t i = 0; i < rx; i++){
            for (size_t j = 0; j < rx; j++){
                //T_xyzxyz_projected[ i + arg1*j + arg2*k] += T_xx_projected(i, j);
                this->core[ i + arg1*j + arg2*k] += T_xx_projected(i, j);
            }
        }
    }
    // T_yy_projected -> T_xyzxyz_projected //
    arg1 = rx;
    arg2 = rx * rx * ry * rz;
    arg3 = rx * ry * rz + 1;
    arg4 = rx * rx * ry * ry * rz  + ry * rx;
    //#pragma omp parallel for num_threads(4) collapse(4)
    for (size_t k = 0; k < rx; k++){
        for (size_t l = 0; l < rz; l++){
            for (size_t i = 0; i < ry; i++){
                for (size_t j = 0; j < ry; j++){
                    //T_xyzxyz_projected[ arg1*i + arg2*j + arg3*k + arg4*l ] += T_yy_projected(i, j);
                    this->core[ arg1*i + arg2*j + arg3*k + arg4*l ] += T_yy_projected(i, j);
                }
            }
        }
    }
    // T_zz_projected -> T_xyzxyz_projected //
    arg1 = rx * ry * rz + 1;
    arg2 = ry * rx;
    arg3 = rx * rx * ry * ry * rz;
    //#pragma omp parallel for num_threads(3) collapse(3)
    for (size_t k = 0; k < ry * rx; k++){
        for (size_t i = 0; i < rz; i++){
            for (size_t j = 0; j < rz; j++){
                //T_xyzxyz_projected[ arg1*k + arg2*i + arg3*j ] += T_zz_projected(i, j);
                this->core[ arg1*k + arg2*i + arg3*j ] += T_zz_projected(i, j);
            }
        }
    }

    // Choose Ordering: (T_xx (x) T_yy) (x) T_zz OR T_xx (x) (T_yy (x) T_zz)
    // 2-1. Kronecker product : T_xx_projected (x) T_yy_projected //
    // ?ger or ?geru or ?gerc
/*
    Matrix<double> T_xyxy_projected(T_xx_projected.row * T_yy_projected.row, T_xx_projected.column * T_yy_projected.column);
    for (size_t i = 0; i < T_xx_projected.column; i++){
        for (size_t j = 0; j < T_yy_projected.column; j++){
            MKL_INT row = T_yy_projected.row;
            MKL_INT col = T_xx_projected.row;
            MKL_INT lda = T_xx_projected.row * T_yy_projected.row;
            cblas_dger(CblasColMajor, row, col, 1, T_yy_projected.pointer.data() + j*T_yy_projected.row,
                        1, T_xx_projected.pointer.data() + i*T_xx_projected.row, 
                        1, T_xyxy_projected.pointer.data() + i*lda*T_yy_projected.column + j*lda, row);
        }
    }
*/
    if (TIME_CHECK){
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::project_kinetic_matrix(Matrix<datatype>& T_xx, Matrix<datatype>& T_yy, Matrix<datatype>& T_zz)");
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, std::vector<datatype> t_xx, std::vector<datatype> t_yy, std::vector<datatype> t_zz){
    Clock::instance().start("Tucker<datatype, dimension>::project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, std::vector<datatype> t_xx, std::vector<datatype> t_yy, std::vector<datatype> t_zz)");
    static_assert(dimension==6, "dimension for Tucker::project_kinetic_matrix should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    /**********************************************************************************************************************************
    "projected kinetic matrix" = U^T T_xxyyzz U
                               = ( U_z (x) U_y (x) U_x )^T ( T_xx (+) T_yy (+) T_zz ) ( U_z (x) U_y (x) U_x )
                               = U^T ( I_zz (x) I_yy (x) T_xx + I_zz (x) T_yy (x) I_xx + T_zz (x) I_yy (x) I_xx ) U
                               = I_rz,rz (x) I_ry,ry (x) T'_rx,rx + I_rz,rz (x) T'_ry,ry (x) I_rx,rx + T'_rz,rz (x) I_ry,ry (x) I_rx,rx

    "original kinetic matrix" (N_x, N_y, N_z, N_x, N_y, N_z) -> "projected kinetic matrix" (r_x, r_y, r_z, r_x, r_y, r_z)

    Complexity of Matrix multiplication of each axis = O(r_x*N_x^2 + r_x^2*N_x + r_y*N_y^2 + r_y^2*N_y + r_z*N_z^2 + r_z^2*N_z)
                                                     ~ O(r*N^{2/3})

    Complexity of Kronecker product = O( 3 r_x^2*r_y^2*r_z^2 ) ~ O(3*r^6)
    **********************************************************************************************************************************/
    bool TIME_CHECK = true;
    //double start, end, start_time, end_time;
    double start_time, end_time;
    if(TIME_CHECK){
        start_time = omp_get_wtime();
        std::cout << "====== project_kinetic_matrix ======" << std::endl;
    }
    // std::vector -> Matrix
    Matrix<datatype> T_xx(dim_x, dim_x, t_xx);
    Matrix<datatype> T_yy(dim_y, dim_y, t_yy);
    Matrix<datatype> T_zz(dim_z, dim_z, t_zz);

    // 1. Matrix multiplication //
    // 1-1. Calculate (U_x)^T T_xx U_x //
    Matrix<datatype> T_xx_projected;
    T_xx_projected = multiply(Us[0], T_xx, "T", "NoT");
    ///T_xx_projected = multiply(T_xx_projected, Us[0], "NoT", "NoT");
    T_xx_projected = multiply(T_xx_projected, Us[3], "NoT", "NoT");
    // 1-2. Calculate (U_y)^T T_yy U_y //
    Matrix<datatype> T_yy_projected;
    T_yy_projected = multiply(Us[1], T_yy, "T", "NoT");
    ///T_yy_projected = multiply(T_yy_projected, Us[1], "NoT", "NoT");
    T_yy_projected = multiply(T_yy_projected, Us[4], "NoT", "NoT");
    // 1-3. Calculate (U_z)^T T_zz U_z //
    Matrix<datatype> T_zz_projected;
    T_zz_projected = multiply(Us[2], T_zz, "T", "NoT");
    ///T_zz_projected = multiply(T_zz_projected, Us[2], "NoT", "NoT");
    T_zz_projected = multiply(T_zz_projected, Us[5], "NoT", "NoT");

    size_t rx = this->ranks[0];
    size_t ry = this->ranks[1];
    size_t rz = this->ranks[2];

    //Matrix<double> T_xyzxyz_projected(rx*ry*rz, rx*ry*rz);
    /********************************************************************
     Make T_xyzxyz_projected. (No kronecker product. Just reordering)
     Complexity : O( rx*ry*rx*(rx + ry + rz) ) ~ O(r^4)
     *******************************************************************/
    // Complexity
    // T_xx_projected -> T_xyzxyz_projected //
    size_t arg1, arg2, arg3, arg4;
    arg1 = rx * ry * rz;
    arg2 = rx * rx * ry * rz + rx;
    //#pragma omp parallel for num_threads(3) collapse(3)
    for (size_t k = 0; k < ry * rz; k++){
        for (size_t i = 0; i < rx; i++){
            for (size_t j = 0; j < rx; j++){
                //T_xyzxyz_projected[ i + arg1*j + arg2*k] += T_xx_projected(i, j);
                this->core[ i + arg1*j + arg2*k] += T_xx_projected(i, j);
            }
        }
    }
    // T_yy_projected -> T_xyzxyz_projected //
    arg1 = rx;
    arg2 = rx * rx * ry * rz;
    arg3 = rx * ry * rz + 1;
    arg4 = rx * rx * ry * ry * rz  + ry * rx;
    //#pragma omp parallel for num_threads(4) collapse(4)
    for (size_t k = 0; k < rx; k++){
        for (size_t l = 0; l < rz; l++){
            for (size_t i = 0; i < ry; i++){
                for (size_t j = 0; j < ry; j++){
                    //T_xyzxyz_projected[ arg1*i + arg2*j + arg3*k + arg4*l ] += T_yy_projected(i, j);
                    this->core[ arg1*i + arg2*j + arg3*k + arg4*l ] += T_yy_projected(i, j);
                }
            }
        }
    }
    // T_zz_projected -> T_xyzxyz_projected //
    arg1 = rx * ry * rz + 1;
    arg2 = ry * rx;
    arg3 = rx * rx * ry * ry * rz;
    //#pragma omp parallel for num_threads(3) collapse(3)
    for (size_t k = 0; k < ry * rx; k++){
        for (size_t i = 0; i < rz; i++){
            for (size_t j = 0; j < rz; j++){
                //T_xyzxyz_projected[ arg1*k + arg2*i + arg3*j ] += T_zz_projected(i, j);
                this->core[ arg1*k + arg2*i + arg3*j ] += T_zz_projected(i, j);
            }
        }
    }

    // Choose Ordering: (T_xx (x) T_yy) (x) T_zz OR T_xx (x) (T_yy (x) T_zz)
    // 2-1. Kronecker product : T_xx_projected (x) T_yy_projected //
    // ?ger or ?geru or ?gerc
/*
    Matrix<double> T_xyxy_projected(T_xx_projected.row * T_yy_projected.row, T_xx_projected.column * T_yy_projected.column);
    for (size_t i = 0; i < T_xx_projected.column; i++){
        for (size_t j = 0; j < T_yy_projected.column; j++){
            MKL_INT row = T_yy_projected.row;
            MKL_INT col = T_xx_projected.row;
            MKL_INT lda = T_xx_projected.row * T_yy_projected.row;
            cblas_dger(CblasColMajor, row, col, 1, T_yy_projected.pointer.data() + j*T_yy_projected.row,
                        1, T_xx_projected.pointer.data() + i*T_xx_projected.row, 
                        1, T_xyxy_projected.pointer.data() + i*lda*T_yy_projected.column + j*lda, row);
        }
    }
*/
    if (TIME_CHECK){
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, std::vector<datatype> t_xx, std::vector<datatype> t_yy, std::vector<datatype> t_zz)");
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::project_hamiltonian(SparseTensor<datatype, dimension>& tensor){
    Clock::instance().start("Tucker<datatype, dimension>::project_hamiltonian(SparseTensor<datatype, dimension>& tensor)");
    static_assert(dimension==6, "dimension for Tucker::project_hamiltonian should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    const std::vector<std::pair<std::array<size_t, dimension>, datatype>>& data = tensor.data;
    const size_t size_k = data.size();
    const size_t size_i = this->ranks_mult[dimension];
    #pragma omp parallel for
    for (size_t i=0; i < size_i; i++){
        const auto index = get_tensor_index(i, this->ranks);
        for (size_t k=0; k<size_k; k++){
            datatype val = data[k].second;
            #pragma unroll
            for (size_t j=0; j<dimension ; j++){
                val *= this->Us[j](data[k].first[j] , index[j] );
            }
            this->core[i]+=val;
        }
    }
    Clock::instance().end("Tucker<datatype, dimension>::project_hamiltonian(SparseTensor<datatype, dimension>& tensor)");
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::project_local_potential(double* local_potential, size_t project_axis_level){
    Clock::instance().start("Tucker<datatype, dimension>::project_local_potential(std::vector<datatype> local_potential, size_t project_axis_level)");
    static_assert(dimension==6, "dimension for Tucker::project_local_potential should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    //  project_axis == 3 : do projection along x,y,z-axes  //
    //  project_axis == 2 : do projection along x,y-axes    //
    //  project_axis == 1 : do projection along x-axis      //

    // Time Check //
    bool TIME_CHECK = true;
    double start, end, start_time, end_time;
    if(TIME_CHECK){
        start_time = omp_get_wtime();
        std::cout << "====== project_local_potential =====" << std::endl;
    }

    const size_t N_x = this->shape[0];    const size_t N_y = this->shape[1];    const size_t N_z = this->shape[2];
    const size_t r_x = this->ranks[0];    const size_t r_y = this->ranks[1];    const size_t r_z = this->ranks[2];
    const size_t r_xy = r_x * r_y;
    const size_t N_yz = N_y * N_z;

    // assert //
    assert(project_axis_level == 1 || project_axis_level == 2 || project_axis_level == 3); 
    if (project_axis_level == 1){
        assert(N_y == r_y && N_z == r_z);
    }
    else if (project_axis_level == 2){
        assert(N_z == r_z);
    }

    if (TIME_CHECK){ start = omp_get_wtime(); }

    std::vector<Matrix<datatype>> M_1d(N_yz, Matrix<datatype>(r_x,r_x));
    #pragma omp parallel for
    for (size_t i = 0; i < N_yz; i++){
        for (size_t a = 0; a < r_x; a++){
            for (size_t b = 0; b < r_x; b++){
                for (size_t n = 0; n < N_x; n++){
                    const datatype alpha = this->Us[0](n, a) * this->Us[3](n, b);
                    M_1d[i](a, b) += local_potential[n + i*N_x] * alpha;
                }
            }
        }
    }

    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 1 : " << end - start << " s" << std::endl;
    }

    if (project_axis_level == 1){
        // Copy and add M_1d's values to Core_tensor //
        const size_t ldc1 = r_x * N_y * N_z;
        #pragma omp parallel for
        for (size_t i = 0; i < N_yz; i++){
            omatadd<datatype>('C', 'N', 'N', r_x, r_x, 1, M_1d[i].pointer.data(), r_x, 1, this->core.data() + r_x*i + r_x*r_x*N_y*N_z*i, 
                              ldc1, this->core.data() + r_x*i + r_x*r_x*N_y*N_z*i, ldc1);
        }
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
        return;
    }

    if (TIME_CHECK){ start = omp_get_wtime(); }

    std::vector<Matrix<datatype>> M_2d(N_z, Matrix<datatype>(r_xy, r_xy));
    const size_t ldc2 = r_xy;
    #pragma omp parallel for
    for (size_t i = 0; i < N_z; i++){
        for (size_t a = 0; a < r_y; a++){
            for (size_t b = 0; b < r_y; b++){
                for (size_t n = 0; n < N_y; n++){
                    // M_2d[i](a,b) += (U_na * U_nb) * M_1d[n] //
                    const datatype alpha = this->Us[1](n, a) * this->Us[4](n, b);   // U^T U ??
                    omatadd<datatype>('C', 'N', 'N', r_x, r_x, alpha, M_1d[n + i*N_y].pointer.data(), r_x, 1, M_2d[i].pointer.data() + r_x*a + r_xy*r_x*b, ldc2, 
                                      M_2d[i].pointer.data() + r_x*a + r_xy*r_x*b, ldc2);
                }
            }
        }
    }

    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 2 : " << end - start << " s" << std::endl;
    }

    if (project_axis_level == 2){
        // Copy and add M_2d's values to Core_tensor //
        const size_t ldc2 = r_xy * N_z;
        #pragma omp parallel for
        for (size_t i = 0; i < N_z; i++){
            omatadd<datatype>('C', 'N', 'N', r_xy, r_xy, 1, M_2d[i].pointer.data(), r_xy, 1, this->core.data() + r_xy*i + r_xy*r_xy*N_z*i, ldc2, 
                              this->core.data() + r_xy*i + r_xy*r_xy*N_z*i, ldc2);
        }
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
        return;
    }


    // free M_1d memory
    std::vector<Matrix<datatype>>().swap(M_1d);
                
    if (TIME_CHECK){ start = omp_get_wtime(); }
    const size_t r_xyz = r_x * r_y * r_z;
    Matrix<datatype> M_3d(r_xyz, r_xyz);
    const size_t ldc3 = r_xyz;
    #pragma omp parallel for    // Even if parallel is used, no speed up at all.
    for (size_t a = 0; a < r_z; a++){
        //for (size_t b = a; b < r_z; b++){     // Upper triangular
        for (size_t b = 0; b < r_z; b++){
            for (size_t n = 0; n < N_z; n++){
                //const datatype alpha = this->Us[2](n, a) * this->Us[2](n, b);
                const datatype alpha = this->Us[2](n, a) * this->Us[5](n, b);
                //const datatype alpha = this->Us[5](n, a) * this->Us[2](n, b);
                omatadd<datatype>('C', 'N', 'N', r_xy, r_xy, alpha, M_2d[n].pointer.data(), r_xy, 1, M_3d.pointer.data() + r_xy*a + r_xyz*r_xy*b, ldc3,
                                  M_3d.pointer.data() + r_xy*a + r_xyz*r_xy*b, ldc3);
            }
        }
    }

    #pragma omp parallel for
    for (size_t i = 0; i < r_xyz*r_xyz; i++){
        this->core[i] += M_3d[i];
    }
    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 3 : " << end - start << " s" << std::endl;
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::project_local_potential(std::vector<datatype> local_potential, size_t project_axis_level)");
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::new_project_local_potential(double* local_potential, size_t project_axis_level){
    Clock::instance().start("Tucker<datatype, dimension>::project_local_potential(std::vector<datatype> local_potential, size_t project_axis_level)");
    static_assert(dimension==6, "dimension for Tucker::project_local_potential should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    //  project_axis == 3 : do projection along x,y,z-axes  //
    //  project_axis == 2 : do projection along x,y-axes    //
    //  project_axis == 1 : do projection along x-axis      //

    // Time Check //
    bool TIME_CHECK = true;
    double start, end, start_time, end_time;
    if(TIME_CHECK){
        start_time = omp_get_wtime();
        std::cout << "====== project_local_potential =====" << std::endl;
    }

    const size_t N_x = this->shape[0];    const size_t N_y = this->shape[1];    const size_t N_z = this->shape[2];
    const size_t r_x = this->ranks[0];    const size_t r_y = this->ranks[1];    const size_t r_z = this->ranks[2];
    const size_t r_xy = r_x * r_y;
    const size_t N_yz = N_y * N_z;

    // assert //
    assert(project_axis_level == 1 || project_axis_level == 2 || project_axis_level == 3); 
    if (project_axis_level == 1){
        assert(N_y == r_y && N_z == r_z);
    }
    else if (project_axis_level == 2){
        assert(N_z == r_z);
    }

    if (TIME_CHECK){ start = omp_get_wtime(); }

    std::vector<Matrix<datatype>> M_1d(N_yz, Matrix<datatype>(r_x,r_x));
    //#pragma omp parallel for collapse(3)
    for (size_t n = 0; n < N_x; n++){
    	for (size_t a = 0; a < r_x; a++){
        	for (size_t b = 0; b < r_x; b++){
                const datatype alpha = this->Us[0](n, a) * this->Us[3](n, b);
                #pragma omp parallel for 
                for (size_t i = 0; i < N_yz; i++){
                    M_1d[i](a, b) += local_potential[n + i*N_x] * alpha;
                }
            }
        }
    }

    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 1 : " << end - start << " s" << std::endl;
    }

    if (project_axis_level == 1){
        // Copy and add M_1d's values to Core_tensor //
        const size_t ldc1 = r_x * N_y * N_z;
        #pragma omp parallel for
        for (size_t i = 0; i < N_yz; i++){
            omatadd<datatype>('C', 'N', 'N', r_x, r_x, 1, M_1d[i].pointer.data(), r_x, 1, this->core.data() + r_x*i + r_x*r_x*N_y*N_z*i, 
                              ldc1, this->core.data() + r_x*i + r_x*r_x*N_y*N_z*i, ldc1);
        }
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
        return;
    }

    if (TIME_CHECK){ start = omp_get_wtime(); }

    std::vector<Matrix<datatype>> M_2d(N_z, Matrix<datatype>(r_xy, r_xy));
    const size_t ldc2 = r_xy;
//    mkl_set_num_threads(1);
//    #pragma omp parallel for collapse(2)    // Not work well ( omp_num_threads=8 -> x2 speed up )
//    #pragma omp parallel for
    for (size_t a = 0; a < r_y; a++){
        //for (size_t b = a; b < r_y; b++){   // Upper triangular
        for (size_t b = 0; b < r_y; b++){
            //#pragma omp parallel for
            for (size_t n = 0; n < N_y; n++){
                // M_2d[i](a,b) += (U_na * U_nb) * M_1d[n] //
                const datatype alpha = this->Us[1](n, a) * this->Us[4](n, b);   // U^T U ??
                for (size_t i = 0; i < N_z; i++){
                    omatadd<datatype>('C', 'N', 'N', r_x, r_x, alpha, M_1d[n + i*N_y].pointer.data(), r_x, 1, M_2d[i].pointer.data() + r_x*a + r_xy*r_x*b, ldc2, 
                                      M_2d[i].pointer.data() + r_x*a + r_xy*r_x*b, ldc2);
                }
            }
        }
    }
//    mkl_set_num_threads(omp_get_num_threads());

    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 2 : " << end - start << " s" << std::endl;
    }

    if (project_axis_level == 2){
        // Copy and add M_2d's values to Core_tensor //
        const size_t ldc2 = r_xy * N_z;
        #pragma omp parallel for
        for (size_t i = 0; i < N_z; i++){
            omatadd<datatype>('C', 'N', 'N', r_xy, r_xy, 1, M_2d[i].pointer.data(), r_xy, 1, this->core.data() + r_xy*i + r_xy*r_xy*N_z*i, ldc2, 
                              this->core.data() + r_xy*i + r_xy*r_xy*N_z*i, ldc2);
        }
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
        return;
    }


    // free M_1d memory
    std::vector<Matrix<datatype>>().swap(M_1d);
                
    if (TIME_CHECK){ start = omp_get_wtime(); }
    const size_t r_xyz = r_x * r_y * r_z;
    Matrix<datatype> M_3d(r_xyz, r_xyz);


    const int group_count = r_z*r_z*N_z;
    std::vector<int> n_array(group_count, r_xy);
    std::vector<datatype> alpha (group_count);
    datatype** x_array = new datatype*[group_count*r_xy];
    std::vector<int> incx_array(group_count, 1);
    datatype** y_array = new datatype*[group_count*r_xy];
    std::vector<int> incy_array(group_count, 1);
    std::vector<int> group_size(group_count, r_xy);

    int group_index;
    #pragma omp parallel for collapse(3) private(group_index)  
    for (size_t a = 0; a < r_z; a++){
        for (size_t b = 0; b < r_z; b++){
            for (size_t n = 0; n < N_z; n++){
                group_index=r_z*N_z*a+N_z*b+n;
                alpha[group_index] = this->Us[2](n, a) * this->Us[5](n, b);
                for (size_t m=0; m<r_xy; m++){
                    x_array[group_index*r_xy+m] = M_2d[n].pointer.data()+r_xy*m;
                    y_array[group_index*r_xy+m] = M_3d.pointer.data() + r_xy*a + r_xyz*r_xy*b+r_xyz*m;
                }
    		}
		}
	}


    axpy_batch<datatype>((const int*) n_array.data(), (const datatype*) alpha.data(), (const datatype**) x_array, (const int*) incx_array.data(), (datatype**) y_array, (const int*) incy_array.data(), group_count, (const int*) group_size.data());
    delete[] x_array;
    delete[] y_array;

//    #pragma omp parallel 
//    {
//    #pragma omp single nowait
//    mkl_set_num_threads(omp_get_num_threads());
//    }

    #pragma omp parallel for
    for (size_t i = 0; i < r_xyz*r_xyz; i++){
        this->core[i] += M_3d[i];
    }
    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 3 : " << end - start << " s" << std::endl;
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::project_local_potential(std::vector<datatype> local_potential, size_t project_axis_level)");
    return;
}

// for general case
//template <class datatype, size_t dimension>
//void Tucker<datatype, dimension>::hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core){
//    static_assert(dimension==6, "dimension for Tucker::diagonalize should be 6");
//    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
//    /*
//    If 'calculate_core' == false, only U matrices are calculated.
//
//    (Usage example)
//    auto tucker = hosvd(tensor, ranks, true);
//    */
//    double start, end, start_time, end_time;
//    bool TIME_CHECK = true;
//    if (TIME_CHECK){
//        start_time = omp_get_wtime();
//        //std::cout << "== hosvd (SparseTensor) time check ==" << std::endl;
//        std::cout << "====== HOSVD time measurement ======" << std::endl;
//    }
//
//    if (TIME_CHECK) { start = omp_get_wtime(); }
//    // Store information about ranks, and set memory size of core tensor.
////    auto tucker = Tucker<datatype, dimension>(tensor.shape, ranks);
////    assert(tucker.ranks_mult[dimension] != 0);
//
////    int n_core;
////    #pragma omp parallel
////    {
////        #pragma omp master
////        n_core = omp_get_num_threads();
////    }
////    mkl_set_num_threads( n_core / 6);
////    #pragma omp parallel for num_threads(6)
//    //#pragma omp parallel for
//    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! OMP performance should be checked later. (20201023)
//    for (size_t dim = 0; dim < dimension; ++dim) {
//        std::vector<int> Bp;
//        std::vector<int> Bj;
//        std::vector<datatype> Bx;
//        tensor.to_csr(dim, Bp, Bj, Bx);
//        get_left_singular_vector(tensor.shape[dim], tensor.shape_mult[dimension]/tensor.shape[dim], Bp, Bj, Bx, this->Us[dim], this->ranks[dim]);
//    }
//    if (TIME_CHECK) {
//        end = omp_get_wtime();
//        std::cout << "|\tstep 1 (SVD): " << end - start << " s" << std::endl;
//    }
//
//    if (calculate_core == false){
//        if (TIME_CHECK){
//            end_time = omp_get_wtime();
//            std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
//            std::cout << "==================================== " << std::endl;
//        }
//        return;
//    }
//
//    if (TIME_CHECK) { start = omp_get_wtime(); }
//    const std::vector<std::pair<std::array<size_t, dimension>, datatype>>& data = tensor.data;
//    const size_t size_k = data.size();
//    const size_t size_i = this->ranks_mult[dimension];
//    #pragma omp parallel for // collapse(2)
//    for (size_t i=0; i<size_i; i++){
//        const auto index = get_tensor_index(i, this->ranks);
//        for (size_t k=0; k<size_k; k++){
//            datatype val = data[k].second;
//            #pragma unroll
//            for (size_t j=0; j<dimension ; j++){
//                val*=this->Us[j](data[k].first[j] , index[j] );
//            }
////            #pragma omp atomic
//             this->core[i]+=val;
//         }
//     }
//    if (TIME_CHECK) {
//        end = omp_get_wtime();
//        std::cout << "|\tstep 2 (fold): " << end - start << " s" << std::endl;
//    }
//    if (TIME_CHECK){
//        end_time = omp_get_wtime();
//        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
//        std::cout << "==================================== " << std::endl;
//    }
//    return;
//}
//

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core){
    Clock::instance().start("Tucker<datatype, dimension>::hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core)");
    static_assert(dimension==6, "dimension for Tucker::hosvd should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    /*
    For decomposition of hamiltonian 6-dimensional tensor.
    Calculate Us efficiently using symmetry of hamiltonian.
    If 'calculate_core' == false, only U matrices are calculated.

    (Usage example)
    auto tucker = hosvd(tensor, ranks, true);
    */
    const size_t dimension = 6;
    double start, end, start_time, end_time;
    bool TIME_CHECK = true;
    if (TIME_CHECK){
        start_time = omp_get_wtime();
        //std::cout << "== hosvd (SparseTensor) time check ==" << std::endl;
        std::cout << "====== HOSVD time measurement ======" << std::endl;
    }

    if (TIME_CHECK) { start = omp_get_wtime(); }
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! OMP performance should be checked later. (20201023)
    //std::cout << "using symmetry of U pairs" << std::endl;
    for (size_t dim = 0; dim < 3; ++dim) {
        std::vector<int> Bp;
        std::vector<int> Bj;
        std::vector<datatype> Bx;
        tensor.to_csr(dim, Bp, Bj, Bx);
        get_left_singular_vector(tensor.shape[dim], tensor.shape_mult[dimension]/tensor.shape[dim], Bp, Bj, Bx,
                                 this->Us[dim], this->singular_values[dim], this->singular_vectors[dim], this->ranks[dim]);
        //this->Us[dim+3] = this->Us[dim];
        this->Us[dim+3] = this->Us[dim].conj();
        this->singular_values[dim+3] = this->singular_values[dim];
        this->singular_vectors[dim+3] = this->singular_vectors[dim].conj();
    }
    if (TIME_CHECK) {
        end = omp_get_wtime();
        std::cout << "|\tstep 1 (SVD): " << end - start << " s" << std::endl;
    }

    if (calculate_core == false){
        if (TIME_CHECK){
            end_time = omp_get_wtime();
            std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
            std::cout << "==================================== " << std::endl;
        }
        return;
    }

    if (TIME_CHECK) { start = omp_get_wtime(); }
    const std::vector<std::pair<std::array<size_t, dimension>, datatype>>& data = tensor.data;
    const size_t size_k = data.size();
    const size_t size_i = this->ranks_mult[dimension];
    #pragma omp parallel for // collapse(2)
    for (size_t i=0; i<size_i; i++){
        const auto index = get_tensor_index(i, this->ranks);
        for (size_t k=0; k<size_k; k++){
            datatype val = data[k].second;
            #pragma unroll
            for (size_t j=0; j<dimension ; j++){
                val*=this->Us[j](data[k].first[j] , index[j] );
            }
//            #pragma omp atomic
             this->core[i]+=val;
         }
     }
    if (TIME_CHECK) {
        end = omp_get_wtime();
        std::cout << "|\tstep 2 (fold): " << end - start << " s" << std::endl;
    }
    if (TIME_CHECK){
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    Clock::instance().end("Tucker<datatype, dimension>::hosvd(SparseTensor<datatype, dimension>& tensor, std::array<size_t, dimension> ranks, bool calculate_core)");
    return;
}


template <class datatype>
datatype conj(const datatype a){
    static_assert(true, "not implemented yet");
}

template<>
std::complex<double> conj(const std::complex<double> a){
    return std::conj(a);
}
template<>
double conj(const double a){
    return a;
}

template <class datatype, size_t dimension>
int Tucker<datatype, dimension>::get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype>& Bx, Matrix<datatype>& U, std::vector<double>& singular_values, Matrix<datatype>& singular_vector, const size_t rank) {
//    Clock::instance().start("Tucker<datatype, dimension>::get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype> & Bx, Matrix<datatype>& U, std::vector<datatype>& singular_values, Matrix<datatype>& singular_vector, const size_t rank)");
    static_assert(dimension==6, "dimension for Tucker::get_left_singular_vector should be 6");
    static_assert(std::is_same<datatype,double>::value or std::is_same<datatype,std::complex<double> >::value, "Only two data types(double or std::complex<double>) are allowed ");
    MKL_INT k;                      /* Output: Total number of sv calculated */
    /* Output: E - k entries of singular values, XL - k corresponding left-singular vectors, XR - k corresponding right-singular vectors */
    Matrix<datatype> M (row, row);
    Matrix<datatype> U_unsorted (row, row);

    /* Sparse BLAS IE variables */
//    sparse_status_t status;
//    sparse_matrix_t A; // = NULL; /* Handle containing sparse matrix in internal data structure */
//    sparse_matrix_t B; //= NULL; /* Handle containing sparse matrix in internal data structure */
//    // We tested create_csc function 
//    status = sparse_create_csr<datatype>( &A, SPARSE_INDEX_BASE_ZERO, row, col, const_cast<int*>(Bp.data()), 
//                                                                                const_cast<int*>(Bp.data())+1, 
//                                                                                const_cast<int*>(Bj.data()), 
//                                                                                const_cast<double*>(Bx.data()) );
//    assert(SPARSE_STATUS_SUCCESS==status);
//
//    // compute B = A^T A 
//    // We tested spmmd function but it does not work fine (v19.0.5)  
//    status = mkl_sparse_syrk(SPARSE_OPERATION_NON_TRANSPOSE,A, &B); 
//    assert(SPARSE_STATUS_SUCCESS==status);
//    // set dense matrix (output of syrk contains only upper-triangular part so it is required to symmetrize ouput
//    int* rows_start  =  NULL;
//    int* rows_end    =  NULL;
//    int* col_indx    =  NULL;
//    datatype* values =  NULL;
//    int row_, col_;
//    sparse_index_base_t index_base;
//    double start_time = omp_get_wtime();
//    status = sparse_export_csr<datatype> (B, &index_base, &row_, &col_, &rows_start, &rows_end, &col_indx, &values);
//    double end_time = omp_get_wtime();
//    assert(SPARSE_STATUS_SUCCESS==status);
//    #pragma omp parallel for 
//    for (int i =0; i< row_; i++){
//        for (int j=rows_start[i]; j<rows_end[i]; j++){
//            M(i, col_indx[j]) = values[j];
//            M(col_indx[j], i) = values[j];
//        }
//    }
//    std::cout << M <<std::endl<<end_time-start_time <<std::endl;
    //

    // compute B = A^T A 
    #pragma omp parallel for schedule(dynamic,1)
    for (size_t i = 0; i < row; i++){
        // off diagonal term
        for (size_t j = i+1; j < row; j++){
            datatype value{};
            const int*      ind_ptr_a  = &Bj[Bp[i]];
            const int*      ind_ptr_b  = &Bj[Bp[j]];
            const datatype* val_ptr_a  = &Bx[Bp[i]];
            const datatype* val_ptr_b  = &Bx[Bp[j]];
            const int*      end_ptr_a  = &Bj[Bp[i+1]];
            const int*      end_ptr_b  = &Bj[Bp[j+1]];
            while (ind_ptr_a!=end_ptr_a && ind_ptr_b!=end_ptr_b){
                if(*ind_ptr_a>*ind_ptr_b){
                    ind_ptr_b++;
                    val_ptr_b++;
                }
                else if(*ind_ptr_a<*ind_ptr_b){
                    ind_ptr_a++;
                    val_ptr_a++;
                }
                else{
                    // (*ind_ptr_a==*ind_ptr_b)
                    value+= conj<datatype>(*val_ptr_a)*(*val_ptr_b);
                    ind_ptr_b++;
                    val_ptr_b++;
                    ind_ptr_a++;
                    val_ptr_a++;
                }
            }
            M(i, j) = value;
            M(j, i) = conj<datatype>(value);
        }
        // diagonal term
        const int*      ind_ptr_a  = &Bj[Bp[i]];  
        const datatype* val_ptr_a  = &Bx[Bp[i]];
        const int*      end_ptr_a  = &Bj[Bp[i+1]];
        datatype value{};
        for(; ind_ptr_a!=end_ptr_a; ind_ptr_a++){
            value+=conj<datatype>(*val_ptr_a)*(*val_ptr_a);
            val_ptr_a++;
        }
        M(i,i) = value;
    }
    //double end_time = omp_get_wtime();
    //std::cout << M <<std::endl<<end_time-start_time <<std::endl;
//    exit(-1);

    sym_eigen_solver(M, U_unsorted, singular_values);

    // Sort U in desending order.
    #pragma omp parallel for collapse(2)
    for (size_t i = 0; i < row; ++i){
        for (size_t j = 0; j < rank; ++j){
            U(i,j) = U_unsorted(i,row-j-1);
        }
    }

    // Sort U in desending order.
    #pragma omp parallel for collapse(2)
    for (size_t i = 0; i < row; ++i){
        for (size_t j = 0; j < row; ++j){
            singular_vector(i,j) = U_unsorted(i, row-j-1);
        }
    }

    //assert(mkl_sparse_destroy(A)==SPARSE_STATUS_SUCCESS);
    //assert(mkl_sparse_destroy(B)==SPARSE_STATUS_SUCCESS);

    //
    //Not necessary to deallocate arrays for which we don't allocate memory: rows_end, col_indx, values
    //(please refer example of mkl, spblasc/source/sparse_spmm.c )
    //
        
//    Clock::instance().end("Tucker<datatype, dimension>::get_left_singular_vector(const size_t row, const size_t col, const std::vector<int>& Bp, const std::vector<int>& Bj, const std::vector<datatype> & Bx, Matrix<datatype>& U, std::vector<datatype>& singular_values, Matrix<datatype>& singular_vector, const size_t rank)");
    return 0;
}

template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::project_KB_projector(
std::vector<size_t> D_shape, std::vector<datatype> D_val, std::vector<size_t> KB_shape, std::vector<size_t> KB_row, std::vector<size_t> KB_col, std::vector<datatype> KB_val
){
    Clock::instance().start("Tucker<datatype, dimension>::project_KB_projector(std::vector<size_t> D_shape, std::vector<double> D_val, std::vector<size_t> KB_shape, std::vector<size_t> KB_row, std::vector<size_t> KB_col, std::vector<datatype> KB_val)");
    static_assert(dimension==6, "dimension for Tucker::project_KB_projector should be 6");
    /*  project Pseudopotential nonlocal values to core tensor. 
     *  Eq)
     *      U^{T} \hat{V}^{NL} U = U^{T} ( P^{T} D P ) U
     *                           = (PU)^{T} D (PU)
     *      ; U : Tucker decomposition's projection matrix
     *        D : DIJ matrix in UPF
     *        P : <proj|r> matrix
     *
     *  #1. compute (PU)
     *  #2. compute (PU)^{T} D (PU)
     */
    assert (D_shape.size() == 2);
    assert (KB_shape.size() == 2);
    if (D_shape[0] == 0){ // When an atom has no projector function.
        return;
    }

    double start, end, start_time, end_time;
    bool TIME_CHECK = true;
    if (TIME_CHECK){
        start_time = omp_get_wtime();
        std::cout << "======= project_KB_projector =======" << std::endl;
    }

    if (TIME_CHECK){
        start = omp_get_wtime();
    }
    // #1. compute (PU)
    const size_t N  = this->shape[0] * this->shape[1] * this->shape[2];
    const size_t R  = this->ranks[0] * this->ranks[1] * this->ranks[2];
    assert (R == this->core.size());
    const size_t LM = D_shape[0];
    assert (D_shape[0] == D_shape[1]);
    Matrix<datatype> D_matrix(D_shape[0], D_shape[1], D_val);

    std::array<size_t, 2> shape_arr = {KB_shape[0], KB_shape[1]};

    SparseTensor<datatype, 2> kb_projector(shape_arr, KB_row, KB_col, KB_val);
    Matrix<datatype> PU(LM, R);

    const std::vector<std::pair<std::array<size_t, 2>, datatype>>& data = kb_projector.data;
    const size_t size_k = data.size();
    std::array<size_t, 3> ranks = {this->ranks[0], this->ranks[1], this->ranks[2]};
    std::array<size_t, 3> shape = {this->shape[0], this->shape[1], this->shape[2]};
    #pragma omp parallel for
    for (size_t r = 0; r < R; r++){
        const std::array<size_t, 3> indx_r = get_tensor_index<3>(r, ranks, "F");
        for (size_t k = 0; k < size_k; k++){
            datatype val = data[k].second;
            const size_t lm = data[k].first[0];
            const std::array<size_t, 3> indx_n = get_tensor_index<3>(data[k].first[1], shape, "C");
            #pragma unroll
            for (size_t dim = 0; dim < 3; dim++){
                val *= this->Us[dim](indx_n[dim], indx_r[dim]);
            }
            PU(lm, r) += val;
        }
    }

    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 1 (compute PU)  : " << end - start << " s" << std::endl;
        start = omp_get_wtime();
    }

    // #2. compute (PU)^{T} D (PU)
    Matrix<datatype> projected_matrix = multiply(PU, D_matrix * PU.conj(), "T", "NoT");
    #pragma omp parallel for
    for (size_t r = 0; r < R*R; r++){
        this->core[r] += projected_matrix[r];
    }
    if (TIME_CHECK){
        end = omp_get_wtime();
        std::cout << "|\tstep 2 (PU^{T} D PU) : " << end - start << " s" << std::endl;
    }

    if (TIME_CHECK){
        end_time = omp_get_wtime();
        std::cout << "|\ttotal time = " << end_time - start_time << " s" << std::endl;
        std::cout << "==================================== " << std::endl;
    }
    //Clock::instance().end("Tucker<datatype, dimension>::project_KB_projector(std::vector<size_t> D_shape, std::vector<datatype> D_val, std::vector<size_t> KB_shape, std::vector<size_t> KB_row, std::vector<size_t> KB_col, std::vector<datatype> KB_val)");
    return;
}


template <class datatype, size_t dimension>
void Tucker<datatype, dimension>::clear(){
    Clock::instance().start("Tucker<datatype, dimension>::clear()");
    // clear core tensor elements
    #pragma omp parallel for
    for (size_t r = 0; r < this->core.size(); r++){
        this->core[r] = 0.0;
    }
    Clock::instance().end("Tucker<datatype, dimension>::clear()");
}
}
