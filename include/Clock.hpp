#pragma once 
#include<chrono>
#include<iostream>
#include<string>
#include<map>
#include<numeric>
namespace TensorDecompose{

using std::chrono::system_clock;
using std::chrono::duration;
class Clock{

//    friend std::ostream &operator << (std::ostream &c, const Clock  &clock);
//    friend std::ostream &operator << (std::ostream &c, const Clock* &clock);
    public:
        static Clock& instance(){
            static Clock myClock;
            return myClock;
        }

        Clock(Clock const&) = delete;             // Copy construct
        Clock(Clock&&) = delete;                  // Move construct
        Clock& operator=(Clock const&) = delete;  // Copy assign
        Clock& operator=(Clock &&) = delete;      // Move assign

        void start(std::string tag){
            auto result=map.find(tag);
            if( result==map.end()){
                map[tag] = map.size();
                tags.push_back(tag);
                start_times.push_back(system_clock::now());
                end_times.push_back(start_times[start_times.size()-1]); // put the same value on end_times temperaly 
                elapsed_times.push_back(std::vector<std::chrono::duration<double> >());
            }
            else{
                start_times[result->second]=system_clock::now();
            }
            return ;
        };
        void end(std::string tag){
            if( map.find(tag)==map.end() ){
                std::cout << "given tag:  " << tag <<std::endl;
                std::cout << "Clock::No such tag exist" <<std::endl;
                exit(-1);
            }
            auto index = map.find(tag)->second;
            end_times[index] = system_clock::now();
            elapsed_times[index].push_back(end_times[index]-start_times[index]);
            return ;
        };
        
        std::pair<double,size_t> get_wall_time(std::string tag){
            if( map.find(tag)==map.end() ){
                std::cout << "given tag:  " << tag <<std::endl;
                std::cout << "Time_Measure::No such tag exist" <<std::endl;
                exit(-1);
            }
            auto index = map.find(tag)->second;
            std::chrono::duration<double> total_time = std::chrono::duration<double>::zero();
            for(size_t i=0; i<elapsed_times[index].size(); i++){
                total_time+=elapsed_times[index][i];
            }
            return std::make_pair(total_time.count(), elapsed_times[index].size());
        };
        void print(){
            for (auto iter=map.begin(); iter!=map.end(); iter++){
                auto index = iter->second;
                std::cout << iter->first << "\t" << std::accumulate(elapsed_times[index].begin(), 
                                                              elapsed_times[index].end(),
                                                              std::chrono::duration<double>::zero()).count()
                          << "\t(" << elapsed_times[index].size() << ")" <<std::endl;
            }
        };

    protected:
        Clock(){  };
        ~Clock(){ print(); };

        /// Contain map between name and index
        std::map<std::string, int> map;
        /// Contains name of all tags.
        std::vector<std::string> tags;
        /// Contains elapsed time of all tags.
        std::vector<std::vector<std::chrono::duration<double> > > elapsed_times;
        std::vector<system_clock::time_point> start_times;
        std::vector<system_clock::time_point> end_times;
        std::vector<int> counts;

        //static Clock* pClock ;
};


};
