#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include "mkl.h"
#include <cassert>
#include "Tensor.hpp"
#include "Util.hpp"
#include <functional>
#include <algorithm>
#include <utility>
#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include "mkl_solvers_ee.h"


namespace TensorDecompose{
template<class datatype, size_t dimension>
class SparseTensor: public Tensor<datatype, dimension>{
    public:
        SparseTensor(){};
        SparseTensor(std::array<size_t, dimension> shape);
        SparseTensor(std::array<size_t, dimension> shape, std::vector<std::array<size_t, dimension>> indices, std::vector<datatype> values, bool pass_complete=false);
        SparseTensor(std::array<size_t, dimension> shape, const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<datatype>& val, const std::string order="C", bool pass_complete=false) {};
        datatype& operator()(const std::array<size_t, dimension> index);
        //datatype& operator[](size_t index);
    //    std::vector<datatype> values;
    //    std::vector<std::array<size_t, dimension> > indices;
        std::vector<std::pair<std::array<size_t, dimension>, datatype>> data;

        // Make data by inserting index and value to data
        void insert_value(std::array<size_t, dimension> index, datatype value);

        void to_csr(
                   const size_t dim, 
                         std::vector<int>& Bp,      // ROW_INDEX
                         std::vector<int>& Bj,      // COL_INDEX
                         std::vector<datatype>& Bx  // values
                    );
        void from_csr(const int* row_ptr, const int* col_ind, const datatype* val,
                      const size_t row_size, const size_t col_size, const size_t val_size,
                      const std::string order, bool pass_complete);
    //    void to_coo_hamiltonian(std::vector<int>& row_indx,
    //                            std::vector<int>& col_indx,
    //                            std::vector<datatype>& values);
        void complete();
        bool get_filled(){ return this->filled; };
        void write_file(const std::string file_name);
        void read_file(const std::string file_name);
        void read_file_Epetra(const std::string file_name) { static_assert(false, "This is not implemented yet"); return; }
        void local_shift(const datatype shif_value);
    protected:
        size_t calculate_column( std::array<size_t, dimension> index, size_t dim);
        bool filled = false;
    //    Tensor<datatype,dimension> clone();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SparseTensor related function
template <class datatype, size_t dimension>
int diagonalize_iter(SparseTensor<datatype, dimension>& tensor, std::vector<datatype>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t eig_num);
template <class datatype, size_t dimension>
void matricization(const SparseTensor<datatype, dimension>& sp_tensor,
                         std::vector<int>& Bp,
                         std::vector<int>& Bj,
                         std::vector<datatype>& Bx,
                         std::string symmetry = "G");
std::vector<std::string> split(std::string str, char delimiter);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SparseTensor implementation

template <class datatype, size_t dimension>
SparseTensor<datatype, dimension>::SparseTensor(std::array<size_t, dimension> shape){
    this->shape=shape;
    //cumprod<dimension>(this->shape, this->shape_mult);
    cumprod(this->shape, this->shape_mult);
};

template <class datatype, size_t dimension>
SparseTensor<datatype, dimension>::SparseTensor(std::array<size_t, dimension> shape, std::vector<std::array<size_t, dimension>> indices, std::vector<datatype> values, bool pass_complete){
    this->shape = shape;
    assert(indices.size() == values.size());
    const size_t size = values.size();
    for (size_t k = 0; k < size; k++){
        data.push_back(std::make_pair(indices[k], values[k]));
    }
    cumprod<dimension>(this->shape, this->shape_mult);
    if(pass_complete==false){
        this->complete();
    }
};


SparseTensor<std::complex<double>, 2>::SparseTensor(std::array<size_t, 2> shape, const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<std::complex<double>>& val, const std::string order, bool pass_complete){
    /* Make 2-dimensional Sparse Matrix.
     * Input)
     * shape  : the shape of matrix
     * row    : row index of matrix elements
     * col    : col index of matrix elements
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */
    this->shape = shape;
    const size_t nnz = val.size();
    cumprod<2>(this->shape, this->shape_mult);

//    #pragma omp parallel for
    for (size_t i = 0; i < nnz; i++){
        std::array<size_t, 2> indx = {row[i], col[i]};
        this->data.push_back(std::make_pair(indx, val[i]));
    }
    if(pass_complete==false){
        this->complete();
    }
}


SparseTensor<double, 2>::SparseTensor(std::array<size_t, 2> shape, const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<double>& val, const std::string order, bool pass_complete){
    /* Make 2-dimensional Sparse Matrix.
     * Input)
     * shape  : the shape of matrix
     * row    : row index of matrix elements
     * col    : col index of matrix elements
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */
    this->shape = shape;
    const size_t nnz = val.size();
    cumprod<2>(this->shape, this->shape_mult);

//    #pragma omp parallel for
    for (size_t i = 0; i < nnz; i++){
        std::array<size_t, 2> indx = {row[i], col[i]};
        this->data.push_back(std::make_pair(indx, val[i]));
    }
    if(pass_complete==false){
        this->complete();
    }
}


SparseTensor<double, 6>::SparseTensor(std::array<size_t, 6> shape, const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<double>& val, const std::string order, bool pass_complete){
    /* Make 6-dimensional SparseTensor from 2-dimensional matrix.
     * Input)
     * shape  : the shape of tensor
     * row    : row index of matrix elements
     * col    : col index of matrix elements
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */
    assert( row.size() == col.size() && col.size() == val.size() );
    assert( shape[0] == shape[3] );
    assert( shape[1] == shape[4] );
    assert( shape[2] == shape[5] );

    this->shape = shape;
    const size_t nnz = val.size();
    cumprod<6>(this->shape, this->shape_mult);

    std::array<size_t, 3> shape_3d = {shape[0], shape[1], shape[2]};
    std::array<size_t, 4> shape_3d_mult;
    cumprod<3>(shape_3d, shape_3d_mult);

    std::vector<std::array<size_t, 6>> decomposed_indx(nnz);
//    #pragma omp parallel for
    this->data.reserve(nnz);
    #pragma omp parallel for ordered
    for (size_t i = 0; i < nnz; i++){
        std::array<size_t, 3> decomposed_row = get_tensor_index<3>(row[i], shape_3d_mult, order);
        std::array<size_t, 3> decomposed_col = get_tensor_index<3>(col[i], shape_3d_mult, order);
        for (size_t dim = 0; dim < 3; dim++){
            decomposed_indx[i][dim] = decomposed_row[dim];
            decomposed_indx[i][dim+3] = decomposed_col[dim];
        }
        #pragma omp ordered 
        this->data.push_back(std::make_pair(decomposed_indx[i], val[i]));
    }
    if(pass_complete==false){
        this->complete();
    }
}

SparseTensor<std::complex<double>, 6>::SparseTensor(std::array<size_t, 6> shape, const std::vector<size_t>& row, const std::vector<size_t>& col, const std::vector<std::complex<double> >& val, const std::string order, bool pass_complete){
    /* Make 6-dimensional SparseTensor from 2-dimensional matrix.
     * Input)
     * shape  : the shape of tensor
     * row    : row index of matrix elements
     * col    : col index of matrix elements
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */
    assert( row.size() == col.size() && col.size() == val.size() );
    assert( shape[0] == shape[3] );
    assert( shape[1] == shape[4] );
    assert( shape[2] == shape[5] );

    this->shape = shape;
    const size_t nnz = val.size();
    cumprod<6>(this->shape, this->shape_mult);

    std::array<size_t, 3> shape_3d = {shape[0], shape[1], shape[2]};
    std::array<size_t, 4> shape_3d_mult;
    cumprod<3>(shape_3d, shape_3d_mult);

    std::vector<std::array<size_t, 6>> decomposed_indx(nnz);
    this->data.reserve(nnz);
    #pragma omp parallel for ordered
    for (size_t i = 0; i < nnz; i++){
        std::array<size_t, 3> decomposed_row = get_tensor_index<3>(row[i], shape_3d_mult, order);
        std::array<size_t, 3> decomposed_col = get_tensor_index<3>(col[i], shape_3d_mult, order);
        for (size_t dim = 0; dim < 3; dim++){
            decomposed_indx[i][dim] = decomposed_row[dim];
            decomposed_indx[i][dim+3] = decomposed_col[dim];
        }
        #pragma omp ordered 
        this->data.push_back(std::make_pair(decomposed_indx[i], val[i]));
    }
    if(pass_complete==false){
        this->complete();
    }
}


template <class datatype, size_t dimension>
size_t SparseTensor<datatype, dimension>::calculate_column( std::array<size_t, dimension> index, size_t dim){
    size_t return_val = 0;
    size_t stride = 1;
    for (size_t i = 0; i < dimension; i++){
        const size_t curr_dim = dimension - i - 1;
        if(curr_dim == dim){
            continue;
        }
        return_val += stride * index[curr_dim];
        stride *= this->shape[curr_dim];
    }
    return return_val;
}

template <class datatype, size_t dimension>
void SparseTensor<datatype, dimension>::to_csr(
               const size_t dim,
                     std::vector<int>& Bp,
                     std::vector<int>& Bj,
                     std::vector<datatype>& Bx)
{
    const size_t n_row = this->shape[dim];
    const size_t n_col = this->shape_mult[dimension] / n_row;
    const size_t nnz   = this->data.size();
    //compute number of non-zero entries per row of A 
    Bp.resize(n_row+1, 0);
    Bj.resize(nnz);
    Bx.resize(nnz);

    for (size_t n = 0; n < nnz; n++){
        Bp[ this->data[n].first[dim] ]+=1;
    }

    //cumsum the nnz per row to get Bp[]
    for(size_t i = 0, cumsum = 0; i < n_row; i++){
        const size_t temp = Bp[i];
        Bp[i] = cumsum;
        cumsum += temp;
    }
    Bp[n_row] = nnz; 

    // row major
    //write Aj,Ax into Bj,Bx
    for(size_t n = 0; n < nnz; n++){
        size_t row  = this->data[n].first[dim];
        size_t dest = Bp[row];

        Bj[dest] = calculate_column(this->data[n].first, dim);
        Bx[dest] = this->data[n].second;

        Bp[row]++;
    }
    Bp.insert(Bp.begin(), 0);
    Bp.pop_back();
    //now Bp,Bj,Bx form a CSR representation (with possible duplicates)
}


void SparseTensor<double, 6>::from_csr(const int* row_ptr, const int* col_ind, const double* val,
                                       const size_t row_size, const size_t col_size, const size_t val_size,
                                       const std::string order, bool pass_complete){
    /* clear all existing information and load value from csr matrix 
     * Input)
     * row_ptr: The row_ptr vector stores the locations in the val vector that start a row
     * col_ind: The col_ind vector stores the column index 
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */

    const size_t nnz = val_size;
    const size_t row_size_ = row_size - 1;   // 'scipy.sparse.csr_matrix' has one more larger row size.
    const std::array<size_t, 3> shape_3d = {this->shape[0], this->shape[1], this->shape[2]};
    std::array<size_t, 4> shape_3d_mult;
    cumprod<3>(shape_3d, shape_3d_mult, order);

    const std::vector<size_t> row_ptr_(row_ptr, row_ptr + row_size);
    const std::vector<size_t> col_ind_(col_ind, col_ind + col_size);

    this->data.clear();
    std::vector<std::array<size_t, 6>> decomposed_indx(nnz);
    this->data.resize(nnz);
    #pragma omp parallel for schedule(dynamic)
    for (size_t i = 0; i < row_size_; i++){
        std::array<size_t, 3> decomposed_row = get_tensor_index<3>(i, shape_3d_mult, order);
        for (size_t j = row_ptr_[i]; j < row_ptr_[i+1]; j++){
            std::array<size_t, 3> decomposed_col = get_tensor_index<3>(col_ind_[j], shape_3d_mult, order);
            #pragma unroll
            for (size_t dim = 0; dim < 3; dim++){
                decomposed_indx[j][dim] = decomposed_row[dim];
                decomposed_indx[j][dim+3] = decomposed_col[dim];
            }
            this->data[j] = std::make_pair(decomposed_indx[j], val[j]);
        }
    }
    if(pass_complete==false){
        this->complete();
    }
    return;
}


void SparseTensor<std::complex<double>, 6>::from_csr(const int* row_ptr, const int* col_ind, const std::complex<double>* val,
                                                     const size_t row_size, const size_t col_size, const size_t val_size,
                                                     const std::string order, bool pass_complete){
    /* clear all existing information and load value from csr matrix 
     * Input)
     * row_ptr: The row_ptr vector stores the locations in the val vector that start a row
     * col_ind: The col_ind vector stores the column index 
     * val    : non-zero values of matrix elements
     * order  : order of the input tensor's index. "C" or "F"
    */

    const size_t nnz = val_size;
    const size_t row_size_ = row_size - 1;   // 'scipy.sparse.csr_matrix' has one more larger row size.
    const std::array<size_t, 3> shape_3d = {shape[0], shape[1], shape[2]};
    std::array<size_t, 4> shape_3d_mult;
    cumprod<3>(shape_3d, shape_3d_mult, order);

    const std::vector<size_t> row_ptr_(row_ptr, row_ptr + row_size);
    const std::vector<size_t> col_ind_(col_ind, col_ind + col_size);

    this->data.clear();
    std::vector<std::array<size_t, 6>> decomposed_indx(nnz);
    this->data.resize(nnz);
    #pragma omp parallel for schedule(dynamic)
    for (size_t i=0; i< row_size_; i++){
        std::array<size_t, 3> decomposed_row = get_tensor_index<3>(i, shape_3d_mult, order);
        for (size_t j= row_ptr_[i] ; j<row_ptr_[i+1]; j++){
            std::array<size_t, 3> decomposed_col = get_tensor_index<3>(col_ind_[j], shape_3d_mult, order);
            #pragma unroll
            for (size_t dim = 0; dim < 3; dim++){
                decomposed_indx[j][dim] = decomposed_row[dim];
                decomposed_indx[j][dim+3] = decomposed_col[dim];
            }
            this->data[j] = std::make_pair(decomposed_indx[j], val[j]);
        }
    }
    if(pass_complete==false){
        this->complete();
    }
    return;
}



template <class datatype, size_t dimension>
void SparseTensor<datatype, dimension>::complete(){
    if(this->filled) { return; }

    if (this->data.size()!=0){

        // sort, in order to capture member variable, 'this' need to be captured in lambda function
        std::sort( 
                  std::begin(this->data), std::end(this->data), 
                  [](std::pair<std::array<size_t, dimension>, datatype> item1, std::pair<std::array<size_t, dimension>, datatype> item2)
                  { return item1.first < item2.first; } 
                 );

        for (size_t i = 0; i < data.size() - 1; i++){
            if(data[i].first == data[i+1].first){
                data[i].second += data[i+1].second;
                data.erase(std::begin(data)+i+1);
                i -= 1;
            }
        }
    }
    this->filled = true;
    return;
}

template <class datatype, size_t dimension>
void SparseTensor<datatype, dimension>::insert_value(std::array<size_t, dimension> index, datatype value){
    assert(this->filled == false);
    this->data.push_back(std::make_pair(index, value));
    return;
}

template <class datatype, size_t dimension>
void SparseTensor<datatype, dimension>::write_file(const std::string file_name){
    /* Write down the information about SparseTensor (dimension, shape, nnz, indices, data)
     * (Usage example)
     * SparseTensor<double, dimension> = sp_tensor(shape);
     * sp_tensor.write_file("file_name")
     */

    assert(this->filled == true);
    std::ofstream outFile(file_name.data());
    if( outFile.is_open() ){
        // Write dimension & shape
        outFile << "dimension= \n" << dimension << " \n";
        outFile << "shape= \n";
        for (size_t i = 0; i < dimension; i++){
            outFile << this->shape[i] << " ";
        } outFile << std::endl;

        // Write data
        const size_t nnz = this->data.size();
        outFile << "nnz= \n" << nnz << " \n";
        outFile << "index= \n";
        for (size_t n = 0; n < nnz; n++){
            for (size_t dim = 0; dim < dimension; dim++){
                outFile << this->data[n].first[dim] << " ";
            } outFile << std::endl;
        }
        outFile << "values= \n";
        for (size_t n = 0; n < nnz; n++){
            outFile << this->data[n].second << " ";
        } outFile << std::endl;
    }

    outFile.close();
    return;
}

template <class datatype, size_t dimension>
void SparseTensor<datatype, dimension>::read_file(const std::string file_name){
    /* Read the file written in write_file method. And complete this SparseTensor.
     * (Usage example)
     * SparseTensor<double, dimension> = sp_tensor(shape);
     * sp_tensor.read_file("file_name")
     */
    std::ifstream inFile(file_name.data());
    if( inFile.is_open() ){
        // copy to tmp
        std::vector<std::string> tmp;
        std::string line;
        while(getline(inFile, line, ' ')){
            tmp.push_back(line);
        }

        // Index
        const size_t indx_dimension = 1;
        const size_t indx_shape = indx_dimension + 2; //3
        const size_t indx_nnz = indx_shape + dimension + 1; //10
        const size_t nnz = atoi(tmp[indx_nnz].c_str());
        const size_t indx_index = indx_nnz + 2; //12
        const size_t indx_values = indx_index + dimension * nnz + 1; //205

        // Check dimension & shape
        size_t read_dimension = atoi(tmp[indx_dimension].c_str());
        if ( !(read_dimension == dimension) ){
                std::cout << tmp[indx_dimension] << std::endl;
                std::cout << atoi(tmp[indx_dimension].c_str()) << std::endl;
            std::cout << read_dimension << " != " << dimension << std::endl;
            assert( read_dimension == dimension );
        }
        for (size_t dim = 0; dim < dimension; dim++){
            assert( atoi(tmp[indx_shape + dim].c_str()) == this->shape[dim] );
        }

        // Resize data
        this->data.resize(nnz);
        // Fill data
        for (size_t i = 0; i < nnz; i++){
            std::array<size_t, dimension> _arr;
            for (size_t dim = 0; dim < dimension; dim++){
                _arr[dim] = atoi(tmp[indx_index + i * dimension + dim].c_str());
            }
            this->data[i].first = _arr;
            if (std::is_same<datatype, double>::value){
                this->data[i].second = atof(tmp[indx_values + i].c_str());
            }
            else if (std::is_same<datatype, int>::value){
                this->data[i].second = atoi(tmp[indx_values + i].c_str());
            }
            else if (std::is_same<datatype, long>::value){
                this->data[i].second = atol(tmp[indx_values + i].c_str());
            }
            else if (std::is_same<datatype, long long>::value){
                this->data[i].second = atoll(tmp[indx_values + i].c_str());
            }
            else{
                assert(0 && "Only int, double, long, and long long data-types are possible.");
            }
        }
    }

    inFile.close();
    this->filled = true;
    return;
}

void SparseTensor<double, 6>::read_file_Epetra(const std::string file_name){
    // Read csr_format_file (from Epetra_CsrMatrix::Print)
    std::ifstream inFile(file_name.data());
    std::vector<std::string> tmp;
    //std::vector< std::vector<std::string> > tmp;

    size_t num_of_rows;
    size_t num_of_cols;
    size_t num_of_nonzeros;
    size_t data_start_line = 0;
    std::vector<size_t> row_index;  //reserve()
    std::vector<size_t> col_index;
    std::vector<double> values;

    if( inFile.is_open() ){
        std::string line;
        while(getline(inFile, line)){
            tmp.push_back(line);
            //auto line_split = split(line, ' ');
            //tmp.push_back(line_split);
        }
        // Read Rows, Cols, and Nonzeros.
        //while(getline(inFile, line)){
        for(size_t i = 0; i < tmp.size(); i++){
            data_start_line += 1;
            auto line_split = split(tmp[i], ' ');
            if(line_split.size() == 0){
                continue;
            }
            if(line_split[2] == "Global" && line_split[3] == "Rows"){
                num_of_rows = atoi((line_split[5]).c_str());
            }
            if(line_split[2] == "Global" && line_split[3] == "Cols"){
                num_of_cols = atoi((line_split[5]).c_str());
            }
            if(line_split[2] == "Global" && line_split[3] == "Nonzeros"){
                num_of_nonzeros = atoi((line_split[5]).c_str());
            }
            if(line_split[0] == "Processor"){
                break;
            }
        }

        for (size_t i = data_start_line; i < data_start_line + num_of_nonzeros; i++){
            auto line_split = split(tmp[i], ' ');
            row_index.push_back(atoi(line_split[1].c_str()));
            col_index.push_back(atoi(line_split[2].c_str()));
            values.push_back(atof(line_split[3].c_str()));
        }
    }
    inFile.close();
    tmp.clear();

    std::cout << "num_of_rows = " << num_of_rows << std::endl;
    std::cout << "num_of_cols = " << num_of_cols << std::endl;
    assert(num_of_rows == num_of_cols);
    assert(num_of_rows == this->shape_mult[3]);

    // Fill data
    this->data.resize(num_of_nonzeros);
    for (size_t i = 0; i < num_of_nonzeros; i++){
        size_t combined_index = row_index[i] + num_of_rows * col_index[i];  // Column Major
        std::array<size_t, 6> _arr = get_tensor_index<6>(combined_index, this->shape);
        this->data[i].first = _arr; 
        this->data[i].second = values[i];
    }
    this->complete();
    return;
}

std::vector<std::string> split(std::string str, char delimiter){
    std::vector<std::string> internal;
    std::stringstream ss(str);
    std::string temp;
    while (getline(ss, temp, delimiter)) {
        if(temp !=""){ 
            internal.push_back(temp);
        }
    }
    return internal;
}


template <class datatype, size_t dimension>
void matricization(const SparseTensor<datatype, dimension>& sp_tensor,
                         std::vector<int>& Bp,
                         std::vector<int>& Bj,
                         std::vector<datatype>& Bx,
                         std::string symmetry)
{
    //assert(sp_tensor.get_filled() == true);
    assert(symmetry == "S" || symmetry == "G");

    const std::array<size_t, dimension> shape = sp_tensor.shape;
    //assert(shape.size() == 6);
    assert(dimension == 6);
    assert(shape[0] == shape[3]);
    assert(shape[1] == shape[4]);
    assert(shape[2] == shape[5]);

    const size_t n_row = shape[0]*shape[1]*shape[2];
    const size_t n_col = shape[3]*shape[4]*shape[5];
    const size_t nnz   = sp_tensor.data.size();

    //compute number of non-zero entries per row of A
    if (symmetry != "S"){
        Bp.resize(n_row+1, 0);
        Bj.resize(nnz);
        Bx.resize(nnz);
    }
    else if (symmetry == "S"){
        Bp.resize(n_row+1, 0);
        Bj.resize( (nnz - n_row)/2 + n_row );
        Bx.resize( (nnz - n_row)/2 + n_row );
    }

    const size_t stride1 = sp_tensor.shape[0];
    const size_t stride2 = sp_tensor.shape[0] * sp_tensor.shape[1];

    if (symmetry != "S"){
        for (size_t n = 0; n < nnz; n++){
            const size_t row_combined_index = sp_tensor.data[n].first[0] + sp_tensor.data[n].first[1] * stride1 + sp_tensor.data[n].first[2] * stride2;
            Bp[row_combined_index] += 1;
        }
    }
    else if (symmetry == "S"){
        for (size_t n = 0; n < nnz; n++){
            const size_t row_combined_index = sp_tensor.data[n].first[0] + sp_tensor.data[n].first[1] * stride1 + sp_tensor.data[n].first[2] * stride2;
            const size_t col_combined_index = sp_tensor.data[n].first[3] + sp_tensor.data[n].first[4] * stride1 + sp_tensor.data[n].first[5] * stride2;
            if (row_combined_index <= col_combined_index){
                Bp[ row_combined_index ] += 1;
            }
        }
    }

    //cumsum the nnz per row to get Bp[]
    for(size_t i = 0, cumsum = 0; i < n_row; i++){
        const size_t temp = Bp[i];
        Bp[i] = cumsum;
        cumsum += temp;
    }
    Bp[n_row] = nnz;

    // row major
    //write Aj,Ax into Bj,Bx
    for(size_t n = 0; n < nnz; n++){
            const size_t row_combined_index = sp_tensor.data[n].first[0] + sp_tensor.data[n].first[1] * stride1 + sp_tensor.data[n].first[2] * stride2; 
            const size_t col_combined_index = sp_tensor.data[n].first[3] + sp_tensor.data[n].first[4] * stride1 + sp_tensor.data[n].first[5] * stride2; 
            const size_t dest = Bp[row_combined_index];
            if (symmetry != "S"){
                Bj[dest] = col_combined_index;
                Bx[dest] = sp_tensor.data[n].second;
                Bp[row_combined_index]++;
            }
            else if (symmetry == "S"){
                if (row_combined_index <= col_combined_index){
                    Bj[dest] = col_combined_index;
                    Bx[dest] = sp_tensor.data[n].second;
                    Bp[row_combined_index]++;
                }
            }
    }
    Bp.insert(Bp.begin(), 0);
    //now Bp,Bj,Bx form a CSR representation (with possible duplicates)

    return;
}

template <class datatype, size_t dimension>
int diagonalize_iter(SparseTensor<datatype, dimension>& tensor, std::vector<datatype>& eigen_values, std::vector<datatype>& eigen_vectors, const size_t eig_num){

//    const size_t dimension = tensor.dimension;
    static_assert(std::is_same<datatype, double>::value,"diagonalize_iter works for only double sparse tensor " );
    static_assert( dimension % 2 == 0, "This function is defined only for tensor of even-number-ranks.");

    const int row_ = tensor.shape_mult[dimension / 2];
    const int col_ = tensor.shape_mult[dimension / 2];

//    // CSR format Matrix
    std::vector<int>      Bp;
    std::vector<int>      Bj;
    std::vector<datatype> Bx;
    matricization<datatype>( tensor, Bp, Bj, Bx );
//    tensor.to_csr(0, Bp, Bj, Bx);
    // COO format
    MKL_INT nnz = tensor.data.size();
//    std::vector<int>    row_indx;
//    std::vector<int>    col_indx;
//    std::vector<double> values;
    //tensor.to_coo_hamiltonian(row_indx, col_indx, values);

    /* mkl_sparse_d_ev input paramters */
    char        which = 'S';     /* Input: 'S'indicates the smallest eigenvalues. 'L' indicates the largest eigenvalues. */
    MKL_INT     pm[128];         /* Input: Array used to pass various parameters to Extended Eigensolver Extensions routines. */

    /* mkl_sparse_d_ev output parameters */
    MKL_INT     k;                           /* Output: Total number of sv calculated */
    MKL_INT     k0 = eig_num;
    double*     res = new double[eig_num];   /* Residuals */
//    std::vector<double> E(eig_num);          /* eigenvalues */
//    std::vector<double> X(eig_num * row_);   /* eigenvectors */
    eigen_values.resize(eig_num);
    eigen_vectors.resize(eig_num * row_);

    /* Sparse BLAS IE variables */
    sparse_status_t status;
    sparse_matrix_t A = NULL;  /* Handle containing sparse matrix in internal data structure */
    struct matrix_descr descr; /* Structure specifying sparse matrix properties */
    descr.type = SPARSE_MATRIX_TYPE_GENERAL;
///    descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
    descr.mode = SPARSE_FILL_MODE_UPPER; // or SPARSE_FILL_MODE_LOWER (save memory)

    /* Create handle for matrix A stored in COO format */
//    status = mkl_sparse_d_create_coo ( &A, SPARSE_INDEX_BASE_ZERO, row_, col_, nnz, const_cast<int*>(row_indx.data()),
//                                                                                    const_cast<int*>(col_indx.data()),
//                                                                                    const_cast<double*>(values.data()) );


    status = mkl_sparse_d_create_csr ( &A, SPARSE_INDEX_BASE_ZERO, row_, col_, const_cast<int*>(Bp.data()),
                                                                               const_cast<int*>(Bp.data())+1,
                                                                               const_cast<int*>(Bj.data()),
                                                                               const_cast<double*>(Bx.data()) );

    assert(status == SPARSE_STATUS_SUCCESS);

    sparse_matrix_t B = NULL;
    /* COO to CSR format */
    status = mkl_sparse_convert_csr(A, SPARSE_OPERATION_NON_TRANSPOSE, &B);
    assert(status == SPARSE_STATUS_SUCCESS);

    /* Local variables */
    MKL_INT     compute_vectors = 1;    /* Flag to compute eigenvectors */
    MKL_INT     tol = 5;                /* Tolerance */
    //MKL_INT     info;                   /* Errors */

    /* Call mkl_sparse_ee_init to define default input values */
    auto info0 = mkl_sparse_ee_init(pm);
    pm[1] = tol;
    pm[2] = 1;  /* Kryolov Schur method */
    pm[3] = k0 * 6;//std::pow(k0, 6);  /* The number of Lanczos vectors(NCV). only for Kryolov method*/
    pm[4] = 1500000;
    pm[6] = compute_vectors;
    pm[7] = 1;  /* Use absolute stopping critertia. Iteration is stopped if norm(Ax-λx) < 10^pm[1] */
    pm[8] = 1;  /* If 1, the solvers compute true residuals. If 0, a cheap formula that gives an estimation for eigenvalue residual. */

    /* Call mkl_sparse_d_ev to obtain eigenvalues and eigenvectors */
    //info = mkl_sparse_d_svd(&whichE, &whichV, pm, A, descr, rank, &k, E.data(), XL.data(), XR.data(), res);
    //auto info = mkl_sparse_d_ev(&which, pm, A, descr, eig_num, &k, E.data(), X.data(), res);
    //auto info = mkl_sparse_d_ev(&which, pm, B, descr, k0, &k, E.data(), X.data(), res);
    auto info = mkl_sparse_d_ev(&which, pm, A, descr, k0, &k, eigen_values.data(), eigen_vectors.data(), res);
    //auto info = mkl_sparse_d_ev(&which, pm, B, descr, k0, &k, eigen_values.data(), eigen_vectors.data(), res);
    assert(info == SPARSE_STATUS_SUCCESS);
    //assert(info == SPARSE_STATUS_SUCCESS && char(info));     // error: SPARSE_STATUS_NOT_SUPPORTED (COO), SPARSE_STATUS_INVALID_VALUE (CSR)
    auto info1 = mkl_sparse_destroy(A);
    auto info2 = mkl_sparse_destroy(B);
    assert(info1 == SPARSE_STATUS_SUCCESS );
    assert(info2 == SPARSE_STATUS_SUCCESS );
    delete[] res;
//    eigen_values = E;
//    eigen_vectors = X;

    return info;
}

void SparseTensor<double, 6>::local_shift(const double shift_value){
    // Add some constant values to Diagonal elements. (to downshift all eigenvalues)
    for (size_t i = 0; i < this->data.size(); i++){
        auto arr_ = data[i].first;
        if (arr_[0] == arr_[3] && arr_[1] == arr_[4] && arr_[2] == arr_[5]){
            this->data[i].second -= shift_value;
        }
    }
}

template <class datatype, size_t dimension>
datatype& SparseTensor<datatype, dimension>::operator()(const std::array<size_t, dimension> index){
    for (size_t i = 0; i < this->data.size(); i++){
        int is_same = 0; 
        for (size_t dim = 0; dim < dimension; dim++){
            if ( (data[i].first)[dim] == index[dim] ){
                is_same += 1;
            }
        }
        if (is_same == dimension){
            return this->data[i].second;
        }
    }
}

template <class datatype, size_t dimension>
std::ostream& operator<<(std::ostream& os, SparseTensor<datatype, dimension>& sparse_tensor){
    os << std::endl;
    for (size_t i = 0; i < sparse_tensor.data.size(); i++){
        os << "( ";
        for (size_t dim = 0; dim < dimension; dim++){
            os << sparse_tensor.data[i].first[dim] << " ";
        }
        os << ")    " << sparse_tensor.data[i].second << std::endl;
    }
    return os;
}
}
