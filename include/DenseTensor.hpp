#pragma once
#include <iostream>
#include <array>
#include "Util.hpp"
#include "mkl.h"
#include <cassert>

namespace TensorDecompose{
template<class datatype, size_t dimension>
class DenseTensor: public Tensor<datatype,dimension>{
    public:
        DenseTensor(){};
        DenseTensor(std::array<size_t, dimension> shape);
        DenseTensor(std::array<size_t, dimension> shape, std::vector<datatype> data);

        datatype& operator()(const std::array<size_t, dimension> index);
        datatype& operator[](size_t index);
        DenseTensor<datatype, dimension>& operator=(const DenseTensor<datatype, dimension>& tensor);

        std::vector<datatype> data;
        void insert_value(std::array<size_t, dimension> index, datatype value);
        DenseTensor<datatype,dimension> clone() { return DenseTensor<datatype,dimension> (this->shape, this->data); };
//        void random();
};

template <class datatype, size_t dimension>
DenseTensor<datatype, dimension>::DenseTensor(std::array<size_t, dimension> shape){
    this->shape = shape;
    cumprod<dimension>(this->shape, this->shape_mult);
    assert(this->shape_mult[dimension] != 0);
    data = std::vector<datatype>(this->shape_mult[dimension], 0);
}

template <class datatype, size_t dimension>
DenseTensor<datatype, dimension>::DenseTensor(std::array<size_t, dimension> shape, std::vector<datatype> data){
    this->shape = shape;
    cumprod<dimension>(this->shape, this->shape_mult);
    assert(this->shape_mult[dimension] != 0);
    assert(this->shape_mult[dimension] == data.size());
    this->data = data;
}

template <class datatype, size_t dimension>
datatype& DenseTensor<datatype, dimension>::operator()(const std::array<size_t, dimension> index){
    //return index[0] + index[1] * dims[0] + index[2] * dims[0] * dims[1];
    size_t combined_index = 0;
    for (size_t i = 0; i < dimension; i++) {
        combined_index += index[i] * this->shape_mult[i];
    }
    return this->data[combined_index];
}

template <class datatype, size_t dimension>
datatype& DenseTensor<datatype, dimension>::operator[](size_t index) { return this->data[index]; }

template <class datatype, size_t dimension>
DenseTensor<datatype, dimension> operator+(DenseTensor<datatype, dimension>& a, DenseTensor<datatype, dimension>& b){
    if (a.shape != b.shape){
        std::cout << "Can't subtract tucker having different shape." << std::endl;
        exit(-1);
    }
    auto result = a.clone();
    cblas_daxpy(1.0, b.data.data(), 1, result.data.data(), 1 );
    return result;
}

template <class datatype, size_t dimension>
DenseTensor<datatype, dimension> operator-(DenseTensor<datatype, dimension>& a, DenseTensor<datatype, dimension>& b){
    if (a.shape != b.shape){
        std::cout << "Can't subtract tucker having different shape." << std::endl;
        exit(-1);
    }
    auto result = a.clone();
    cblas_daxpy(-1.0, b.data.data(), 1, result.data.data(), 1);
    return result;
}

template <class datatype, size_t dimension>
void DenseTensor<datatype, dimension>::insert_value(std::array<size_t, dimension> index, datatype value){
    this->operator()(index) += value;
    return;
}

//template <class datatype, size_t dimension>
//void DenseTensor<datatype, dimension>::random() {
//    // datatype == 'double'
//    for (size_t i = 0; i < this->shape_mult[dimension]; i++) {
//        this->data[i] = std::rand()/(datatype(RAND_MAX));
//    }
//    return;
//}

template <class datatype, size_t dimension>
//DenseTensor<datatype, dimension> DenseTensor<datatype, dimension>::operator=(const DenseTensor<datatype, dimension>& tensor){
DenseTensor<datatype, dimension>& DenseTensor<datatype, dimension>::operator=(const DenseTensor<datatype, dimension>& tensor){
//    DenseTensor<datatype, dimension> return_tensor(tensor.shape, tensor.data);
//    return return_tensor;
    this->shape = tensor.shape;
    cumprod<dimension>(this->shape, this->shape_mult);
    assert(this->shape_mult[dimension] != 0 );
    this->data = tensor.data;

    return *this;
}
}
