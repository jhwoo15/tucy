# cython: language_level=3
# distutils: language = c++
from libcpp.vector cimport vector
from libcpp.string cimport string

cdef extern from "Matrix.hpp" namespace "TensorDecompose":
    cppclass Matrix "TensorDecompose::Matrix<double>":
        Matrix() except+
        Matrix(size_t row, size_t column) except+
        Matrix(size_t row, size_t column, vector[double] tensor) except+
        #~Matrix()

        size_t row
        size_t column
        size_t get_row()
        size_t get_col()
        vector[double] pointer

        Matrix& multiply(Matrix& matrix1, Matrix& matrix2, string trans1, string trans2)
        #Matrix operator*(Matrix& matrix)
