import numpy as np
from ase.units import Bohr


class Grid:
    def __init__(self, atoms, gpts=None, spacing=None, FD_order=3):
        assert spacing is not None or gpts is not None, "Either 'spacing' or 'gpts' must be entered."
        if gpts is not None: assert len(gpts) == 3

        self.atoms = atoms.copy()
        self.__cell = self.atoms.get_cell()
        self.__cell_lengths = self.atoms.cell.cellpar()[:3]
        self.__cell_angles  = self.atoms.cell.cellpar()[3:]
        self.__is_cartesian = np.all(abs(self.__cell_angles - [90, 90, 90]) < 1E-7)
        if gpts is None:
            self.__gpts = np.ceil(self.__cell_lengths / np.asarray(spacing)).astype(int)
        else: # if 'gpts' is entered, 'spacing' is ignored.
            self.__gpts = np.array(gpts).astype(int)
        self.__FD_order = FD_order

        self.__total_gpts = np.prod(self.__gpts)
        self.__spacings = self.__cell_lengths / self.__gpts
        self.__points = self.make_grid_points()
        self.__center_point = [0.5, 0.5, 0.5] @ self.__cell
        self.__microvolume = np.prod(self.__spacings)
        return


    def __str__(self):
        s = str()
        s += '\n============================== [ Grid ] =============================='
        s += '\n* cell     (Bohr) : \n{}'.format(np.round(np.asarray(self.__cell), 3))
        s += '\n* cell     (ang ) : \n{}'.format(np.round(np.asarray(self.__cell) * Bohr, 3))
        s += '\n* spacings (Bohr) : {}'.format(np.round(self.__spacings, 3))
        s += '\n* spacings (ang ) : {}'.format(np.round(self.__spacings * Bohr, 3))
        s += '\n* gpts            : {}'.format(self.__gpts)
        s += '\n* total_gpts      : {}'.format(self.__total_gpts)
        s += '\n* pbc             : {}'.format(self.atoms.get_pbc())
        s += '\n* FD_order        : {}'.format(self.__FD_order)
        s += '\n======================================================================\n'
        return s


    def get_vertex_points(self):
        ## return 8 vertex points of cubic cell
        from itertools import product
        vertex = np.asarray(list(product([0,1], [0,1], [0,1]))) @ self.__cell
        return vertex


    def get_radius(self):
        ## return the radius of sphere containing ths cell
        radius = max(np.linalg.norm(self.get_vertex_points() - self.__center_point, axis=1))
        return radius


    def integrate_over_unit_cell(self, values):
        assert isinstance(values, np.ndarray)
        assert len(values.shape) == 2 or len(values.shape) == 1, "The shape of 'values' should be (num_spin, n) or (n,)."
        if   len(values.shape) == 2: return self.__microvolume * np.sum(values, axis=-1).reshape(len(values), -1)
        elif len(values.shape) == 1: return self.__microvolume * np.sum(values, axis=-1)


    def compute_grid_points_on_axis(self):
        grid_a = np.arange(self.__gpts[0]) * self.__spacings[0]
        grid_b = np.arange(self.__gpts[1]) * self.__spacings[1]
        grid_c = np.arange(self.__gpts[2]) * self.__spacings[2]
        return np.array([grid_a, grid_b, grid_c], dtype=object)


    def make_grid_points(self, order='C'):
        points = np.indices(self.__gpts, dtype=np.float).reshape((3, -1), order=order).T
        if self.__is_cartesian:
            points *= self.__spacings
        else:
            points = points @ (self.__cell / self.__gpts)
        return points


    def get_points(self, order='C'):
        if order == 'C':
            return self.__points
        elif order == 'F':
            points = self.make_grid_points(order='F')
            return points


    @property
    def FD_order(self):
        return self.__FD_order


    @property
    def points(self):
        return self.__points


    @property
    def spacings(self):
        return self.__spacings


    @property
    def gpts(self):
        return self.__gpts


    @property
    def microvolume(self):
        return self.__microvolume


    @property
    def grid_points_on_axis(self):
        return self.compute_grid_points_on_axis()


    @property
    def center_point(self):
        return self.__center_point


    @property
    def total_gpts(self):
        return self.__total_gpts


    @property
    def is_cartesian(self):
        return self.__is_cartesian


    def get_cell(self):
        return self.__cell


    def get_pbc(self):
        return self.atoms.get_pbc()


    def set_pbc(self, pbc):
        assert len(pbc) == 3
        self.atoms.set_pbc(pbc)
        return


if __name__=="__main__":
    import sys
    np.set_printoptions(threshold=sys.maxsize)

    gpts = [4, 4, 4]
    cell = [10., 10., 10.]
    grid = Grid( gpts, cell )
    print(grid.center_point)
    print('grid.get_points = \n', grid.get_points())
