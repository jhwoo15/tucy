import numpy as np
from time import time
import scipy
import torch


def timer(func):
    from time import time
    def wrapper(*args, **kwargs):
        start  = time()
        result = func(*args, **kwargs)
        end    = time()
        print("Elapsed time[{}]: {} sec".format(func.__name__, (end - start)))
        return result
    return wrapper


class PyTucker:
    """Tucker decomposition method.

    :type  shape: np.ndarray or list
    :param shape:
        shape of grid, e.g., shape=[20, 20, 20]
    :type  ranks: np.ndarray of list
    :param ranks:
        decomposition ranks, e.g., ranks=[10, 10, 10]
    """
    def __init__(self, shape=None, ranks=None):
        assert isinstance(shape, np.ndarray) or isinstance(shape, list)
        assert isinstance(ranks, np.ndarray) or isinstance(ranks, list)
        assert len(shape) == 3 and len(ranks) == 3  # ! 6 -> 3
        self.shape = shape
        self.ranks = ranks

        self.Us = np.empty(3, dtype=object)
        self.starting_vector = None  # need for guess eigenvectors
        self.core_matrix = None
        return

    @timer
    def core_diagonalize(self, num_eig=None, tol=0, maxiter=20, method='lobpcg'):
        from scipy.sparse.linalg import lobpcg, eigsh
        from scipy.linalg import eigh

        dim = np.prod(self.ranks)

        ## Set starting_vector
        if self.starting_vector is None:
            self.starting_vector = np.random.random((dim, num_eig))

        st_diag = time()
        if method == 'lobpcg':
            val, vec = lobpcg(
                self.core_matrix,
                self.starting_vector,
                tol=tol,
                maxiter=maxiter,
                largest=False,
            )
        elif method == 'eigsh':
            val, vec = eigsh(
                self.core_matrix,
                num_eig,
                which='SA',
                tol=tol,
                v0=self.starting_vector[:,0],
            )
        elif method == 'eigh':
            val, vec = eigh(self.core_matrix)
        else:
            raise NotImplementedError
        ed_diag = time()
        print('pure lobpcg time : {} sec'.format(ed_diag-st_diag))

        ## sort
        indices = val.argsort()
        val, vec = val[indices][:num_eig], np.asarray(vec[:,indices])[:,:num_eig]
        self.starting_vector = vec

        st_proj = time()
        ## Project eig_vec to original space
        tmp = np.tensordot(self.Us[2], 
                           vec.T.reshape(np.append(num_eig, self.ranks[:3]), order='F'),
                           (1, 3))
        tmp = np.tensordot(self.Us[1],
                           tmp,
                           (1, 3))
        tmp = np.tensordot(self.Us[0],
                           tmp, 
                           (1, 3))
        tmp = np.moveaxis(tmp, -1, 0)
        eig_vec = tmp.reshape((num_eig, -1)).T
        ed_proj = time()

        print( 'project eig_vec Time : {} sec'.format(ed_proj-st_proj))
        return val, eig_vec

    def set_custom_Us(self, Ux, Uy, Uz):
        """set custom U matrices."""
        if isinstance(Ux, torch.Tensor):
            Ux = Ux.detach().cpu().numpy()
        if isinstance(Uy, torch.Tensor):
            Uy = Uy.detach().cpu().numpy()
        if isinstance(Uz, torch.Tensor):
            Uz = Uz.detach().cpu().numpy()
        # self.Us = np.empty(3, dtype=object)  # ! 6 -> 3
        self.Us[0], self.Us[1], self.Us[2] = Ux, Uy, Uz
        return

    @timer
    def decompose(self, hamiltonian):
        if(isinstance(hamiltonian, scipy.sparse.csr_matrix)):
            hamiltonian = hamiltonian.tocoo()
        assert isinstance(hamiltonian, scipy.sparse.coo_matrix), "hamiltonian's type can only be 'scipy.sparse.coo_matrix'."
        assert hamiltonian.dtype==np.float64 or hamiltonian.dtype==np.complex128,\
               "hamiltonian's dtype can only be 'np.float64' or 'np.complex128'."
        shape = np.append(self.shape, self.shape)

        row_index = hamiltonian.row
        col_index = hamiltonian.col
        val       = torch.from_numpy(hamiltonian.data)
        indices   = []
        indices.append(  row_index//( np.prod(shape[1:3]) ) )
        indices.append( (row_index%( np.prod(shape[1:3])  ) ) // shape[2] )
        indices.append(  row_index%shape[2]  )

        indices.append(  col_index//( np.prod(shape[1:3]) ) )
        indices.append( (col_index%( np.prod(shape[1:3])  ) ) // shape[2] )
        indices.append(  col_index%shape[2]  )

        Us = np.empty(3, dtype=object)
        for dim in range(3):
            tmp_indices = np.roll(indices, -dim, axis=0)
            tmp_shape   = np.roll(shape, -dim)
             
            row = tmp_indices[0]
            stride = np.concatenate( [np.ones(1, dtype=np.int64), np.cumprod( tmp_shape[1:][::-1] ) ]).reshape((-1,1))
            col = np.sum(tmp_indices[1:]*(stride[:-1][::-1]), 0)
            mat = torch.sparse_coo_tensor( np.array([col, row]), val, size=(stride[-1,0], tmp_shape[0] ) ) # transpose
            eigval, eigvec = torch.linalg.eigh( torch.sparse.mm(mat.H, mat).to_dense()  )
            self.Us[dim]=eigvec[:,-self.ranks[dim]: ].numpy()

        # set core_matrix
        R = np.prod(self.ranks)
        self.core_matrix = np.zeros((R, R), dtype=hamiltonian.dtype)
        return 

    def calculate_PW(self, kpt=0):
        Us = np.empty(3, dtype=object)
        for i in range(3):
            Us[i] = np.zeros((self.shape[i], self.ranks[i]) , dtype=complex)
            for k in range(-int(np.floor(self.ranks[i]/2)), int(np.ceil(self.ranks[i]/2))):
                k_= 2 * abs(k) + ( -1 if k < 0 else 0 )
                Us[i][:, k_] =  np.exp(-2 * np.pi * 1j * (k + kpt[i]) * np.arange(self.shape[i]) / self.shape[i])
                Us[i][:, k_] /= np.linalg.norm(Us[i][:, k_])
        return Us[0], Us[1], Us[2]

    @timer
    def project_kinetic_to_core(self, T_xx, T_yy, T_zz):
        Ux = self.Us[0].reshape((self.shape[0], self.ranks[0]))
        Uy = self.Us[1].reshape((self.shape[1], self.ranks[1]))
        Uz = self.Us[2].reshape((self.shape[2], self.ranks[2]))
        T_xx_sub = Ux.conj().T @ (T_xx @ Ux)
        T_yy_sub = Uy.conj().T @ (T_yy @ Uy)
        T_zz_sub = Uz.conj().T @ (T_zz @ Uz)
        I_xx_sub = np.eye(self.ranks[0])
        I_yy_sub = np.eye(self.ranks[1])
        I_zz_sub = np.eye(self.ranks[2])
        self.core_matrix += np.kron(I_zz_sub, np.kron(I_yy_sub, T_xx_sub))
        self.core_matrix += np.kron(I_zz_sub, np.kron(T_yy_sub, I_xx_sub))
        self.core_matrix += np.kron(T_zz_sub, np.kron(I_yy_sub, I_xx_sub))
        return

    @timer
    def project_local_potential_to_core(self, local_pot):
        local_pot = torch.from_numpy(local_pot)
        local_pot = local_pot.reshape(1,1,1,self.shape[0], self.shape[1], self.shape[2])

        for dim in range(3):
            _U = torch.from_numpy(self.Us[dim].T)
            shape = [1,1,1,1,1,1]
            shape[3+dim] = self.shape[dim]
            shape[2*dim] = -1
            local_pot = local_pot * _U.reshape( shape )
            local_pot = torch.tensordot(_U.conj(), local_pot, dims=([1],[3+dim] ))
        local_pot = torch.permute(local_pot,(0,1,2,5,4,3))
        Rxyz = np.prod(self.ranks)
        self.core_matrix += local_pot.reshape(Rxyz, Rxyz).numpy()
        return

    @timer
    def project_KB_projectors_to_core(self, D_matrix, kb_proj):
        # from scipy.sparse import csr_matrix
        assert isinstance(kb_proj, scipy.sparse.csr_matrix)

        ## project KB projectors
        P = kb_proj.toarray()
        tmp = np.tensordot(self.Us[2], P.reshape(np.append(len(P), self.shape[:3])), (0, 3))
        tmp = np.tensordot(self.Us[1], tmp, (0, 3))
        tmp = np.tensordot(self.Us[0], tmp, (0, 3))
        tmp = np.moveaxis(tmp, -1, 0)
        PU = tmp.reshape((len(P), -1), order='F')
        self.core_matrix += PU.conj().T @ (D_matrix @ PU)
        return

    def get_singular_values(self):
        pass

    def get_singular_vectors(self):
        pass
