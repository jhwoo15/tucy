import re 
import numpy as np
np.random.seed(0)
np.set_printoptions(edgeitems=1000000000000000000)
from time import time
import scipy.sparse as sparse
from scipy.sparse.linalg import eigsh
from tucy import PyTucker
from Util import make_kinetic_op, harmonic_potential
from Grid import Grid
from ase import Atoms


TEST_DIAG = False#True
TEST_HARMONIC = True#False#True
TEST_KB_PROJ = False#True
TEST_COMPLEX = True
test_new_methods = True
print(" PyTucker TEST ")


if TEST_DIAG:
    print("================== START : TEST_DIAG ==================")
    print("Random symmetric matrix diagonalization")
    shape = list(map(int, input("shape : ").split(' ')))
    ranks = list(map(int, input("ranks : ").split(' ')))
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])

    tucker1 = PyTucker(shape, ranks)
    print("tucker.shape = ", tucker1.shape)
    print("tucker.ranks = ", tucker1.ranks)

    M = np.random.rand(num_grid, num_grid) 
    M = (M + M.T)/2 # Symmetric Matrix
    sparse_matrix = sparse.csr_matrix( M )

    num_eig = 20

    ########################### 1.
    print("="*20+" Ref "+"="*20)
    eig_val_ref, eig_vec_ref = eigsh(sparse_matrix, num_eig, which='SA', tol=0, v0=None)
    print("eig_val_ref = ", eig_val_ref)
    print("eig_vec_ref = \n", eig_vec_ref)
    print("="*50)

    ########################### 2.
    print("="*20+" Tucker "+"="*20)
    tucker1.decompose(sparse_matrix, calculate_core=True)
    eig_val, eig_vec = tucker1.diagonalize(num_eig)
    print("tucker1 = ", tucker1)
    print("eig_val = ", eig_val)
    print("eig_vec = \n", eig_vec)
    print("="*50)
    print("==================  END  : TEST_DIAG ==================\n")


if TEST_HARMONIC:
    print("=============== START : TEST_HARMONIC =================")
    shape = list( map(int, re.split("\s+",input("shape : ").strip())) )
    ranks = list( map(int, re.split("\s+",input("ranks : ").strip())) )
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms('H', cell=cells)
    grid = Grid(atoms, shape[:3])

    hamiltonian = sparse.csr_matrix( (num_grid, num_grid), dtype=float ) 
    hamiltonian += make_kinetic_op(grid, combine=True)
    local_potential = harmonic_potential(grid, cells/2)
    hamiltonian += sparse.diags(local_potential) 

    num_eig = 4

    #1. scipy.sparse.linalg.eigsh 
    start = time()
    eig_val_ref, eig_vec_ref = eigsh( hamiltonian, num_eig, which='SA', tol=0, v0=None)
    end = time()
    print("[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end-start))
    print('eig_val_ref = ', eig_val_ref)
    #print('eig_vec_ref = \n', eig_vec_ref)

    #2. Tucker diagonalization
    start = time()
    tucker = PyTucker(shape, ranks)
    T_xx, T_yy, T_zz = make_kinetic_op(grid, combine=False)
    if test_new_methods:
        tucker.new_decompose(hamiltonian, calculate_core=False)
        tucker.new_project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.new_project_local_potential_to_core(local_potential)
    else:
        tucker.decompose(hamiltonian, calculate_core=False)
        tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.project_local_potential_to_core(local_potential)
    #eig_val, eig_vec = tucker.diagonalize(num_eig);    print('eig_val(C++) = ', eig_val)
    eig_val, eig_vec = tucker.core_diagonalize(num_eig, maxiter=1000, method='lobpcg')
    #eig_val, eig_vec = tucker.core_diagonalize(num_eig, maxiter=1000, method='eigsh')
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))

    print('eig_val = ', eig_val)
    #print('eig_vec = \n', eig_vec)
    print("================ END : TEST_HARMONIC ==================\n")


if TEST_KB_PROJ:
    print("=============== START : TEST_KB_PROJ  =================")
    shape = list( map(int, re.split("\s+",input("shape : ").strip())) )
    ranks = list( map(int, re.split("\s+",input("ranks : ").strip())) )
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms('H', cell=cells)
    grid = Grid(atoms, shape[:3])

    hamiltonian = sparse.csr_matrix( (num_grid, num_grid), dtype=float )
    hamiltonian += make_kinetic_op(grid, combine=True)
    local_potential = harmonic_potential(grid, cells/2)
    hamiltonian += sparse.diags(local_potential)
    LM = 2
    D_matrix = sparse.diags( np.random.rand(LM) )
    kb_proj  = sparse.csr_matrix( np.random.rand(LM, num_grid) )
    #kb_proj = sparse.csr_matrix( np.arange(0, num_grid*LM, 1).reshape(LM, num_grid) )
    hamiltonian += kb_proj.T @ D_matrix @ kb_proj

    num_eig = 4
    #1. scipy.sparse.linalg.eigsh
    start = time()
    eig_val_ref, eig_vec_ref = eigsh( hamiltonian, num_eig, which='SA', tol=0, v0=None)
    end = time()
    print("[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end-start))
    print('eig_val_ref = ', eig_val_ref)
    #print('eig_vec_ref = \n', eig_vec_ref)

    #2. Tucker diagonalization
    start = time()
    tucker = PyTucker(shape, ranks)
    #tucker.decompose(hamiltonian, calculate_core=True)
    tucker.decompose(hamiltonian, calculate_core=False)
    T_xx, T_yy, T_zz = make_kinetic_op(grid, combine=False)
    tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
    tucker.project_local_potential_to_core(local_potential)
    tucker.project_KB_projectors_to_core(D_matrix, kb_proj)
    eig_val, eig_vec = tucker.diagonalize(num_eig)
    end = time()

    #3. Print Singular values of Us
    singular_values = tucker.get_singular_values()
    singular_vectors = tucker.get_singular_vectors()
    print('singular_values = \n', singular_values)
    print('singular_vectors = \n', singular_vectors)

    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))

    print('eig_val = ', eig_val)
    #print('eig_vec = \n', eig_vec)

    print("================ END : TEST_KB_PROJ  ==================\n")


if TEST_COMPLEX:
    print("=============== START : TEST_COMPLEX  =================")
    shape = list( map(int, re.split("\s+",input("shape : ").strip())) )
    ranks = list( map(int, re.split("\s+",input("ranks : ").strip())) )
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms('H', cell=cells)
    grid = Grid(atoms, shape[:3])
    #pbc = [True, True, True]
    #grid.set_pbc(pbc)
    kpt = [1,1,1]

    hamiltonian = sparse.csr_matrix( (num_grid, num_grid), dtype=complex )
    hamiltonian += make_kinetic_op(grid, kpt=kpt, combine=True)

    local_potential = harmonic_potential(grid, cells/2)
    hamiltonian += sparse.diags(local_potential)

    LM = 2
    D_matrix = sparse.diags( np.random.rand(LM) )
    kb_proj  = sparse.csr_matrix( np.random.rand(LM, num_grid) + np.random.rand(LM, num_grid) * 1j )
    hamiltonian += kb_proj.conj().T @ D_matrix @ kb_proj

    num_eig = 4

    #1. scipy.sparse.linalg.eigsh
    start = time()
    eig_val_ref, eig_vec_ref = eigsh( hamiltonian, num_eig, which='SA', tol=0, v0=None)
    end = time()
    sort_idx = eig_val_ref.argsort()
    eig_val_ref, eig_vec_ref = eig_val_ref[sort_idx], eig_vec_ref[:, sort_idx]
    print("[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end-start))
    print('eig_val_ref = ', eig_val_ref)
    #print('eig_vec_ref = ', eig_vec_ref)
    density_ref =  np.sum( eig_vec_ref * eig_vec_ref.conj(), axis=1 )

    #2. Tucker diagonalization
    start = time()
    tucker = PyTucker(shape, ranks)

    T_xx, T_yy, T_zz = make_kinetic_op(grid, kpt=kpt, combine=False)
    if test_new_methods:
        tucker.new_decompose(hamiltonian, calculate_core=False)
        tucker.new_project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.new_project_local_potential_to_core(local_potential)
        tucker.project_KB_projectors_to_core(D_matrix, kb_proj)
    else:
        tucker.decompose(hamiltonian, calculate_core=False)
        tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.project_local_potential_to_core(local_potential)
        tucker.project_KB_projectors_to_core(D_matrix, kb_proj)

    #eig_val, eig_vec = tucker.diagonalize(num_eig)
    eig_val, eig_vec = tucker.core_diagonalize(num_eig, tol=0, maxiter=1000, method='lobpcg')
    print('eig_val = ', eig_val)
    #print('eig_vec = \n', eig_vec)
    #density = np.sum( eig_vec * eig_vec.conj(), axis=1 ).real
    #print('density = ', density)
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))
    print("================ END : TEST_COMPLEX  ==================\n")
