# cython: language_level=3
# distutils: language=c++

from libcpp.vector cimport vector
from array cimport array, six
from libcpp cimport bool


cdef extern from "Tucker.hpp" namespace "TensorDecompose":
    cppclass TuckerReal "TensorDecompose::Tucker<double, 6>":
        TuckerReal(array[size_t, six] shape, array[size_t, six] ranks) except+

        array[size_t, six] ranks

        void diagonalize(vector[double]& eigen_value, vector[double]& eigen_vector, size_t eig_num, bool use_fp32)
        void decompose(int* row, int* col, double* val, size_t row_size, size_t col_size, size_t val_size,
                       bool calculate_core, bool pass_complete)
        void add_to_core(double* vec)
        void set_Us(double* U1, double* U2, double* U3,
                    double* U4, double* U5, double* U6)
        void project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, vector[double] t_xx, vector[double] t_yy, vector[double] t_zz)
        void project_local_potential(double* local_potential, size_t project_axis_level)
        void project_KB_projector(vector[size_t] D_shape, vector[double] D_val, vector[size_t] KB_shape,
                                  vector[size_t] KB_row, vector[size_t] KB_col, vector[double] KB_val)
        vector[double] get_singular_values(size_t dim)
        vector[double] get_singular_vector(size_t dim)
        vector[double] get_U(size_t axis)
        void get_core(double* core)


cdef extern from "Tucker.hpp" namespace "TensorDecompose":
    cppclass TuckerComplex "TensorDecompose::Tucker<std::complex<double>, 6>":
        TuckerComplex(array[size_t, six] shape, array[size_t, six] ranks) except+

        array[size_t, six] ranks

        void diagonalize(vector[double]& eigen_value, vector[double complex]& eigen_vector, size_t eig_num, bool use_fp32)
        void decompose(int* row, int* col, double complex* val, size_t row_size, size_t col_size, size_t val_size,
                       bool calculate_core, bool pass_complete)
        void add_to_core(double complex* inp_vec)
        void set_Us(double complex* U1, double complex* U2, double complex* U3,
                    double complex* U4, double complex* U5, double complex* U6)
        void project_kinetic_matrix(size_t dim_x, size_t dim_y, size_t dim_z, vector[double complex] t_xx, vector[double complex] t_yy, vector[double complex] t_zz)
        void project_local_potential(double* local_potential, size_t project_axis_level)
        void project_KB_projector(vector[size_t] D_shape, vector[double complex] D_val, vector[size_t] KB_shape,
                                  vector[size_t] KB_row, vector[size_t] KB_col, vector[double complex] KB_val)
        vector[double] get_singular_values(size_t dim)
        vector[double complex] get_singular_vector(size_t dim)
        vector[double complex] get_U(size_t axis)
        void get_core(double complex* core)
