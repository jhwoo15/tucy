#fd_operator
import numpy as np
from scipy import sparse


laplace = [[0],
           [-2, 1],
           [-5 / 2, 4 / 3, -1 / 12],
           [-49 / 18, 3 / 2, -3 / 20, 1 / 90],
           [-205 / 72, 8 / 5, -1 / 5, 8 / 315, -1 / 560],
           [-5269 / 1800, 5 / 3, -5 / 21, 5 / 126, -5 / 1008, 1 / 3150],
           [-5369 / 1800, 12 / 7, -15 / 56, 10 / 189, -1 / 112, 2 / 1925,
            -1 / 16632]]

derivatives = [[1 / 2],
               [2 / 3, -1 / 12],
               [3 / 4, -3 / 20, 1 / 60],
               [4 / 5, -1 / 5, 4 / 105, -1 / 280]]


def _make_1st_derivative_op(grid, axis):
    FD_order = grid.FD_order
    assert 0 < FD_order and FD_order <= 3
    pbc = grid.get_pbc()[axis]
    gpt = grid.gpts[axis]

    ret_matrix = np.zeros((gpt, gpt), dtype=float)
    for fd in range(1, FD_order+1):
        ret_matrix += np.diag(derivatives[FD_order-1][fd-1] * np.ones(gpt-fd), k=fd)
        if pbc:
            ret_matrix -= np.diag(derivatives[FD_order-1][fd-1] * np.ones(fd), k=gpt-fd)
    ret_matrix -= ret_matrix.T
    return ret_matrix


def _make_2nd_derivative_op(grid, axis):
    FD_order = grid.FD_order
    pbc = grid.get_pbc()[axis]
    gpt = grid.gpts[axis]
    assert 0 < FD_order and FD_order <= 6
    assert gpt >= (2 * FD_order + 1)

    ret_matrix = np.zeros((gpt, gpt), dtype=float)
    for fd in range(1, FD_order+1):
        ret_matrix += np.diag(laplace[FD_order][fd] * np.ones(gpt-fd), k=fd)
        if pbc:
            ret_matrix += np.diag(laplace[FD_order][fd] * np.ones(fd), k=gpt-fd)
    ret_matrix += ret_matrix.T
    ret_matrix += np.diag(laplace[FD_order][0] * np.ones(gpt))
    return ret_matrix


def make_kinetic_op(grid, kpt=[0,0,0], combine=True):
    """
    Eq)
    \vec{T}_{\vec{k}} = ( \vec{\nabla} + i \vec{k} )^2
                      = \nabla^2 + 2i \vec{nabla} \cdot \vec{k} - k^2
    """
    is_gamma = (kpt[0] == 0 and kpt[1] == 0 and kpt[2] == 0)
    gpts = grid.gpts
    spacings = grid.spacings

    dx2 = _make_2nd_derivative_op(grid, 0) / spacings[0]**2
    dy2 = _make_2nd_derivative_op(grid, 1) / spacings[1]**2
    dz2 = _make_2nd_derivative_op(grid, 2) / spacings[2]**2
    I_xx = sparse.identity(gpts[0])
    I_yy = sparse.identity(gpts[1])
    I_zz = sparse.identity(gpts[2])
    T_xx = dx2
    T_yy = dy2
    T_zz = dz2
    if not is_gamma:
        dx  = _make_1st_derivative_op(grid, 0) / spacings[0]
        dy  = _make_1st_derivative_op(grid, 1) / spacings[1]
        dz  = _make_1st_derivative_op(grid, 2) / spacings[2]
        T_xx = T_xx.astype(complex)
        T_yy = T_yy.astype(complex)
        T_zz = T_zz.astype(complex)
        T_xx += 2 * 1j * kpt[0] * dx - kpt[0]**2 * I_xx
        T_yy += 2 * 1j * kpt[1] * dy - kpt[1]**2 * I_yy
        T_zz += 2 * 1j * kpt[2] * dz - kpt[2]**2 * I_zz
    T_xx *= -0.5
    T_yy *= -0.5
    T_zz *= -0.5

    if combine:
        T_xx = sparse.csr_matrix(T_xx)
        T_yy = sparse.csr_matrix(T_yy)
        T_zz = sparse.csr_matrix(T_zz)
        T_xyzxyz = sparse.kron(T_xx, sparse.kron(I_yy, I_zz)) + sparse.kron(I_xx, sparse.kron(T_yy, I_zz)) \
                   + sparse.kron(sparse.kron(I_xx, I_yy), T_zz)
        return T_xyzxyz
    else:
        return T_xx, T_yy, T_zz


def divergence(grid, values, FD_order=2):
    """
    grid   : Grid object
    values : numpy ndarray of (3, N) ; N is the number of grid points.

    div F = \nabla \cdot F
          = (\partial_x, \partial_y, \partial_z) \cdot (F_x, F_y, F_z)
          = \partial_x \cdot F_x + \partial_y \cdot F_y + \partial_z \cdot F_z
          =         A            +         B            +         C
    """
    assert values.shape == (3, grid.get_total_size())
    spacings = grid.get_spacing()
    dims = grid.get_dims()
    d_xx = _make_1st_derivative_op(grid, 0, FD_order) / spacings[0]
    d_yy = _make_1st_derivative_op(grid, 1, FD_order) / spacings[1]
    d_zz = _make_1st_derivative_op(grid, 2, FD_order) / spacings[2]
    A = np.tensordot(d_xx.toarray(), values[0].reshape(dims), (1,0)).flatten()
    B = np.tensordot(values[1].reshape(dims), d_yy.toarray(), (1,1))
    B = np.moveaxis(B, 1, 2).flatten()
    C = np.tensordot(values[2].reshape(dims), d_zz.toarray(), (2,1)).flatten()
    return A + B + C


def harmonic_potential(grid, center_position):
    points = grid.get_points()
    r = np.linalg.norm(points - center_position, axis=1) 
    potential = -300 - 1 / np.sqrt(0.1 + r**2)
    return potential
