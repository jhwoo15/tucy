# cython: language_level=3
# distutils: language = c++

cimport numpy as cnp
import numpy as np
from time import time
from array cimport array, six
from libcpp.vector cimport vector
from Tucker cimport TuckerReal as c_TuckerReal
from Tucker cimport TuckerComplex as c_TuckerComplex
import scipy
import torch


def timer(func):
    from time import time
    def wrapper(*args, **kwargs):
        start  = time()
        result = func(*args, **kwargs)
        end    = time()
        print("Elapsed time[{}]: {} sec".format(func.__name__, (end - start)))
        return result
    return wrapper


cdef class PyTucker:
    cdef:
        c_TuckerReal* c_tucker_real
        c_TuckerComplex* c_tucker_complex
        public cnp.ndarray shape
        public cnp.ndarray ranks
        public str dtype
        public cnp.ndarray starting_vector
        public cnp.ndarray Us


    def __cinit__(self, shape=None, ranks=None):
        assert isinstance(shape, np.ndarray) or isinstance(shape, list)
        assert isinstance(ranks, np.ndarray) or isinstance(ranks, list)
        assert len(shape) == 6 and len(ranks) == 6
        for i in range(3):
            assert shape[i] == shape[i+3], "Tensor shape is wrong."
            assert ranks[i] == ranks[i+3]
        self.shape = np.array(shape)
        self.ranks = np.array(ranks)
        self.dtype = None
        cdef:
            array[size_t, six] c_shape
            array[size_t, six] c_ranks
            size_t dimension = len(shape)
        for i in range(dimension):
            c_shape[i] = shape[i]
            c_ranks[i] = ranks[i]
        self.c_tucker_real = new c_TuckerReal(c_shape, c_ranks)
        self.c_tucker_complex = new c_TuckerComplex(c_shape, c_ranks)

        self.starting_vector = None
        return


    def __dealloc__(self):
        del self.c_tucker_real
        del self.c_tucker_complex


    def diagonalize(self, num_eig=None, use_fp32=False):
        assert self.dtype, "'dtype' is not initialized yet. Please call 'decompose()' first."
        max_num_eig = self.ranks[0] * self.ranks[1] * self.ranks[2]
        if num_eig == None:
            num_eig = max_num_eig
        assert num_eig <= max_num_eig, "'num_eig' is over the maximum value."

        cdef:
            vector[double] eig_vec_real = []
            vector[double] eig_val = []
            vector[double complex] eig_vec_complex = []
        if self.dtype == "float64":
            self.c_tucker_real.diagonalize(eig_val, eig_vec_real, num_eig, use_fp32)
            return np.asarray(eig_val), np.asarray(eig_vec_real).T.reshape(np.append(self.shape[:3], num_eig), order='F').reshape(-1, num_eig)
        elif self.dtype == "complex128":
            self.c_tucker_complex.diagonalize(eig_val, eig_vec_complex, num_eig, use_fp32)
            #return np.asarray(eig_val), np.asarray(eig_vec_complex).T.reshape(np.append(self.shape[:3], num_eig), order='F').reshape(-1, num_eig)
            return np.asarray(eig_val), np.asarray(eig_vec_complex).conj().T.reshape(np.append(self.shape[:3], num_eig), order='F').reshape(-1, num_eig)
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)


    cpdef get_core_real(self):
        cdef cnp.ndarray[double, ndim=1] core = np.zeros(np.prod(self.ranks))
        self.c_tucker_real.get_core(<double*> core.data)
        return core


    cpdef get_core_complex(self):
        cdef cnp.ndarray[double complex, ndim=1] core = np.zeros(np.prod(self.ranks), dtype=complex)
        self.c_tucker_complex.get_core(<double complex*> core.data)
        return core


    @timer
    def get_core_matrix(self):
        if self.dtype == "float64":
            core = self.get_core_real()
        elif self.dtype == "complex128":
            core = self.get_core_complex()
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        dim = np.prod(self.ranks[:3])
        return core.reshape(dim, dim)



    @timer
    def core_diagonalize(self, num_eig=None, tol=0, maxiter=20, method='lobpcg'):
        from scipy.sparse.linalg import lobpcg, eigsh
        from scipy.linalg import eigh

        dim = np.prod(self.ranks[:3])
        core_matrix = self.get_core_matrix()

        ## Set starting_vector
        if self.starting_vector is None:
            self.starting_vector = np.random.random((dim, num_eig))

        st_diag = time()
        if method == 'lobpcg':
            val, vec = lobpcg(core_matrix, self.starting_vector, tol=tol, maxiter=maxiter, largest=False)
        elif method == 'eigsh':
            val, vec = eigsh(core_matrix, num_eig, which='SA', tol=tol, v0=self.starting_vector[:,0])
        elif method == 'eigh':
            val, vec = eigh(core_matrix)
        else:
            raise NotImplementedError
        ed_diag = time()
        print('pure lobpcg time : {} sec'.format(ed_diag-st_diag))

        ## sort
        indices = val.argsort()
        val, vec = val[indices][:num_eig], np.asarray(vec[:,indices])[:,:num_eig]
        self.starting_vector = vec

        st_proj = time()
        ## Project eig_vec to original space
        tmp = np.tensordot(self.Us[2], 
                           vec.T.reshape(np.append(num_eig, self.ranks[:3]), order='F'),
                           (1, 3))
        tmp = np.tensordot(self.Us[1],
                           tmp,
                           (1, 3))
        tmp = np.tensordot(self.Us[0],
                           tmp, 
                           (1, 3))
        tmp = np.moveaxis(tmp, -1, 0)
        eig_vec = tmp.reshape((num_eig, -1)).T
        ed_proj = time()

        print( 'project eig_vec Time : {} sec'.format(ed_proj-st_proj))
        return val, eig_vec


    def decompose_real(self, cnp.ndarray[int, ndim=1] row, cnp.ndarray[int, ndim=1] col,
                       cnp.ndarray[double, ndim=1] val, calculate_core, pass_complete):
        self.c_tucker_real.decompose(<int*> row.data, <int*> col.data, <double*> val.data,
                                     len(row), len(col), len(val), calculate_core, pass_complete)
        return


    def decompose_complex(self, cnp.ndarray[int, ndim=1] row, cnp.ndarray[int, ndim=1] col,
                          cnp.ndarray[double complex, ndim=1] val, calculate_core, pass_complete):
        self.c_tucker_complex.decompose(<int*> row.data, <int*> col.data, <double complex*> val.data,
                                        len(row), len(col), len(val), calculate_core, pass_complete)
        return


    def calculate_PW(self, kpt=0):
        Us = np.empty(3, dtype=object)
        for i in range(3):
            Us[i] = np.zeros((self.shape[i], self.ranks[i]) , dtype=complex)
            for k in range(-int(np.floor(self.ranks[i]/2)), int(np.ceil(self.ranks[i]/2))):
                k_= 2 * abs(k) + ( -1 if k < 0 else 0 )
                #Us[i][:, k_] =  np.exp(-2 * np.pi * 1j * k * np.arange(self.shape[i]) / self.shape[i])
                Us[i][:, k_] =  np.exp(-2 * np.pi * 1j * (k + kpt[i]) * np.arange(self.shape[i]) / self.shape[i])
                Us[i][:, k_] /= np.linalg.norm(Us[i][:, k_])
            #print(f"orthonormality {i}th U = \n{np.abs(Us[i].conj().T @ Us[i])}")
        return Us[0], Us[1], Us[2]


#    def set_U(self, Us):
#        assert len(Us)==6, "length of Us should be 6"
#        if self.dtype == "float64":
#            self.set_U_real(U1, U2, U3, U4, U5, U6)
#        elif self.dtype == "complex128":
#            self.set_U_complex(U1, U2, U3, U4, U5, U6)
#        else:
#            assert False, f"{self.dtype} is not supported data type."
#        return


    def set_U_real(self, cnp.ndarray[double, ndim=1] U1, cnp.ndarray[double, ndim=1] U2,
                         cnp.ndarray[double, ndim=1] U3, cnp.ndarray[double, ndim=1] U4,
                         cnp.ndarray[double, ndim=1] U5, cnp.ndarray[double, ndim=1] U6):
        self.c_tucker_real.set_Us(<double*> U1.data, <double*> U2.data, <double*> U3.data,
                                  <double*> U4.data, <double*> U5.data, <double*> U6.data)
        return


    def set_U_complex(self, cnp.ndarray[double complex, ndim=1] U1, cnp.ndarray[double complex, ndim=1] U2,
                            cnp.ndarray[double complex, ndim=1] U3, cnp.ndarray[double complex, ndim=1] U4,
                            cnp.ndarray[double complex, ndim=1] U5, cnp.ndarray[double complex, ndim=1] U6):
        self.c_tucker_complex.set_Us(<double complex*> U1.data, <double complex*> U2.data, <double complex*> U3.data,
                                     <double complex*> U4.data, <double complex*> U5.data, <double complex*> U6.data)
        return


    def set_custom_Us(self, Ux, Uy, Uz):
        if isinstance(Ux, torch.Tensor):
            Ux = Ux.detach().cpu().numpy()
        if isinstance(Uy, torch.Tensor):
            Uy = Uy.detach().cpu().numpy()
        if isinstance(Uz, torch.Tensor):
            Uz = Uz.detach().cpu().numpy()
        assert (Ux.shape[0], Uy.shape[0], Uz.shape[0]) == tuple(self.shape[:3])
        assert (Ux.shape[1], Uy.shape[1], Uz.shape[1]) == tuple(self.ranks[:3])
        assert Ux.dtype == Uy.dtype == Uz.dtype
        self.dtype = str(Ux.dtype)
        self.Us = np.empty(6, dtype=object)
        self.Us[0], self.Us[1], self.Us[2] = Ux, Uy, Uz
        self.Us[3], self.Us[4], self.Us[5] = Ux.conj(), Uy.conj(), Uz.conj()

        if self.dtype == 'float64':
            self.set_U_real(self.Us[0].T.reshape(-1), self.Us[1].T.reshape(-1), self.Us[2].T.reshape(-1),
                            self.Us[3].T.reshape(-1), self.Us[4].T.reshape(-1), self.Us[5].T.reshape(-1))
        elif self.dtype == 'complex128':
            self.set_U_complex(self.Us[0].T.reshape(-1), self.Us[1].T.reshape(-1), self.Us[2].T.reshape(-1),
                               self.Us[3].T.reshape(-1), self.Us[4].T.reshape(-1), self.Us[5].T.reshape(-1))
        else:
            assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        # self.set_U(self.Us[0].T.reshape(-1), self.Us[1].T.reshape(-1), ...)
        return


    @timer
    def decompose(self, hamiltonian, calculate_core=False, pass_complete=False):
        """
        Input) hamiltonian (scipy sparse matrix

        * row (ndarray) : row indices of COO matrix
        * col (ndarray) : col indices of COO matrix
        * val (ndarray) : values of COO matrix correspoding to row and col indices.
        """
        assert isinstance(hamiltonian, scipy.sparse.csr_matrix), "hamiltonian's type can only be 'scipy.sparse.csr_matrix'."
        assert hamiltonian.dtype==np.float64 or hamiltonian.dtype==np.complex128,\
               "hamiltonian's dtype can only be 'np.float64' or 'np.complex128'."
        self.dtype = str(hamiltonian.dtype)
        row = hamiltonian.indptr
        col = hamiltonian.indices
        val = hamiltonian.data
        print( "Tucker Decomposition : ", self.shape[:3], " -> ", self.ranks[:3])
        assert hamiltonian.shape[0] == hamiltonian.shape[1], "Hamiltonian matrix should be square matrix."
        assert np.prod(hamiltonian.shape) == np.prod(self.shape), "Wrong shape error"
        if self.dtype == "float64":
            self.decompose_real(row, col, val, calculate_core, pass_complete)
        elif self.dtype == "complex128":
            self.decompose_complex(row, col, val, calculate_core, pass_complete)
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)

        ## get U matrices
        self.Us = np.empty(6, dtype=object)
        for i in range(6):
            if self.dtype == "float64":
                self.Us[i] = np.asarray(self.c_tucker_real.get_U(i)).reshape(self.shape[i], self.ranks[i], order='F')
            else: #self.dtype == "complex128"
                self.Us[i] = np.asarray(self.c_tucker_complex.get_U(i)).reshape((self.shape[i], self.ranks[i]), order='F')
        return

    @timer
    def new_decompose(self, hamiltonian, calculate_core=False, pass_complete=False):
        if(isinstance(hamiltonian, scipy.sparse.csr_matrix)):
            hamiltonian = hamiltonian.tocoo()
        assert isinstance(hamiltonian, scipy.sparse.coo_matrix), "hamiltonian's type can only be 'scipy.sparse.coo_matrix'."
        assert hamiltonian.dtype==np.float64 or hamiltonian.dtype==np.complex128,\
               "hamiltonian's dtype can only be 'np.float64' or 'np.complex128'."
        row_index = hamiltonian.row
        col_index = hamiltonian.col
        val       = torch.from_numpy(hamiltonian.data)
        indices   = []
        indices.append(  row_index//( np.prod(self.shape[1:3]) ) )
        indices.append( (row_index%( np.prod(self.shape[1:3])  ) ) // self.shape[2] )
        indices.append(  row_index%self.shape[2]  )

        indices.append(  col_index//( np.prod(self.shape[1:3]) ) )
        indices.append( (col_index%( np.prod(self.shape[1:3])  ) ) // self.shape[2] )
        indices.append(  col_index%self.shape[2]  )
        self.Us = np.empty(6, dtype=object)
        for dim in range(3):
            tmp_indices = np.roll(indices, -dim, axis=0)
            tmp_shape   = np.roll(self.shape, -dim)
             
            row = tmp_indices[0]
            stride = np.concatenate( [np.ones(1, dtype=np.int64), np.cumprod( tmp_shape[1:][::-1] ) ]).reshape((-1,1))
            col = np.sum(tmp_indices[1:]*(stride[:-1][::-1]), 0)
            mat = torch.sparse_coo_tensor( np.array([col, row]), val, size=(stride[-1,0], tmp_shape[0] ) ) # transpose 
            eigval, eigvec = torch.linalg.eigh( torch.sparse.mm(mat.H,mat).to_dense()  )
            self.Us[dim]=eigvec[:,-self.ranks[dim]: ]
        self.set_custom_Us(self.Us[0], self.Us[1], self.Us[2] )
        return 

    @timer
    def new_project_kinetic_to_core(self, T_xx, T_yy, T_zz):
        Ux = self.Us[0].reshape((self.shape[0], self.ranks[0]))
        Uy = self.Us[1].reshape((self.shape[1], self.ranks[1]))
        Uz = self.Us[2].reshape((self.shape[2], self.ranks[2]))
        T_xx_sub = Ux.conj().T @ (T_xx @ Ux)
        T_yy_sub = Uy.conj().T @ (T_yy @ Uy)
        T_zz_sub = Uz.conj().T @ (T_zz @ Uz)

        I_xx_sub = np.eye(self.ranks[0])
        I_yy_sub = np.eye(self.ranks[1])
        I_zz_sub = np.eye(self.ranks[2])

        Rxyz = np.prod(self.ranks[:3])
        core = np.zeros((Rxyz, Rxyz), dtype=T_xx_sub.dtype)
        core += np.kron(I_zz_sub, np.kron(I_yy_sub, T_xx_sub))
        core += np.kron(I_zz_sub, np.kron(T_yy_sub, I_xx_sub))
        core += np.kron(T_zz_sub, np.kron(I_yy_sub, I_xx_sub))

        self.add_to_core(core.reshape(-1))
        return

    @timer
    def project_kinetic_to_core(self, T_xx, T_yy, T_zz):
        assert self.dtype, "'dtype' is not initialized yet. Please call 'decompose()' first."
        if self.dtype == "float64":
            self.c_tucker_real.project_kinetic_matrix(len(T_xx), len(T_yy), len(T_zz), T_xx.reshape(-1), T_yy.reshape(-1), T_zz.reshape(-1)) 
        elif self.dtype == "complex128":
            self.c_tucker_complex.project_kinetic_matrix(len(T_xx), len(T_yy), len(T_zz), T_xx.reshape(-1), T_yy.reshape(-1), T_zz.reshape(-1)) 
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        return

    @timer
    def new_project_local_potential_to_core(self, local_pot):
        local_pot = torch.from_numpy(local_pot)
        local_pot = local_pot.reshape(1,1,1,self.shape[0], self.shape[1], self.shape[2])#.type(torch.complex128)

        for dim in range(3):
            _U = torch.from_numpy(self.Us[dim].T)
            shape = [1,1,1,1,1,1]
            shape[3+dim] = self.shape[dim]
            shape[2*dim] = -1
            local_pot = local_pot * _U.reshape( shape )
            local_pot = torch.tensordot(_U.conj(), local_pot, dims=([1],[3+dim] ))
        local_pot = torch.permute(local_pot,(0,1,2,5,4,3))
        local_pot = local_pot.reshape(-1).numpy()
        self.add_to_core(local_pot)
        return

    @timer
    def project_local_potential_to_core(self, local_pot):
        assert self.dtype, "'dtype' is not initialized yet. Please call 'decompose()' first."
        dim = np.prod(self.ranks[:3])

        # change tensor elements order 'C' to 'F'
        cdef cnp.ndarray[double, ndim=1] reshaped_local_pot = np.transpose( local_pot.reshape(self.shape[:3]), (2,1,0) ).reshape(-1)

        if self.dtype == "float64":
            self.c_tucker_real.project_local_potential(<double*> reshaped_local_pot.data, 3)
        elif self.dtype == "complex128":
            self.c_tucker_complex.project_local_potential(<double*> reshaped_local_pot.data, 3)
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        return


    def add_to_core(self, vec):
        if self.dtype == "float64":
            self.add_to_core_real(vec)
        elif self.dtype == "complex128":
            self.add_to_core_complex(vec)
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        return


    def add_to_core_real(self, cnp.ndarray[double, ndim=1] vec):
        self.c_tucker_real.add_to_core(<double*> vec.data)
        return


    def add_to_core_complex(self, cnp.ndarray[double complex, ndim=1] vec):
        self.c_tucker_complex.add_to_core(<double complex*> vec.data)
        return


    @timer
    def project_KB_projectors_to_core(self, D_matrix, kb_proj):
        from scipy.sparse import csr_matrix
        assert self.dtype, "'dtype' is not initialized yet. Please call 'decompose()' first."
        assert isinstance(kb_proj, csr_matrix)

        ## project KB projectors
        P = kb_proj.toarray()
        tmp = np.tensordot(self.Us[2], P.reshape(np.append(len(P), self.shape[:3])), (0, 3))
        tmp = np.tensordot(self.Us[1], tmp, (0, 3))
        tmp = np.tensordot(self.Us[0], tmp, (0, 3))
        tmp = np.moveaxis(tmp, -1, 0)
        PU = tmp.reshape((len(P), -1), order='F')
        core = (PU.conj().T @ D_matrix @ PU).reshape(-1)

        self.add_to_core(core)
        return


    def get_singular_values(self):
        if self.dtype == "float64":
            singular_values_Ux = self.c_tucker_real.get_singular_values(0)
            singular_values_Uy = self.c_tucker_real.get_singular_values(1)
            singular_values_Uz = self.c_tucker_real.get_singular_values(2)
        elif self.dtype == "complex128":
            singular_values_Ux = self.c_tucker_complex.get_singular_values(0)
            singular_values_Uy = self.c_tucker_complex.get_singular_values(1)
            singular_values_Uz = self.c_tucker_complex.get_singular_values(2)
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        return singular_values_Ux, singular_values_Uy,  singular_values_Uz


    def get_singular_vectors(self):
        if self.dtype == "float64":
            Ux = np.asarray(self.c_tucker_real.get_singular_vector(0)).reshape((self.shape[0], self.shape[0]), order='F')
            Uy = np.asarray(self.c_tucker_real.get_singular_vector(1)).reshape((self.shape[1], self.shape[1]), order='F')
            Uz = np.asarray(self.c_tucker_real.get_singular_vector(2)).reshape((self.shape[2], self.shape[2]), order='F')
        elif self.dtype == "complex128":
            Ux = np.asarray(self.c_tucker_complex.get_singular_vector(0)).reshape((self.shape[0], self.shape[0]), order='F')
            Uy = np.asarray(self.c_tucker_complex.get_singular_vector(1)).reshape((self.shape[1], self.shape[1]), order='F')
            Uz = np.asarray(self.c_tucker_complex.get_singular_vector(2)).reshape((self.shape[2], self.shape[2]), order='F')
        else: assert 0, "{} type is not supported. Only 'float64' or 'compex128' is supported.".format(self.dtype)
        return Ux, Uy, Uz
