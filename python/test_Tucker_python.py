"""
Test tucy_python.
Compare eigenvalues of tucy's and tucy_python's.
"""
import re 
import numpy as np
np.random.seed(0)
np.set_printoptions(edgeitems=1000000000000000000)
from time import time
import scipy.sparse as sparse
from scipy.sparse.linalg import eigsh
from tucy import PyTucker
from Util import make_kinetic_op, harmonic_potential
from Grid import Grid
from ase import Atoms
from tucy_python import PyTucker as PyTucker2


TEST_HARMONIC = True#False#True
TEST_COMPLEX = True
test_new_methods = True
print(" PyTucker TEST ")


if TEST_HARMONIC:
    print("=============== START : TEST_HARMONIC =================")
    shape = list( map(int, re.split("\s+",input("shape : ").strip())) )
    ranks = list( map(int, re.split("\s+",input("ranks : ").strip())) )
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms('H', cell=cells)
    grid = Grid(atoms, shape[:3])

    hamiltonian = sparse.csr_matrix( (num_grid, num_grid), dtype=float ) 
    hamiltonian += make_kinetic_op(grid, combine=True)
    local_potential = harmonic_potential(grid, cells/2)
    hamiltonian += sparse.diags(local_potential) 

    num_eig = 4

    #1. scipy.sparse.linalg.eigsh 
    # start = time()
    # eig_val_ref, eig_vec_ref = eigsh( hamiltonian, num_eig, which='SA', tol=0, v0=None)
    # end = time()
    # print("[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end-start))
    # print('eig_val_ref = ', eig_val_ref)
    # #print('eig_vec_ref = \n', eig_vec_ref)

    #2. Tucker diagonalization
    start = time()
    tucker = PyTucker(shape, ranks)
    T_xx, T_yy, T_zz = make_kinetic_op(grid, combine=False)
    if test_new_methods:
        tucker.new_decompose(hamiltonian, calculate_core=False)
        tucker.new_project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.new_project_local_potential_to_core(local_potential)
    else:
        tucker.decompose(hamiltonian, calculate_core=False)
        tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.project_local_potential_to_core(local_potential)
    eig_val, eig_vec = tucker.core_diagonalize(num_eig, maxiter=1000, method='lobpcg')
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))
    print('eig_val = ', eig_val)

    #3. Tucker_python
    start = time()
    tucker2 = PyTucker2(shape[:3], ranks[:3])
    tucker2.decompose(hamiltonian)
    tucker2.project_kinetic_to_core(T_xx, T_yy, T_zz)
    tucker2.project_local_potential_to_core(local_potential)
    eig_val2, eig_vec2 = tucker2.core_diagonalize(num_eig, maxiter=1000, method="lobpcg")
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))
    print(f"eig_val = {eig_val2}")
    print(f"eigval err = {(eig_val - eig_val2).sum()}")
    print("================ END : TEST_HARMONIC ==================\n")


if TEST_COMPLEX:
    print("=============== START : TEST_COMPLEX  =================")
    shape = list( map(int, re.split("\s+",input("shape : ").strip())) )
    ranks = list( map(int, re.split("\s+",input("ranks : ").strip())) )
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms('H', cell=cells)
    grid = Grid(atoms, shape[:3])
    #pbc = [True, True, True]
    #grid.set_pbc(pbc)
    kpt = [1,1,1]

    hamiltonian = sparse.csr_matrix( (num_grid, num_grid), dtype=complex )
    hamiltonian += make_kinetic_op(grid, kpt=kpt, combine=True)

    local_potential = harmonic_potential(grid, cells/2)
    hamiltonian += sparse.diags(local_potential)

    LM = 2
    D_matrix = sparse.diags( np.random.rand(LM) )
    kb_proj  = sparse.csr_matrix( np.random.rand(LM, num_grid) + np.random.rand(LM, num_grid) * 1j )
    hamiltonian += kb_proj.conj().T @ D_matrix @ kb_proj

    num_eig = 4

    #1. scipy.sparse.linalg.eigsh
    # start = time()
    # eig_val_ref, eig_vec_ref = eigsh( hamiltonian, num_eig, which='SA', tol=0, v0=None)
    # end = time()
    # sort_idx = eig_val_ref.argsort()
    # eig_val_ref, eig_vec_ref = eig_val_ref[sort_idx], eig_vec_ref[:, sort_idx]
    # print("[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end-start))
    # print('eig_val_ref = ', eig_val_ref)
    # density_ref =  np.sum( eig_vec_ref * eig_vec_ref.conj(), axis=1 )

    #2. Tucker diagonalization
    start = time()
    tucker = PyTucker(shape, ranks)

    T_xx, T_yy, T_zz = make_kinetic_op(grid, kpt=kpt, combine=False)
    if test_new_methods:
        tucker.new_decompose(hamiltonian, calculate_core=False)
        tucker.new_project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.new_project_local_potential_to_core(local_potential)
        tucker.project_KB_projectors_to_core(D_matrix, kb_proj)
    else:
        tucker.decompose(hamiltonian, calculate_core=False)
        tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
        tucker.project_local_potential_to_core(local_potential)
        tucker.project_KB_projectors_to_core(D_matrix, kb_proj)

    eig_val, eig_vec = tucker.core_diagonalize(num_eig, tol=0, maxiter=1000, method='lobpcg')
    print('eig_val = ', eig_val)
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))

    #3. Tucker_python
    start = time()
    tucker2 = PyTucker2(shape[:3], ranks[:3])
    tucker2.decompose(hamiltonian)
    tucker2.project_kinetic_to_core(T_xx, T_yy, T_zz)
    tucker2.project_local_potential_to_core(local_potential)
    tucker2.project_KB_projectors_to_core(D_matrix, kb_proj)
    eig_val2, eig_vec2 = tucker2.core_diagonalize(num_eig, maxiter=1000, method="lobpcg")
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end-start))
    print(f"eig_val = {eig_val2}")
    print(f"eigval err = {abs(eig_val - eig_val2).sum()}")
    print("================ END : TEST_COMPLEX  ==================\n")
