# cython: language_level=3
# distutils: language = c++
cdef extern from *:
#    ctypedef void* int_parameter
#    int_parameter one   "1"
#    int_parameter two   "2"
#    int_parameter three "3"
#    int_parameter four  "4"
#    int_parameter five  "5"
#    int_parameter six   "6"
    ctypedef size_t one   "1" 
    ctypedef size_t two   "2" 
    ctypedef size_t three "3" 
    ctypedef size_t four  "4" 
    ctypedef size_t five  "5" 
    ctypedef size_t six   "6" 

cdef extern from "<array>" namespace "std" nogil:
    cdef cppclass array[datatype, dimension]:
      array() except+
      datatype& operator[](size_t)

