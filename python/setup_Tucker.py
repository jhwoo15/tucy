import numpy
from setuptools import Extension, setup

from Cython.Build import cythonize

extensions = [
    Extension(name="Tucker", 
              #sources=["tensor_decomposition.pyx"],
              sources=["Tucker.pyx"],
              #sources=["tensor_decomposition.pyx", "DenseTensor.pyx"],
              include_dirs=[numpy.get_include(), "../source/"], #Extension("blahblah")
              extra_compile_args=["-std=c++11", "-mkl", "-qopenmp"],
              libraries = ['mkl_lapack95_lp64', 'mkl_intel_lp64', 'mkl_intel_thread', 'mkl_core', 'iomp5', 'pthread']
             )
    ]

setup(
    name="TDL",
    version="0.0.1",
    description="Finite Difference Hamiltonian Decomposition Library",
    ext_modules=cythonize(extensions),
    language="c++"
    )
