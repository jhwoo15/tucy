import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tucy",
    version="0.0.1",
    author="Jeheon Woo",
    author_email="woojh@kaist.ac.kr",
    description="Finite Difference Hamiltonian Decomposition Library",
    long_description=long_description,
    url="https://gitlab.com/jhwoo15/tucy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Physics",
    ],
    python_requires=">=3.7",
    install_requires=[
        "numpy",
        "scipy",
        "ase",
    ]
)
