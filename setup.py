import numpy
from setuptools import Extension, setup

from Cython.Build import cythonize

extensions = [
    Extension(name="tucy",
              sources=["python/tucy.pyx"],
              include_dirs=[numpy.get_include(), "include/", "python/"],
              extra_compile_args=["-std=c++11", "-mkl=parallel", "-qopenmp", "-no-multibyte-chars"],#, "-xMIC-AVX512"
              libraries = [ "iomp5", "pthread"],
              #libraries = ["mkl_rt", "mkl_lapack95_lp64", "mkl_intel_lp64", "mkl_intel_thread", "mkl_core", "iomp5", "pthread"],
              extra_link_args=["-mkl"]
             )
    ]

setup(
    name="Tucy",
    version="0.0.1",
    description="Finite Difference Hamiltonian Decomposition Library",
    ext_modules=cythonize(extensions, compiler_directives={"language_level" : "3"}, language='c++'),
    #language="c++"
    install_requires=[
                      "setuptools",
                      "Cython",
                      "numpy",
                      "scipy",
                      "ase",
                     ]
    )
