import numpy as np
from time import time
import scipy
import torch
from scipy.sparse.linalg import lobpcg, eigsh
from scipy.linalg import eigh

torch.manual_seed(0)


def timer(func):
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        print("Elapsed time[{}]: {} sec".format(func.__name__, (end - start)))
        return result

    return wrapper


def vprint(*args, verbosity=True):
    if verbosity:
        for arg in args:
            print(arg, end="")
        print()
    else:
        pass


def decompose_index(idx, shape, order="C"):
    """Decompose combined index.

    :type idx: int or array[int]
    :param idx:
        combined index
    :type shape: array[int]
    :param shape:
        shape of 3-dimensional grid, e.g., shape=(10, 20, 30)
    :type order: str, optional
    :param order:
        index order, "C" or "F", defaults to "C"

    :rtype: tuple[int or array[int]]
    :return:
        tuple of decomposed indices of each axis
    """
    assert len(shape) == 3
    if order == "F":
        idx_x = idx % shape[0]
        idx_y = torch.div(idx % (shape[0] * shape[1]), shape[0], rounding_mode="trunc")
        idx_z = torch.div(idx, shape[0] * shape[1], rounding_mode="trunc")
    elif order == "C":
        idx_x = torch.div(idx, (shape[1] * shape[2]), rounding_mode="trunc")
        idx_y = torch.div(idx % (shape[1] * shape[2]), shape[2], rounding_mode="trunc")
        idx_z = idx % shape[2]
    else:
        raise ValueError
    return idx_x, idx_y, idx_z


class Tucy:
    """Tucker decomposition method.

    :type shape: list or tuple
    :param shape:
        shape of grid, e.g., shape=(20, 20, 20)
    :type ranks: list or tuple
    :param ranks:
        decomposition ranks, e.g., ranks=(10, 10, 10)
    :type verbosity: int, optional
    :param verbosity:
        verbosity level, defaults to 0
    """

    def __init__(self, shape=None, ranks=None, verbosity=0):
        assert len(shape) == len(ranks) == 3
        self.shape = torch.tensor(shape)
        self.ranks = torch.tensor(ranks)
        self.verbosity = verbosity

        self.Us = np.empty(3, dtype=object)
        self.starting_vector = None  # need for guess eigenvectors
        self.singular_values = None
        self.core_matrix = None
        return

    @timer
    def core_diagonalize(self, neig, tol=None, maxiter=20):
        """Diagonalize core_matrix and restore orbitals."""
        ## Set starting vectors
        device = self.core_matrix.device
        if self.starting_vector is None:
            self.starting_vector = torch.randn(
                (self.ranks.prod(), neig),
                dtype=self.core_matrix.dtype,
                device=device,
            )
            self.starting_vector = torch.linalg.qr(self.starting_vector)[0]

        ## Solve eigenvalue problem
        # solve_options = {
        #     "A": self.core_matrix,
        #     "k": neig,
        #     "X": self.starting_vector,
        #     "tol": tol,
        #     "niter": maxiter,
        #     "largest": False,
        # }
        # val, vec = torch.lobpcg(**solve_options)

        # solve_options = {
        #     "A": np.array(self.core_matrix),
        #     "X": np.array(self.starting_vector),
        #     "tol": tol,
        #     "maxiter": maxiter,
        #     "largest": False,
        # }
        # val, vec = scipy.sparse.linalg.lobpcg(**solve_options)

        from tucy.torch_lobpcg import lobpcg

        solve_options = {
            "A": self.core_matrix,
            "X": self.starting_vector.to(device),
            "tol": tol,
            "maxiter": maxiter,
            "largest": False,
        }
        val, vec = lobpcg(**solve_options)

        self.starting_vector = vec
        eigvec = self.restore_orbital(vec)
        return val.cpu(), eigvec.cpu()

    @timer
    def restore_orbital(self, X):
        """Restore orbitals by projecting X from decomposed space to original space.

        :type X: np.ndarray
        :param X:
            orbitals with shape=(ngpts, neig)
        """
        # X.shape=(ngpts, neig)
        if isinstance(X, np.ndarray):
            X = torch.from_numpy(X)
        neig = X.shape[1]
        X = X.T.reshape(neig, *self.ranks)
        tmp = torch.tensordot(X, self.Us[0].T, ([1], [0]))
        tmp = torch.tensordot(tmp, self.Us[1].T, ([1], [0]))
        tmp = torch.tensordot(tmp, self.Us[2].T, ([1], [0]))
        tmp = tmp.reshape(neig, -1).T
        return tmp

    def set_custom_Us(self, Us):
        """set custom U matrices."""
        assert len(Us) == 3
        for i in range(3):
            assert Us[i].shape == (self.shape[i], self.ranks[i])
            self.Us[i] = Us[i]
        return

    @timer
    def decompose(self, hamiltonian):
        assert isinstance(hamiltonian, torch.Tensor)
        assert hamiltonian.layout in [torch.sparse_csr, torch.sparse_coo]
        assert (
            hamiltonian.dtype == torch.float64 or hamiltonian.dtype == torch.complex128
        )
        device = hamiltonian.device

        shape = torch.cat((self.shape, self.shape)).to(device)
        hamiltonian = hamiltonian.to_sparse_coo().coalesce()

        row_index, col_index = hamiltonian.indices()
        val = hamiltonian.values()

        indices = torch.zeros(6, len(row_index), dtype=torch.int32, device=device)
        indices[0], indices[1], indices[2] = decompose_index(row_index, self.shape, "C")
        indices[3], indices[4], indices[5] = decompose_index(col_index, self.shape, "C")

        Us = np.empty(3, dtype=object)
        for dim in range(3):
            tmp_indices = torch.roll(indices, -dim, 0)
            tmp_shape = torch.roll(shape, -dim)
            stride = torch.cat(
                (
                    torch.cumprod(tmp_shape[1:].flip(0), 0).flip(0),
                    torch.ones(1, dtype=torch.int32, device=device),
                )
            ).reshape(-1, 1)
            row = tmp_indices[0]
            col = (tmp_indices[1:] * stride[1:]).sum(0)

            size = (shape[dim].item(), torch.prod(shape, 0).item() // shape[dim].item())
            mat = torch.sparse_coo_tensor(torch.vstack((row, col)), val, size=size)
            if torch.cuda.is_available() and torch.version.hip:
                eigval, eigvec = torch.linalg.eigh(
                    torch.sparse.mm(mat, mat.conj_physical().t()).to_dense()
                )
            else:
                mat = mat.to_sparse_csr()
                eigval, eigvec = torch.linalg.eigh(
	            (mat @ mat.conj_physical().t()).to_dense()
                # sparse @ sparse operation is only working when cuda version >= 11
                )
            self.singular_values = eigval
            vprint(
                f"singular_values=\n{self.singular_values}", verbosity=self.verbosity
            )
            self.Us[dim] = eigvec[:, -self.ranks[dim] :]

        ## initialize core_matrix
        R = self.ranks.prod()
        self.core_matrix = torch.zeros(R, R, dtype=self.Us[0].dtype, device=device)
        return

    def calculate_PW(self, kpt=[0, 0, 0]):
        """Make plane-wave basis."""
        Us = np.empty(3, dtype=object)
        for i in range(3):
            Us[i] = torch.zeros(self.shape[i], self.ranks[i], dtype=torch.complex128)
            for k in range(
                -int(torch.floor(self.ranks[i] / 2)), int(torch.ceil(self.ranks[i] / 2))
            ):
                k_ = 2 * abs(k) + (-1 if k < 0 else 0)
                Us[i][:, k_] = torch.exp(
                    -2
                    * np.pi
                    * 1j
                    * (k + kpt[i])
                    * torch.arange(self.shape[i])
                    / self.shape[i]
                )
                Us[i][:, k_] /= torch.linalg.norm(Us[i][:, k_])
        return Us[0], Us[1], Us[2]

    @timer
    def project_kinetic_to_core(self, T_xx, T_yy, T_zz):
        device = self.core_matrix.device
        Ux, Uy, Uz = self.Us
        T_xx_sub = Ux.conj().T @ (T_xx.to(device) @ Ux)
        T_yy_sub = Uy.conj().T @ (T_yy.to(device) @ Uy)
        T_zz_sub = Uz.conj().T @ (T_zz.to(device) @ Uz)
        I_xx_sub = torch.eye(self.ranks[0], device=device)
        I_yy_sub = torch.eye(self.ranks[1], device=device)
        I_zz_sub = torch.eye(self.ranks[2], device=device)
        self.core_matrix += torch.kron(T_xx_sub, torch.kron(I_yy_sub, I_zz_sub))
        self.core_matrix += torch.kron(I_xx_sub, torch.kron(T_yy_sub, I_zz_sub))
        self.core_matrix += torch.kron(I_xx_sub, torch.kron(I_yy_sub, T_zz_sub))
        return

    @timer
    def project_local_potential_to_core(self, local_pot):
        local_pot = local_pot.reshape(1, 1, 1, *self.shape)
        local_pot = local_pot.to(self.core_matrix.device)

        for dim in range(3):
            _U = self.Us[dim].T
            shape = [1, 1, 1, 1, 1, 1]
            shape[3 + dim] = self.shape[dim]
            shape[2 * dim] = -1
            local_pot = local_pot * _U.reshape(shape)
            local_pot = torch.tensordot(_U.conj(), local_pot, dims=([1], [3 + dim]))
        local_pot = torch.permute(local_pot, (2, 1, 0, 3, 4, 5))
        Rxyz = self.ranks.prod()
        self.core_matrix += local_pot.reshape(Rxyz, Rxyz)
        return

    @timer
    def project_KB_projectors_to_core(self, D_matrix, kb_proj):
        assert isinstance(kb_proj, torch.Tensor)
        assert kb_proj.layout == torch.sparse_csr
        if D_matrix.layout == torch.sparse_csr:
            D_matrix = D_matrix.to_dense()  # D_matrix is forced to be dense matrix.
        P = kb_proj.to_dense().reshape(-1, *self.shape)

        device = self.core_matrix.device
        D_matrix = D_matrix.to(device)
        P = P.to(device)

        Ux, Uy, Uz = self.Us
        tmp = torch.tensordot(P, Ux, ([1], [0]))
        tmp = torch.tensordot(tmp, Uy, ([1], [0]))
        tmp = torch.tensordot(tmp, Uz, ([1], [0]))
        PU = tmp.reshape(len(P), -1)
        D_matrix = D_matrix.to(PU.dtype)
        self.core_matrix += PU.conj().T @ (D_matrix @ PU)
        return

    def get_singular_values(self):
        """Return singular values of singular vectors (Us) calculated by HOSVD."""
        return self.singular_values
