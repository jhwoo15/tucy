"""
Test tucy_python.
Compare eigenvalues of tucy's and tucy_python's.
"""
import re
import numpy as np

np.random.seed(0)
from time import time
import scipy.sparse as sparse
from scipy.sparse.linalg import eigsh
from util import make_kinetic_op, harmonic_potential
from Grid import Grid
from ase import Atoms
from tucy import Tucy
import torch

torch.set_printoptions(edgeitems=1000000000000000000)
from util import scipy_to_torch_sparse


TEST_HARMONIC = True  # False#True
TEST_COMPLEX = True
TEST_COMPLEX = False
test_new_methods = True
print(" Tucy TEST ")
USE_CUDA = False#True
USE_CUDA = True

shape = list(map(int, re.split("\s+", input("shape : ").strip())))
ranks = list(map(int, re.split("\s+", input("ranks : ").strip())))
# shape = [7, 8, 9]
# ranks = [5, 6, 7]
num_eig = 8
print(f"* shape = {shape}")
print(f"* ranks = {ranks}")
print(f"* num_eig = {num_eig}")
print(f"* USE_CUDA = {USE_CUDA}")

if USE_CUDA:
    ## Warm-up
    _ = torch.randn(10, 10).cuda()
    _ = _ @ _
    _ = _ @ _
    print("Finish Warm-up!")

if TEST_HARMONIC:
    print("=============== START : TEST_HARMONIC =================")
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms("H", cell=cells)
    grid = Grid(atoms, shape[:3])

    hamiltonian = sparse.csr_matrix((num_grid, num_grid), dtype=float)
    hamiltonian += make_kinetic_op(grid, combine=True)
    local_potential = harmonic_potential(grid, cells / 2)
    hamiltonian += sparse.diags(local_potential)

    # 1. scipy.sparse.linalg.eigsh
    start = time()
    eig_val_ref, eig_vec_ref = eigsh(hamiltonian, num_eig, which="SA", tol=0, v0=None)
    end = time()
    print(
        "[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end - start)
    )
    print("eig_val_ref = ", eig_val_ref)

    # 3. Tucker_python
    T_xx, T_yy, T_zz = make_kinetic_op(grid, combine=False)

    hamiltonian = scipy_to_torch_sparse(hamiltonian)
    T_xx, T_yy, T_zz = (
        torch.from_numpy(T_xx),
        torch.from_numpy(T_yy),
        torch.from_numpy(T_zz),
    )
    local_potential = torch.from_numpy(local_potential)

    if USE_CUDA:
        hamiltonian = hamiltonian.to_sparse_coo().cuda().to_sparse_csr() #hamiltonian.cuda()

    start = time()
    tucker = Tucy(shape[:3], ranks[:3], verbosity=1)
    tucker.decompose(hamiltonian)
    tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
    tucker.project_local_potential_to_core(local_potential)
    eig_val2, eig_vec2 = tucker.core_diagonalize(num_eig, maxiter=1000)
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end - start))
    print(f"eig_val = {eig_val2}")
    print(
        f"|eig_vec_ref - eig_vec2| = {np.linalg.norm(abs(eig_vec_ref) - abs(eig_vec2), axis=0)}"
    )
    print("================ END : TEST_HARMONIC ==================\n")


if TEST_COMPLEX:
    print("=============== START : TEST_COMPLEX  =================")
    shape = np.append(shape, shape)
    ranks = np.append(ranks, ranks)
    num_grid = np.prod(shape[:3])
    cells = shape[:3]
    atoms = Atoms("H", cell=cells)
    grid = Grid(atoms, shape[:3])
    # pbc = [True, True, True]
    # grid.set_pbc(pbc)
    kpt = [1, 1, 1]

    hamiltonian = sparse.csr_matrix((num_grid, num_grid), dtype=complex)
    hamiltonian += make_kinetic_op(grid, kpt=kpt, combine=True)

    local_potential = harmonic_potential(grid, cells / 2)
    hamiltonian += sparse.diags(local_potential)

    LM = 2
    D_matrix = sparse.diags(np.random.rand(LM))
    kb_proj = sparse.csr_matrix(
        np.random.rand(LM, num_grid) + np.random.rand(LM, num_grid) * 1j
    )
    hamiltonian += kb_proj.conj().T @ D_matrix @ kb_proj

    # 1. scipy.sparse.linalg.eigsh
    start = time()
    eig_val_ref, eig_vec_ref = eigsh(hamiltonian, num_eig, which="SA", tol=0, v0=None)
    end = time()
    sort_idx = eig_val_ref.argsort()
    eig_val_ref, eig_vec_ref = eig_val_ref[sort_idx], eig_vec_ref[:, sort_idx]
    print(
        "[Measure Time] Time for scipy.sparse.linalg.eigsh : {} s".format(end - start)
    )
    print("eig_val_ref = ", eig_val_ref)
    density_ref = np.sum(eig_vec_ref * eig_vec_ref.conj(), axis=1)

    # 3. Tucker_python
    T_xx, T_yy, T_zz = make_kinetic_op(grid, kpt=kpt, combine=False)

    hamiltonian = scipy_to_torch_sparse(hamiltonian)
    T_xx, T_yy, T_zz = (
        torch.from_numpy(T_xx),
        torch.from_numpy(T_yy),
        torch.from_numpy(T_zz),
    )
    local_potential = torch.from_numpy(local_potential)
    D_matrix = scipy_to_torch_sparse(sparse.csr_matrix(D_matrix))
    kb_proj = scipy_to_torch_sparse(kb_proj)

    start = time()
    tucker = Tucy(shape[:3], ranks[:3], verbosity=1)
    tucker.decompose(hamiltonian)
    tucker.project_kinetic_to_core(T_xx, T_yy, T_zz)
    tucker.project_local_potential_to_core(local_potential)
    tucker.project_KB_projectors_to_core(D_matrix, kb_proj)
    eig_val2, eig_vec2 = tucker.core_diagonalize(num_eig, maxiter=1000)
    end = time()
    print("[Measure Time] Time for Tucker Diagonalization : {} s".format(end - start))
    print(f"eig_val = {eig_val2}")
    # print(f"|eig_vec_ref - eig_vec2| = {np.linalg.norm(eig_vec_ref - eig_vec2, axis=0)}")
    print(
        f"|eig_vec_ref - eig_vec2| = {np.linalg.norm(abs(eig_vec_ref) - abs(eig_vec2), axis=0)}"
    )
    print("================ END : TEST_COMPLEX  ==================\n")
