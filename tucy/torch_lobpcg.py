import warnings
import torch
from tucy.LinearOperator import LinearOperator, aslinearoperator


def eigh(A, B=None):
    """
    Generalized eigenvalue problem solver for a complex Hermitian or real symmetric
    matrix, where B is positive-definite matrix. Solve :math:`Ax=\lambda Bx`, with
    cholesky decomposition :math:`L^{-1}AL^{*-1}\ L^*x=\lambda x\ s.t B=LL^*`
    When B is not defined, it is assumed identity matrix.

    :type  A: torch.Tensor
    :param A:
        tensor of shape (n,n), hermitian or real symmetric matrix
    :type B: torch.Tensor, optional
    :param B:
        tensor of shape (n,n), hermitian or real symmetric positive definite matrix.
        Assumed identity matrix of same shape of A if None. Default: None.
    :rtype: tuple
    :return:
        tuple of eigenvalue tensor and eigenvector tensor
    """
    if B is not None:
        L = torch.linalg.cholesky(B)
        Linv = torch.linalg.inv(L)
        _A = (Linv @ A) @ Linv.conj().T
        _lambda, eighv = torch.linalg.eigh(_A)
        eighv = Linv.conj().T @ eighv
    else:
        _lambda, eighv = torch.linalg.eigh(A)
    return _lambda, eighv


def bmat(x):
    """Build a 2d-tensor from a nested sequence of 2d-tensor. Patch block 2d-tensors to return one large 2d-tensor.

    :type x: list[list[torch.Tensor]]
    :param x:
        Input data. Nested list of tensors.
    :rtype: torch.Tensor
    :return:
        block

    **Example**
    >>> A = torch.Tensor([[1.0, 2.0], [3.0, 4.0]])
    >>> B = torch.Tensor([[1.1, 2.1], [3.1, 4.1]])
    >>> C = torch.Tensor([[1.2, 2.2], [3.2, 4.2]])
    >>> D = torch.Tensor([[1.3, 2.3], [3.3, 4.3]])
    >>> bmat([[A, B],[C, D]])
    torch.Tensor([[1.0, 2.0, 1.1, 2.1],
                  [3.0, 4.0, 3.1, 4.1],
                  [1.2, 2.2, 1.3, 2.3],
                  [3.2, 4.2, 3.3, 4.3]])
    """
    return torch.cat([torch.cat(i, dim=1) for i in x], dim=0)


def _report_nonhermitian(M, name):
    """Report if `M` is not a Hermitian matrix given its type."""
    md = M - M.T.conj()
    nmd = torch.linalg.norm(md, 1)
    tol = 10 * torch.finfo(M.dtype).eps
    tol = max(tol, tol * torch.linalg.norm(M, 1))
    if nmd > tol:
        print(
            "matrix %s of the type %s is not sufficiently Hermitian:" % (name, M.dtype)
        )
        print("condition: %.e < %e" % (nmd, tol))
    return


def _as2d(ar):
    """If the input array is 2D return it, if it is 1D, append a dimension, making it a column vector."""
    if ar.ndim == 2:
        return ar
    elif ar.ndim == 1:
        aux = ar.view(-1, 1)
        return aux


def _applyConstraints(blockVectorV, factYBY, blockVectorBY, blockVectorY):
    """Changes blockVectorV in place."""
    YBV = blockVectorBY.T.conj() @ blockVectorV
    tmp = torch.linalg.cholesky_solve(YBV, factYBY[0], upper=False)
    blockVectorV -= blockVectorY @ tmp
    return


def initialize(A, B, blockVectorX, sizeX, largest):
    # B-orthonormalize X.
    blockVectorX, blockVectorBX = _b_orthonormalize(B, blockVectorX)

    # Compute the initial Ritz vectors: solve the eigenproblem.
    blockVectorAX = A(blockVectorX)
    gramXAX = blockVectorX.T.conj() @ blockVectorAX

    _lambda, eigBlockVector = eigh(gramXAX)
    ii = _get_indx(_lambda, sizeX, largest)
    _lambda = _lambda[ii]

    eigBlockVector = eigBlockVector[:, ii]
    blockVectorX = blockVectorX @ eigBlockVector
    blockVectorAX = blockVectorAX @ eigBlockVector
    if B is not None:
        blockVectorBX = blockVectorBX @ eigBlockVector

    # Active index set.
    activeMask = torch.ones((sizeX,), dtype=bool, device=blockVectorX.device)

    return blockVectorX, blockVectorBX, blockVectorAX, _lambda, activeMask


def _b_orthonormalize(B, blockVectorV, blockVectorBV=None, retInvR=False):
    """
    B-orthonormalize the given block vector using Cholesky.
    so that any :math:`v_i,v_j`  :math:`v_iBv_j=\delta_{ij}`

    :type B: torch.Tensor or gospel.LinearOperator
    :param B:
        A linear system of size (n, n) that is positive definite hermitian.
    :type blockVectorV: torch.Tensor
    :param blockVectorV:
        Input data of size (n, b), b < n.
        rank of this tensor should be exactly b,
        else it occurs cholesky decomposition error
    :type blockVectorBV: torch.Tensor, optional
    :param blockVectorBV:
        matmul results of B@V. Calculate matmul of B@V if it is None.
        Default is None.
    :type retInvR: bool, optional
    :param retInvR:
        return inverse of upper cholesky matrix of B and normalization term of V
        if it is True. Default False.
    :rtype: tuple
    :return:
        B-orthonormalized block vector, V, BV.
        if retInvR is True, return inverse of cholesky matrix and normalizer of V additionally.
    """
    normalization = (
        blockVectorV.abs().max(axis=0)[0] + torch.finfo(blockVectorV.dtype).eps
    )
    blockVectorV = blockVectorV / normalization
    if blockVectorBV is None:
        if B is not None:
            blockVectorBV = B(blockVectorV)
        else:
            blockVectorBV = blockVectorV  # Shared data!!!
    else:
        blockVectorBV = blockVectorBV / normalization
    VBV = blockVectorV.T.conj() @ blockVectorBV
    # VBV = (VBV + VBV.T.conj()) / 2  # symmetrize VBV
    try:
        # VBV is an upper cholesky factor from now on.
        VBV = torch.linalg.cholesky(VBV, upper=True)
        VBV = torch.linalg.inv(VBV)
        blockVectorV = blockVectorV @ VBV
        if B is not None:
            blockVectorBV = blockVectorBV @ VBV
        else:
            blockVectorBV = None
    except:
        # raise ValueError("Cholesky has failed")
        blockVectorV = None
        blockVectorBV = None
        VBV = None

    if retInvR:
        return blockVectorV, blockVectorBV, VBV, normalization
    else:
        return blockVectorV, blockVectorBV


def _get_indx(_lambda, num, largest):
    """Get `num` indices into `_lambda` depending on `largest` option."""
    ii = torch.argsort(_lambda, descending=largest)
    return ii[:num]


def check_verbosity(verbosityLevel, B, M, X, Y):
    if verbosityLevel:
        aux = "Solving "
        if B is None:
            aux += "standard"
        else:
            aux += "generalized"
        aux += " eigenvalue problem with"
        if M is None:
            aux += "out"
        aux += " preconditioning\n\n"
        aux += "matrix size %d\n" % X.shape[0]
        aux += "block size %d\n\n" % X.shape[1]
        if Y is None:
            aux += "No constraints\n\n"
        else:
            if Y.shape[1] > 1:
                aux += "%d constraints\n\n" % Y.shape[1]
            else:
                aux += "%d constraint\n\n" % Y.shape[1]
        print(aux)
    return


def lobpcg(
    A,
    X,
    B=None,
    M=None,
    Y=None,
    tol=None,
    maxiter=None,
    largest=True,
    verbosityLevel=-1,
    retLambdaHistory=False,
    retResidualNormsHistory=False,
    test=False,
    verboseEvery=100,
):
    """Locally Optimal Block Preconditioned Conjugate Gradient Method (LOBPCG)

    LOBPCG is a preconditioned eigensolver for large symmetric positive
    definite (SPD) generalized eigenproblems.

    Parameters
    ----------
    :type A: torch.Tensor or LinearOperator
    :param A:
        The symmetric linear operator of the problem, usually a
        sparse matrix.  Often called the "stiffness matrix".
    :type X: torch.Tensor
    :param X:
        Initial approximation to the ``k`` eigenvectors (non-sparse). If `A`
        has ``shape=(n,n)`` then `X` should have shape ``shape=(n,k)``.
    :type B: torch.Tensor or LinearOperator, optional
    :param B:
        The right hand side operator in a generalized eigenproblem.
        By default, ``B = Identity``.  Often called the "mass matrix".
    :type M: torch.Tensor or LinearOperator, optional
    :param M:
        Preconditioner to `A`; by default ``M = Identity``.
        `M` should approximate the inverse of `A`.
        :math:`|M@A-I|_{F}` be sufficiently small
    :type Y: torch.Tensor or LinearOperator, optional
    :param Y:
        n-by-sizeY matrix of constraints (non-sparse), sizeY < n
        The iterations will be performed in the B-orthogonal complement
        of the column-space of Y. Y must be full rank.
    :type tol: scalar, optional
    :param tol:
        Solver tolerance (stopping criterion).
        The default is ``tol=n*sqrt(eps)``.
    :type maxiter: int, optional
    :param maxiter:
        Maximum number of iterations.  The default is ``maxiter = 20``.
    :type largest: bool, optional
    :param largest:
        If True, solve for the largest eigenvalues, otherwise the smallest.
    :type verbosityLevel: int, optional
    :param verbosityLevel:
        print intermediate solver output.  The default is ``verbosityLevel=0``.
    :type verboseEvery: int, optional
    :param verbosityEvery:
        print every this many iteration step. The default is ``verbosityEvery=300``.
    :type retLambdaHistory: bool, optional
    :param retLambdaHistory:
        Whether to return eigenvalue history.  Default is False.
    :type retResidualNormsHistory: bool, optional
    :param retResidualNormsHistory:
        Whether to return history of residual norms.  Default is False.
    :rtype: tuple
    :return:
        eigenvalue tensor, eigenvector tensor
    -------
    w : ndarray
        Array of ``k`` eigenvalues
    v : ndarray
        An array of ``k`` eigenvectors.  `v` has the same shape as `X`.
    lambdas : list of ndarray, optional
        The eigenvalue history, if `retLambdaHistory` is True.
    rnorms : list of ndarray, optional
        The history of residual norms, if `retResidualNormsHistory` is True.

    Notes
    -----
    If both ``retLambdaHistory`` and ``retResidualNormsHistory`` are True,
    the return tuple has the following format
    ``(lambda, V, lambda history, residual norms history)``.

    In the following ``n`` denotes the matrix size and ``m`` the number
    of required eigenvalues (smallest or largest).

    The LOBPCG code internally solves eigenproblems of the size ``3m`` on every
    iteration by calling the "standard" dense eigensolver, so if ``m`` is not
    small enough compared to ``n``, it does not make sense to call the LOBPCG
    code, but rather one should use the "standard" eigensolver, e.g. numpy or
    scipy function in this case.
    If one calls the LOBPCG algorithm for ``5m > n``, it will most likely break
    internally, so the code tries to call the standard function instead.

    It is not that ``n`` should be large for the LOBPCG to work, but rather the
    ratio ``n / m`` should be large. It you call LOBPCG with ``m=1``
    and ``n=10``, it works though ``n`` is small. The method is intended
    for extremely large ``n / m`` [4]_.

    The convergence speed depends basically on two factors:

    1. How well relatively separated the seeking eigenvalues are from the rest
       of the eigenvalues. One can try to vary ``m`` to make this better.

    2. How well conditioned the problem is. This can be changed by using proper
       preconditioning. For example, a rod vibration test problem (under tests
       directory) is ill-conditioned for large ``n``, so convergence will be
       slow, unless efficient preconditioning is used. For this specific
       problem, a good simple preconditioner function would be a linear solve
       for `A`, which is easy to code since A is tridiagonal.

    References
    ----------
    .. [1] A. V. Knyazev (2001),
           Toward the Optimal Preconditioned Eigensolver: Locally Optimal
           Block Preconditioned Conjugate Gradient Method.
           SIAM Journal on Scientific Computing 23, no. 2,
           pp. 517-541. :doi:`10.1137/S1064827500366124`

    .. [2] A. V. Knyazev, I. Lashuk, M. E. Argentati, and E. Ovchinnikov
           (2007), Block Locally Optimal Preconditioned Eigenvalue Xolvers
           (BLOPEX) in hypre and PETSc. :arxiv:`0705.2626`

    .. [3] A. V. Knyazev's C and MATLAB implementations:
           https://bitbucket.org/joseroman/blopex

    .. [4] S. Yamada, T. Imamura, T. Kano, and M. Machida (2006),
           High-performance computing for exact numerical approaches to
           quantum many-body problems on the earth simulator. In Proceedings
           of the 2006 ACM/IEEE Conference on Supercomputing.
           :doi:`10.1145/1188455.1188504`

    Examples
    --------

    Solve ``A x = lambda x`` with constraints and preconditioning.

    >>> import numpy as np
    >>> from scipy.sparse import spdiags, issparse
    >>> from scipy.sparse.linalg import lobpcg, LinearOperator
    >>> n = 100
    >>> vals = np.arange(1, n + 1)
    >>> A = spdiags(vals, 0, n, n)
    >>> A.toarray()
    array([[  1.,   0.,   0., ...,   0.,   0.,   0.],
           [  0.,   2.,   0., ...,   0.,   0.,   0.],
           [  0.,   0.,   3., ...,   0.,   0.,   0.],
           ...,
           [  0.,   0.,   0., ...,  98.,   0.,   0.],
           [  0.,   0.,   0., ...,   0.,  99.,   0.],
           [  0.,   0.,   0., ...,   0.,   0., 100.]])

    Constraints:

    >>> Y = np.eye(n, 3)

    Initial guess for eigenvectors, should have linearly independent
    columns. Column dimension = number of requested eigenvalues.

    >>> X = np.random.rand(n, 3)

    Preconditioner in the inverse of A in this example:

    >>> invA = spdiags([1./vals], 0, n, n)

    The preconditiner must be defined by a function:

    >>> def precond( x ):
    ...     return invA @ x

    The argument x of the preconditioner function is a matrix inside `lobpcg`,
    thus the use of matrix-matrix product ``@``.

    The preconditioner function is passed to lobpcg as a `LinearOperator`:

    >>> M = LinearOperator(matvec=precond, matmat=precond,
    ...                    shape=(n, n), dtype=float)

    Let us now solve the eigenvalue problem for the matrix A:

    >>> eigenvalues, _ = lobpcg(A, X, Y=Y, M=M, largest=False)
    >>> eigenvalues
    array([4., 5., 6.])

    Note that the vectors passed in Y are the eigenvectors of the 3 smallest
    eigenvalues. The results returned are orthogonal to those.

    """
    ## check dimension
    if X.ndim != 2:
        raise ValueError("expected rank-2 array for argument X")
    if A.device != X.device:
        raise ValueError(f"A is in {A.device}, while X in {X.device}")
    sizeY = Y.shape[1] if Y is not None else 0
    n, sizeX = X.shape
    assert (n - sizeY) >= (
        5 * sizeX
    ), "The problem size is small compared to the block size. Using dense eigensolver instead of LOBPCG."

    dev_ = A.device
    A = aslinearoperator(A)
    B = aslinearoperator(B)
    M = aslinearoperator(M)
    check_verbosity(verbosityLevel, B, M, X, Y)

    blockVectorX = X
    blockVectorY = Y

    ## set defaults values
    maxiter = 20 if maxiter is None else maxiter
    tol = (1e-15) ** (1 / 2) * n if tol is None else tol
    residualTolerance = tol

    # Apply constraints to X.
    if blockVectorY is not None:
        if B is not None:
            blockVectorBY = B(blockVectorY)
        else:
            blockVectorBY = blockVectorY
        # gramYBY is a dense array.
        gramYBY = blockVectorY.T.conj() @ blockVectorBY
        try:
            # gramYBY is a Cholesky factor from now on...
            gramYBY = torch.linalg.cholesky_ex(gramYBY, upper=False, check_errors=True)
        except RuntimeError as e:
            raise ValueError("cannot handle linearly dependent constraints") from e
        _applyConstraints(blockVectorX, gramYBY, blockVectorBY, blockVectorY)

    blockVectorX, blockVectorBX, blockVectorAX, _lambda, activeMask = initialize(
        A, B, blockVectorX, sizeX, largest
    )

    lambdaHistory = [_lambda]
    residualNormsHistory = []

    previousBlockSize = sizeX
    ident = torch.eye(sizeX, dtype=A.dtype, device=dev_)
    ident0 = torch.eye(sizeX, dtype=A.dtype, device=dev_)

    ## Main iteration loop.

    # set during iteration, Initially None
    blockVectorP = None
    blockVectorAP = None
    blockVectorBP = None

    iterationNumber = -1
    restart = True
    explicitGramFlag = False

    while iterationNumber < maxiter:
        iterationNumber += 1
        if verbosityLevel > 0 and iterationNumber % verboseEvery == 0:
            print("\niteration %d" % iterationNumber)

        if B is not None:
            aux = blockVectorBX * _lambda.unsqueeze(0)
        else:
            aux = blockVectorX * _lambda.unsqueeze(0)

        blockVectorR = blockVectorAX - aux

        aux = torch.sum(blockVectorR.conj() * blockVectorR, 0).real
        residualNorms = torch.sqrt(aux)

        residualNormsHistory.append(residualNorms)

        ii = torch.where(residualNorms > residualTolerance, True, False)
        activeMask = activeMask & ii
        if verbosityLevel > 2:
            print(f"activeMask : {activeMask}")

        currentBlockSize = activeMask.sum()
        if currentBlockSize == 0:
            break
        if currentBlockSize != previousBlockSize:
            previousBlockSize = currentBlockSize
            ident = torch.eye(currentBlockSize, dtype=A.dtype, device=dev_)

        if verbosityLevel > 0 and iterationNumber % verboseEvery == 0:
            print("current block size: ", currentBlockSize)
            print("eigenvalue: ", _lambda)
            print("residual norms: ", residualNorms)
            print("tol : ", residualTolerance)

        activeBlockVectorR = _as2d(blockVectorR[:, activeMask])

        if iterationNumber > 0:
            activeBlockVectorP = _as2d(blockVectorP[:, activeMask])
            activeBlockVectorAP = _as2d(blockVectorAP[:, activeMask])
            if B is not None:
                activeBlockVectorBP = _as2d(blockVectorBP[:, activeMask])

        if M is not None:
            # Apply preconditioner T to the active residuals.
            activeBlockVectorR = M(activeBlockVectorR)

        # Apply constraints to the preconditioned residuals.
        if blockVectorY is not None:
            _applyConstraints(activeBlockVectorR, gramYBY, blockVectorBY, blockVectorY)

        # B-orthogonalize the preconditioned residuals to X.
        if B is not None:
            activeBlockVectorR = activeBlockVectorR - blockVectorX @ (
                blockVectorBX.T.conj() @ activeBlockVectorR
            )
        else:
            activeBlockVectorR = activeBlockVectorR - blockVectorX @ (
                blockVectorX.T.conj() @ activeBlockVectorR
            )

        # B-orthonormalize the preconditioned residuals.
        aux = _b_orthonormalize(B, activeBlockVectorR)
        activeBlockVectorR, activeBlockVectorBR = aux

        if activeBlockVectorR is None:
            warnings.warn(
                f"Failed at iteration {iterationNumber} with accuracies "
                f"{residualNorms}\n not reaching the requested "
                f"tolerance {residualTolerance}.",
                UserWarning,
                stacklevel=2,
            )
            break

        activeBlockVectorAR = A(activeBlockVectorR)

        if iterationNumber > 0:
            if B is not None:
                aux = _b_orthonormalize(
                    B, activeBlockVectorP, activeBlockVectorBP, retInvR=True
                )
                activeBlockVectorP, activeBlockVectorBP, invR, normal = aux
            else:
                aux = _b_orthonormalize(B, activeBlockVectorP, retInvR=True)
                activeBlockVectorP, _, invR, normal = aux
            # Function _b_orthonormalize returns None if Cholesky fails
            if activeBlockVectorP is not None:
                activeBlockVectorAP = activeBlockVectorAP / normal
                activeBlockVectorAP = activeBlockVectorAP @ invR
                restart = False
            else:
                if verbosityLevel > 0:
                    warnings.warn(
                        f"Failed orthonormalization at iteration " f"{iterationNumber}",
                        UserWarning,
                        stacklevel=2,
                    )
                restart = True

        # Perform the Rayleigh Ritz Procedure:
        # Compute symmetric Gram matrices:

        if activeBlockVectorAR.dtype in [torch.float32, torch.complex64]:
            myeps = 1
        elif activeBlockVectorR.dtype in [torch.float32, torch.complex64]:
            myeps = 1e-4
        else:
            myeps = 1e-8

        if residualNorms.max() > myeps and not explicitGramFlag:
            explicitGramFlag = False
        else:
            # Once explicitGramFlag, forever explicitGramFlag.
            explicitGramFlag = True

        # Shared memory assingments to simplify the code
        if B is None:
            blockVectorBX = blockVectorX
            activeBlockVectorBR = activeBlockVectorR
            if not restart:
                activeBlockVectorBP = activeBlockVectorP

        # Common submatrices:
        gramXAR = blockVectorX.T.conj() @ activeBlockVectorAR
        gramRAR = activeBlockVectorR.T.conj() @ activeBlockVectorAR

        if explicitGramFlag:
            gramRAR = (gramRAR + gramRAR.T.conj()) / 2
            gramXAX = blockVectorX.T.conj() @ blockVectorAX
            gramXAX = (gramXAX + gramXAX.T.conj()) / 2
            gramXBX = blockVectorX.T.conj() @ blockVectorBX
            gramRBR = activeBlockVectorR.T.conj() @ activeBlockVectorBR
            gramXBR = blockVectorX.T.conj() @ activeBlockVectorBR
        else:
            gramXAX = torch.diag(_lambda)
            gramXBX = ident0
            gramRBR = ident
            gramXBR = torch.zeros((sizeX, currentBlockSize), dtype=A.dtype, device=dev_)

        def _handle_gramA_gramB_verbosity(gramA, gramB):
            if verbosityLevel > 0 and iterationNumber % verboseEvery == 0:
                _report_nonhermitian(gramA, "gramA")
                _report_nonhermitian(gramB, "gramB")
            if verbosityLevel > 10 and iterationNumber % verboseEvery == 0:
                # Note: not documented, but leave it in here for now
                torch.save(gramA, "gramA.pt")
                torch.save(gramB, "gramB.pt")

        if not restart:
            gramXAP = blockVectorX.T.conj() @ activeBlockVectorAP
            gramRAP = activeBlockVectorR.T.conj() @ activeBlockVectorAP
            gramPAP = activeBlockVectorP.T.conj() @ activeBlockVectorAP
            gramXBP = blockVectorX.T.conj() @ activeBlockVectorBP
            gramRBP = activeBlockVectorR.T.conj() @ activeBlockVectorBP
            if explicitGramFlag:
                gramPAP = (gramPAP + gramPAP.T.conj()) / 2
                gramPBP = activeBlockVectorP.T.conj() @ activeBlockVectorBP
            else:
                gramPBP = ident

            gramA = bmat(
                [
                    [gramXAX, gramXAR, gramXAP],
                    [gramXAR.T.conj(), gramRAR, gramRAP],
                    [gramXAP.T.conj(), gramRAP.T.conj(), gramPAP],
                ]
            )
            gramB = bmat(
                [
                    [gramXBX, gramXBR, gramXBP],
                    [gramXBR.T.conj(), gramRBR, gramRBP],
                    [gramXBP.T.conj(), gramRBP.T.conj(), gramPBP],
                ]
            )
            # gramA = (gramA + gramA.T.conj()) / 2  # symmetrize A
            # gramB = (gramB + gramB.T.conj()) / 2  # symmetrize B

            _handle_gramA_gramB_verbosity(gramA, gramB)

            try:
                _lambda, eigBlockVector = eigh(gramA, B=gramB)
                if verbosityLevel > 10 and iterationNumber % verboseEvery == 0:
                    print(f"gram XAX XAR XAP \n{gramXAX}\n{gramXAR}\n{gramXAP}")
                    print(f"gram RAR RAP PAP \n{gramRAR}\n{gramRAP}\n{gramPAP}")
                    print(f"gram XBX XBR XBP \n{gramXBX}\n{gramXBR}\n{gramXBP}")
                    print(f"gram RBR RBP PBP \n{gramRBR}\n{gramRBP}\n{gramPBP}\n")
            except RuntimeError:
                # try again after dropping the direction vectors P from RR
                restart = True

        if restart:
            gramA = bmat([[gramXAX, gramXAR], [gramXAR.T.conj(), gramRAR]])
            gramB = bmat([[gramXBX, gramXBR], [gramXBR.T.conj(), gramRBR]])
            # gramA = (gramA + gramA.T.conj()) / 2  # symmetrize A
            # gramB = (gramB + gramB.T.conj()) / 2  # symmetrize B

            _handle_gramA_gramB_verbosity(gramA, gramB)

            try:
                _lambda, eigBlockVector = eigh(gramA)
                if verbosityLevel > 10 and iterationNumber % verboseEvery == 0:
                    print(f"gram XAX XAR RAR \n{gramXAX}\n{gramXAR}\n{gramRAR}")
                    print(f"gram XBX XBR RBR \n{gramXBX}\n{gramXBR}\n{gramRBR}\n")
            except RuntimeError as e:
                raise ValueError("eigh has failed in lobpcg iterations") from e

        ii = _get_indx(_lambda, sizeX, largest)
        if verbosityLevel >= 10 and iterationNumber % verboseEvery == 0:
            print("gram eigh result")
            print("lambda index : ", ii)
            print("_lambda : ", _lambda)

        _lambda = _lambda[ii]
        eigBlockVector = eigBlockVector[:, ii]

        lambdaHistory.append(_lambda)

        # Compute Ritz vectors.
        if B is not None:
            if not restart:
                eigBlockVectorX = eigBlockVector[:sizeX]
                eigBlockVectorR = eigBlockVector[sizeX : sizeX + currentBlockSize]
                eigBlockVectorP = eigBlockVector[sizeX + currentBlockSize :]

                pp = activeBlockVectorR @ eigBlockVectorR
                pp += activeBlockVectorP @ eigBlockVectorP

                app = activeBlockVectorAR @ eigBlockVectorR
                app += activeBlockVectorAP @ eigBlockVectorP

                bpp = activeBlockVectorBR @ eigBlockVectorR
                bpp += activeBlockVectorBP @ eigBlockVectorP
            else:
                eigBlockVectorX = eigBlockVector[:sizeX]
                eigBlockVectorR = eigBlockVector[sizeX:]

                pp = activeBlockVectorR @ eigBlockVectorR
                app = activeBlockVectorAR @ eigBlockVectorR
                bpp = activeBlockVectorBR @ eigBlockVectorR

            if verbosityLevel > 10:
                print(pp)
                print(app)
                print(bpp)

            blockVectorX = blockVectorX @ eigBlockVectorX + pp
            blockVectorAX = blockVectorAX @ eigBlockVectorX + app
            blockVectorBX = blockVectorBX @ eigBlockVectorX + bpp

            blockVectorP, blockVectorAP, blockVectorBP = pp, app, bpp
        else:
            if not restart:
                eigBlockVectorX = eigBlockVector[:sizeX]
                eigBlockVectorR = eigBlockVector[sizeX : sizeX + currentBlockSize]
                eigBlockVectorP = eigBlockVector[sizeX + currentBlockSize :]

                pp = activeBlockVectorR @ eigBlockVectorR
                pp += activeBlockVectorP @ eigBlockVectorP

                app = activeBlockVectorAR @ eigBlockVectorR
                app += activeBlockVectorAP @ eigBlockVectorP
            else:
                eigBlockVectorX = eigBlockVector[:sizeX]
                eigBlockVectorR = eigBlockVector[sizeX:]

                pp = activeBlockVectorR @ eigBlockVectorR
                app = activeBlockVectorAR @ eigBlockVectorR

            if verbosityLevel > 10:
                print(pp)
                print(app)

            blockVectorX = blockVectorX @ eigBlockVectorX + pp
            blockVectorAX = blockVectorAX @ eigBlockVectorX + app

            blockVectorP, blockVectorAP = pp, app

    if B is not None:
        aux = blockVectorBX * _lambda.unsqueeze(0)
    else:
        aux = blockVectorX * _lambda.unsqueeze(0)

    blockVectorR = blockVectorAX - aux

    aux = torch.sum(blockVectorR.conj() * blockVectorR, 0).real
    residualNorms = torch.sqrt(aux)

    if verbosityLevel > 0:
        print("final iteration :", iterationNumber)
        print("final eigenvalue:", _lambda)
        print("final residual norms:", residualNorms)

    if retLambdaHistory:
        if retResidualNormsHistory:
            return _lambda, blockVectorX, lambdaHistory, residualNormsHistory
        else:
            return _lambda, blockVectorX, lambdaHistory
    else:
        if retResidualNormsHistory:
            return _lambda, blockVectorX, residualNormsHistory
        else:
            return _lambda, blockVectorX


if __name__ == "__main__":
    import time
    import numpy as np
    from scipy.sparse.linalg import lobpcg as scipy_lobpcg

    seed = 0
    np.random.seed(seed)
    print(f"numpy.random seed is fixed to {seed}.")

    tol = None
    maxiter = 20
    USE_CUDA = torch.cuda.is_available()
    DO_PROFILE = True  # False

    N, neig = 24**3, 132  # MgO 2x2x2 supercell with ranks=(24,24,24)
    # N, neig = 1000, 10

    A = np.random.randn(N, N)
    X = np.random.randn(N, neig)
    # A = (A + 1j * A) / 2  # hermitian case
    # X = (X + 1j * X) / 2  # hermitian case
    A = (A + A.T.conj()) / 2
    X = np.linalg.qr(X)[0]

    print("=========================")
    print(f"* A.shape = {A.shape}")
    print(f"* A.dtype = {A.dtype}")
    print(f"* X.shape = {X.shape}")
    print(f"* X.dtype = {X.dtype}")
    print(f"* USE_CUDA = {USE_CUDA}")
    print("=========================")
    options = {
        "A": A,
        "X": X,
        "tol": tol,
        "maxiter": maxiter,
        "largest": False,
        "verbosityLevel": 0,
    }
    st = time.time()
    val1, vec2 = scipy_lobpcg(**options)
    time1 = time.time() - st

    A = torch.from_numpy(A)
    X = torch.from_numpy(X)

    options = {
        "A": A,
        "X": X,
        "tol": tol,
        "maxiter": maxiter,
        "largest": False,
        "verbosityLevel": 0,
    }
    st = time.time()
    val2, vec2 = lobpcg(**options)
    time2 = time.time() - st

    if USE_CUDA:
        ##################################################
        ## Time accumulation
        import time
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        AX_time = 0.0
        #################################################

        print("Start Warm-up.")
        A = A.cuda()
        X = X.cuda()
        _ = X.T @ X; _ = X.T @ X; _ = X.T @ X
        print(f"Warm-up finished !!")
        options = {
            "A": A,
            "X": X,
            "tol": tol,
            "maxiter": maxiter,
            "largest": False,
            "verbosityLevel": 0,
        }

        if DO_PROFILE:
            from torch.profiler import profile, ProfilerActivity
            with profile(
                activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA],
                with_stack=True,
                with_modules=True,
                record_shapes=True,
                on_trace_ready=torch.profiler.tensorboard_trace_handler("./profileLog"),
            ) as prof:
                st = time.time()
                val3, vec3 = lobpcg(**options)
                time3 = time.time() - st
        else:
            st = time.time()
            val3, vec3 = lobpcg(**options)
            time3 = time.time() - st


    print("=" * 100)
    torch.set_printoptions(precision=6, sci_mode=False)
    np.set_printoptions(precision=6, suppress=True)
    print(f"eigval (scipy):\n{val1}")
    print(f"eigval (torch):\n{val2.numpy()}")
    print(f"scipy Time: {time1} sec")
    print(f"torch Time: {time2} sec")
    if USE_CUDA:
        print(f"Time (GPU): {time3} sec")
